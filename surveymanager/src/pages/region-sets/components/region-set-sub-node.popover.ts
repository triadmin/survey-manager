import { Component } from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams 
} from 'ionic-angular';

@Component({
  template: `
    <h3 padding-left>Add a sub-level to {{ node.list_item.title }}</h3>
    <ion-list>
      <ion-item>  
        <ion-label stacked>Enter a descriptor for this level.</ion-label>
        <ion-input type="text" [(ngModel)]="data.descriptor" placeholder="Like counties or districts.
        "></ion-input>
      </ion-item>
      <ion-item>
        <ion-label stacked>Add the first item to this sub-level.</ion-label>
        <ion-input type="text" [(ngModel)]="data.title" placeholder="Item name"></ion-input>
      </ion-item>
      <ion-item>
        <button ion-button icon-start full (click)="closePopover()">Save</button>
      </ion-item>
    </ion-list>
  `
})
export class RegionSetSubNodePopoverPage {
  node = {};
  data = {};

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  )
  {
    if ( this.navParams.get('node') )
    {
      this.node = this.navParams.get('node');
    }
  }

  closePopover()
  {
    this.viewCtrl.dismiss( this.data );
  }

}
