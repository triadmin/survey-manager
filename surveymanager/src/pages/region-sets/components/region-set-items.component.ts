import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import { 
  AlertController,
  PopoverController,
  ModalController,
  ActionSheetController
} from 'ionic-angular';

// Services
import { RegionsService } from '../../../providers/surveys/regions-service';

// Popovers
import { RegionSetSubNodePopoverPage } from './region-set-sub-node.popover';
import { RegionSetLocationPopoverPage } from './region-set-location.popover';

@Component({
  selector: 'component-region-set-items',
  template: `
    <div *ngIf="node.list_item">
      <!-- New item entry box -->
      
      <ion-item *ngIf="index == 0">
        <ion-input [(ngModel)]="node.list_item.new_title" placeholder="{{ node.list_item.descriptor }}: Add Item"></ion-input>
        <button small item-end icon-only (click)="prepNewNode(node.list_item)"><ion-icon color="primary" name="checkmark-circle"></ion-icon></button>
      </ion-item>

      <button ion-item (click)="toggleChildren(node)">
        <ion-input [(ngModel)]="node.list_item.title"></ion-input>
        <div item-end>
          <button small icon-only (click)="saveNode(node.list_item);$event.stopPropagation();"><ion-icon color="primary" name="checkmark-circle"></ion-icon></button>
          <button small icon only (click)="editSubNodes(node, $event);$event.stopPropagation();"><ion-icon color="warning" name="return-right"></ion-icon></button>
          <button small icon-only (click)="editNodeLocation(node,$event);$event.stopPropagation();"><ion-icon [color]="nodeHasCoords()" name="pin"></ion-icon></button>
          <button small icon-only (click)="presentDeleteNodeActionSheet(node);$event.stopPropagation();"><ion-icon color="danger" name="trash"></ion-icon></button>
        </div>
      </button>
    </div>

    <div padding-left *ngIf="node.open || topLevel">
      <div *ngFor="let child of node.children; let i = index;">
        <component-region-set-items 
          [node]="child"
          [index]="i"
          [showChildren]="showChildren"
          (refreshRegionSet)="refreshRegion()"
          [topLevel]="false"
        >
        </component-region-set-items>
      </div>
    </div>
  `
})

export class RegionSetItemsComponent {
  @Input() parentId = 0;
  @Input() node:any = {};
  @Input() index = 0;
  @Input() topLevel = true;
  @Input() showChildren = false;
  @Output() refreshRegionSet = new EventEmitter<any>();

  newNodeTitle = '';

  constructor(
    private regionsService: RegionsService,
    private popoverCtrl: PopoverController,
    private actionSheetCtrl: ActionSheetController,
    private modalCtrl: ModalController
  ) {
  }

  refreshRegion()
  {
    this.refreshRegionSet.emit();
  }

  nodeHasCoords()
  {
    if ( this.node.list_item.coords.length > 0 )
    {
      return 'header';
    }
    return 'light-gray';
  }

  prepNewNode( list_item )
  {
    if ( list_item.new_title.length > 0 )
    {
      // We're saving a new node.
      list_item.title = list_item.new_title;
      list_item.region_list_item_id = null;
      this.saveNode( list_item );
    }
  }

  saveNode( list_item )
  {
    this.regionsService.saveRegionListItem( list_item )
      .subscribe( data => {
        this.refreshRegionSet.emit();
      });    
  }

  deleteNode( node )
  {
    this.regionsService.deleteRegionListItem( node.list_item )
      .subscribe( data => {
        this.refreshRegionSet.emit();
      });
  }

  presentDeleteNodeActionSheet( node )
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this item?',
      buttons: [
        {
          text: 'Delete item',
          role: 'destructive',
          handler: () => {
            this.deleteNode( node );
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }  

  editSubNodes( node, ev )
  {
    let popover = this.popoverCtrl.create( RegionSetSubNodePopoverPage, {
      node: node
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      if ( data.title !== undefined && data.descriptor !== undefined )
      {
        // Then we're adding a sub-level.
        let new_node = {
          descriptor: data.descriptor,
          parent_id: node.list_item.region_list_item_id,
          region_id: node.list_item.region_id,
          title: data.title
        }
        this.saveNode( new_node );
      }
    });
  }

  editNodeLocation( node, ev )
  {
    let popover = this.modalCtrl.create( RegionSetLocationPopoverPage, {
      node: node
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      console.log(data);
      if ( data !== null )
      {
        let coords = JSON.stringify({
          ne: { lat: data.ne_lat, lng: data.ne_lng },
          sw: { lat: data.sw_lat, lng: data.sw_lng },
          point: { lat: data.point_lat, lng: data.point_lng }
        });

        node.list_item.coords = coords;
        this.saveNode( node.list_item );
      }
    });

  }

  toggleChildren(node)
  {
    console.log(this.showChildren);
    this.showChildren = !this.showChildren;
    console.log(this.showChildren);
    node.open = this.showChildren;
    console.log(node);
  }

  ngOnInit()
  {
    //console.log(this.node);
  }
}