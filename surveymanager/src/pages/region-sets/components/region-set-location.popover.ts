import { 
  Component,
  NgZone
} from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams
} from 'ionic-angular';

declare var google;

@Component({
  template: `
    <ion-header>
      <ion-toolbar color="header">
        <ion-title>Location Coordinates: {{ node.list_item.title }}</ion-title>
        <ion-buttons end>
          <button ion-button icon-start (click)="closePopover()">
            <ion-icon name="ios-close"></ion-icon> Cancel</button>
        </ion-buttons>
      </ion-toolbar>
    </ion-header>
    <ion-content>
      <h4 padding-left>Region Area Coordinates</h4>
      <ion-row>
        <ion-col col-6>
          <ion-item>
            <ion-label stacked>Northeast latitude</ion-label>  
            <ion-input type="text" [(ngModel)]="data.ne_lat"></ion-input>
          </ion-item>
        </ion-col>
        <ion-col col-6>
          <ion-item>
            <ion-label stacked>Northeast longitude</ion-label>
            <ion-input type="text" [(ngModel)]="data.ne_lng"></ion-input>
          </ion-item>
        </ion-col>
      </ion-row>
      <ion-row>
        <ion-col col-6>
          <ion-item>
            <ion-label stacked>Southwest latitude</ion-label>
            <ion-input type="text" [(ngModel)]="data.sw_lat"></ion-input>
          </ion-item>
        </ion-col>
        <ion-col col-6>
          <ion-item>
            <ion-label stacked>Southwest longitude</ion-label>
            <ion-input type="text" [(ngModel)]="data.sw_lng"></ion-input>
          </ion-item>
        </ion-col>
      </ion-row>

      <ion-row>
        <ion-col col-8><h4 padding-left>Single Point Coordinates</h4></ion-col>
        <ion-col col-4>
          <button margin-top clear ion-button icon-start small (click)="getCoordinates()">
            <ion-icon name="pin"></ion-icon>
            Find Coordinates
          </button>
        </ion-col>
      </ion-row>

      <ion-item *ngIf="findCoordinates">
        <ion-label stacked>Adjust location name?</ion-label>
        <ion-input type="text" [(ngModel)]="locationName"></ion-input>
        <button item-end ion-button clear (click)="getCoordinates()">Try Again</button>
      </ion-item>

      <ion-item *ngIf="noPlacesError">
        <ion-icon name="alert" color="danger" item-start></ion-icon>
        <strong>No matching locations found.</strong>
      </ion-item>

      <ion-item *ngIf="autocompleteItems.length > 0">
        <button ion-item *ngFor="let place of autocompleteItems" (click)="getPlaceCoords(place)">
          {{ place.description }}
        </button>
      </ion-item>

      <ion-row>
        <ion-col col-6>
          <ion-item>
            <ion-label stacked>Latitude</ion-label>  
            <ion-input type="text" [(ngModel)]="data.point_lat"></ion-input>
          </ion-item>
        </ion-col>
        <ion-col col-6>
          <ion-item>
            <ion-label stacked>Longitude</ion-label>
            <ion-input type="text" [(ngModel)]="data.point_lng"></ion-input>
          </ion-item>
        </ion-col>
      </ion-row>

      <ion-list>
        <ion-item>
          <button ion-button icon-start full (click)="closePopover()">Save</button>
        </ion-item>
      </ion-list>
    </ion-content>
  `
})
export class RegionSetLocationPopoverPage {
  node:any = {};
  data:any = {};
  locationName = '';
  GoogleAutocomplete = new google.maps.places.AutocompleteService();
  GoogleGeocoder = new google.maps.Geocoder();
  autocompleteItems = [];
  noPlacesError = false;
  findCoordinates = false;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams,
    private zone: NgZone
  )
  {
    if ( this.navParams.get('node') )
    {
      this.node = this.navParams.get('node');
      this.locationName = this.node.list_item.title;
    
      if ( this.node.list_item.coords.length > 0 )
      {
        // Try parsing the coords.
        let coords = JSON.parse( this.node.list_item.coords );
        this.data.ne_lat = coords.ne.lat;
        this.data.ne_lng = coords.ne.lng;
        this.data.sw_lat = coords.sw.lat;
        this.data.sw_lng = coords.sw.lng;

        if ( coords.point !== undefined )
        {
          this.data.point_lat = coords.point.lat;
          this.data.point_lng = coords.point.lng;
        }
      }
    }
  }

  getPlaceCoords( place )
  {
    var data = this.data;

    // Clear autocompleteItems.
    this.autocompleteItems = [];
    this.noPlacesError = false;

    this.GoogleGeocoder.geocode({ 'placeId': place.place_id }, ( results, status ) => {
      this.zone.run(() => {
        if (status == google.maps.GeocoderStatus.OK) {
          this.data.point_lat = results[0].geometry.location.lat();
          this.data.point_lng = results[0].geometry.location.lng();
        } else {
        }
      });
    });
  }

  getCoordinates()
  {
    this.findCoordinates = true;
    if ( this.locationName.length === 0 ) {
      this.autocompleteItems = [];
      return;
    }
    this.GoogleAutocomplete.getPlacePredictions({ input: this.locationName },
      ( predictions, status ) => {
        this.autocompleteItems = [];
        this.zone.run(() => {
          if ( predictions !== null )
          {
            this.noPlacesError = false;
            predictions.forEach((prediction) => {
              this.autocompleteItems.push(prediction);
            });
          } else {
            this.noPlacesError = true;
          }
        });
      });
  }

  closePopover()
  {
    this.viewCtrl.dismiss( this.data );
  }

}
