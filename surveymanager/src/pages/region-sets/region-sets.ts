import { 
  Component
} from '@angular/core';
import {
  Events,
  ActionSheetController,
  NavController
} from 'ionic-angular';

// Providers
import { RegionsService } from '../../providers/surveys/regions-service';

// Pages 
import { RegionSetsAddEditPage } from './region-sets-add-edit';

@Component({
  selector: 'page-region-sets',
  templateUrl: 'region-sets.html'
})

export class RegionSetsPage
{
  headerData: any;
  regionSets = [];

  constructor(
    public regionsService: RegionsService,
    public events: Events,
    public actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController
  )
  {
    this.headerData = { title: 'Manage Region Sets' };
  }

  getRegions()
  {
    this.regionsService.getRegions()
      .subscribe( data => {
        this.regionSets = data;
        //console.log(this.regionSets);
      });
  }

  showRegionSetAddEditPage( regionSet = null )
  {
    this.navCtrl.push( RegionSetsAddEditPage, {
      regionSet: regionSet
    });
  }

  ionViewDidEnter()
  {
    // Get region sets.
    this.getRegions();
  }
}