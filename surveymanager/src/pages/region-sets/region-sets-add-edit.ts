import { 
  Component, 
  EventEmitter,
  Input,
  Output,
  ViewChild
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";
import {
  Slides,
  Events,
  ActionSheetController,
  NavParams,
  NavController
} from 'ionic-angular';

// Providers
import { RegionsService } from '../../providers/surveys/regions-service';

@Component({
  selector: 'page-region-sets-add-edit',
  templateUrl: 'region-sets-add-edit.html'
})

export class RegionSetsAddEditPage
{

  edit = false;
  headerData = {};
  region: any = {};
  regionListItem = {
    region_id: 0,
    parent_id: 0,
    title: '',
    descriptor: ''
  };

  systemWide:boolean = false;
  currentItemDescriptor:string = '';
  currentItemParentId:number = 0;

  constructor(
    public regionsService: RegionsService,
    public events: Events,
    public actionSheetCtrl: ActionSheetController,
    public navParams: NavParams,
    public navCtrl: NavController
  )
  {
    if ( this.navParams.get('regionSet') )
    {
      this.edit = true;
      this.region = this.navParams.get('regionSet');
      this.headerData = { title: 'Edit Region Set: ' + this.region.title };
    } else {
      this.headerData = { title: 'New Survey' };
    } 
  }

  getRegionSet()
  {
    this.regionsService.getRegion( this.region.region_id )
      .subscribe( data => {
        this.region = data;
      });
  }

  saveRegion( quit = false )
  {
    // Save region data
    this.region.system_wide = 'No';
    if ( this.systemWide ) {
      this.region.system_wide = 'Yes';
    }

    if ( this.region.title.length === 0 )
    {
      // Make sure we have minimal amount of data.
      this.events.publish('app:log', {
        message: 'Please enter a Name for this region set.',
        status: 'error',
        type: 'error region entry'
      }); 
    } else {
      this.regionsService.saveRegion( this.region )
        .subscribe( data => {
          this.region = data;
          if ( this.region.children === null )
          {
            this.region.children = [];
          }
        });
    }
  }  

  saveRegionItem()
  {
    this.regionListItem.region_id = this.region.region_id;
    this.regionsService.saveRegionListItem( this.regionListItem )
      .subscribe( data => {
        this.getRegionSet();
      });
  }

  deleteRegion()
  {
    this.regionsService.deleteRegion( this.region.region_id )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Region set has been deleted.',
          status: 'warning',
          type: 'delete region'
        });
        
        this.navCtrl.pop();
      });
  }

  presentDeleteRegionActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this region set?',
      buttons: [
        {
          text: 'Delete region set',
          role: 'destructive',
          handler: () => {
            this.deleteRegion();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  ngOnInit()
  {
  }
}