import { Component } from '@angular/core';
import { 
  NavController, 
  Events,
  Nav,
  PopoverController,
  Popover
} from 'ionic-angular';

// Providers
import { SurveysService } from '../../providers/surveys/surveys-service';
import { Config } from '../../providers/config/config';
import { UserPermissionsService } from '../../providers/users/user-permissions-service';

// Pages
import { SurveyAddEditPage } from './survey-add-edit';
import { DashboardSurveyClientPage } from './../survey-client/dashboard/dashboard-survey';

// Popovers 
import { SurveyMenuPopoverPage } from './components/survey-menu.popover';

@Component({
  selector: 'page-surveys',
  templateUrl: 'surveys.html'
})
export class SurveysPage {
  headerData: any;

  surveys:any = [];
  surveysCopy: any = [];
  searchResults: boolean = true;

  constructor(
    public navCtrl: NavController,
    public surveysService: SurveysService,
    public events: Events,
    public nav: Nav,
    public userPermissionsService: UserPermissionsService,
    public popoverCtrl: PopoverController
  ) {
    this.headerData = { title: 'Surveys and Data Collection Tools' };
  }

  getSurveys()
  {
    this.surveys = [];
    this.surveysCopy = [];

    if ( this.userPermissionsService.userHasRole( ['survey-manager-project-level-admin' ] ))
    {
      // Get full survey records.
      for ( let s of Config.USER_APP_RECORD.surveys )
      {
        this.surveysService.getSurvey( s.survey_id )
          .subscribe( data => {
            let d = this.surveysService.setProjectObj( data );
            this.surveys.push( d );
            this.surveysCopy.push( d );
          });
      }
    } else {
      // Get all surveys in system.  
      // TODO: When we get a bagillion surveys in there, this won't
      //       be scaleable.
      this.surveysService.getSurveys()
        .subscribe( data => {
          this.surveys = data;
          for ( let item in this.surveys )
          {
            this.surveysCopy.push( this.surveys[item] );
          }
        });
    }
  }

  showSurveyAddEditPage( survey = null )
  {
    this.navCtrl.push( SurveyAddEditPage, {
      survey: survey
    });
  }

  searchSurveys( ev )
  {
    let search_term = ev.target.value;

    if ( search_term && search_term.length > 3 )
    {
      // Clear this.surveys array to prep for search results.
      this.surveys.length = 0;

      this.surveysService.searchSurveys(search_term)
        .subscribe(data => {
          this.searchResults = true;
          this.surveys = data;

          if ( this.surveys.length === 0 )
          {
            this.searchResults = false;
          }
      });
    } else {
      // Restore original conference list.
      this.surveys.length = 0;

      for ( var item in this.surveysCopy )
      {
        this.surveys.push( this.surveysCopy[item] );
      }
      this.searchResults = true;
    }
  }

  clearSearch( ev )
  {
    // Restore original entry list.
    for ( var item in this.surveysCopy )
    {
      this.surveys.push( this.surveysCopy[item] );
    }
    this.searchResults = true;
  }

  showViewSurveyClient( survey )
  {
    this.nav.setRoot( DashboardSurveyClientPage, {
      survey: survey
    });
  }

  presentSurveyMenuPopover( ev, survey )
  {
    let popover = this.popoverCtrl.create( SurveyMenuPopoverPage, {
      survey: survey
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      switch( data )
      {
        case 'view-survey-client':
          this.showViewSurveyClient( survey );
          break;
      }
    });
  }

  ionViewDidEnter()
  {
    this.getSurveys();
  }
}
