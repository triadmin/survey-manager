import { 
  Component, 
  Input,
  ViewChild
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";
import {
  ActionSheetController,
  Events,
  reorderArray,
  PopoverController
} from 'ionic-angular';

// Providers
import { SurveyQuestionsService } from '../../../providers/surveys/survey-questions-service';
import { HelperSurveyService } from '../../../providers/helpers/helper-survey-service';
import { QuestionTypeDefinitionService } from '../../../providers/surveys/question-type-definition-service';

// Question Configure Components
import { TextboxSingleFormComponent } from './question-types/textbox-single-form.component';
import { TextboxMultiFormComponent } from './question-types/textbox-multi-form.component';
import { MultiAnswerFormComponent } from './question-types/multi-answer-form.component';
import { HeadingFormComponent } from './question-types/heading-form.component';
import { ContentFormComponent } from './question-types/content-form.component';

// Popovers
import { DeleteSurveyPagePopoverPage } from './delete-page.popover';

@Component({
  selector: 'component-questions',
  templateUrl: 'survey-questions.component.html'
})

export class SurveyQuestionComponent
{
  @Input() survey: any;

  @ViewChild('textboxSingleForm') textboxSingleForm: TextboxSingleFormComponent;
  @ViewChild('textboxMultiForm') textboxMultiForm: TextboxMultiFormComponent;
  @ViewChild('multiAnswerForm') multiAnswerForm: MultiAnswerFormComponent;
  @ViewChild('headingForm') headingForm: HeadingFormComponent;
  @ViewChild('contentForm') contentForm: ContentFormComponent;

  questions:any = [];
  activeSurveyPage:number = 1;
  pageButtons:any = [];
  question:any = {
    survey_question_id: 0,
    survey_id: 0,
    question_text: '',
    question_type: '',
    extra: '',
    answers: []
  };
  surveyPage:any = {
    page: 0,
    questions: []
  }
  valuesDisabled:boolean = true;
  activeFormComponent:string = '';
  questionTypeDefinitions = QuestionTypeDefinitionService;

  constructor(
    public surveyQuestionsService: SurveyQuestionsService,
    public events: Events,
    public formBuilder: FormBuilder,
    public actionSheetCtrl: ActionSheetController,
    public helperSurveyService: HelperSurveyService,
    public popoverCtrl: PopoverController
  )
  {
  }

  getQuestions()
  {
    this.surveyQuestionsService.getQuestions( this.survey.survey_id )
      .subscribe( data => {
        this.questions = data;
        this.survey.questions = this.questions;
        this.switchSurveyPage( this.activeSurveyPage );
      });
  }

  saveQuestion()
  {
    this.question.survey_id = this.survey.survey_id;
    this.question.page = this.activeSurveyPage;

    // If we're adding a new question, give it an order.
    if ( this.question.survey_question_id === 0 )
    {
      this.question.question_order = this.questions.length + 1;
    }

    this.question.status = 'Active';
    if ( this.question.extra )
    {
      this.question.extra = JSON.stringify( this.question.extra );
    }

    if ( this.question.question_type !== '' )
    {
      this.surveyQuestionsService.saveQuestion( this.question )
        .subscribe( data => {
          let d:any = data;
          // If we have answers for this question, then save them.
          if ( this.question.answers.length > 0 )
          {
            this.question.survey_question_id = d.survey_question_id;
            // Let's go ahead and strip blank answers out of the answer 
            // collection here.
            let j = this.question.answers.length;
            while ( j-- ) 
            {
              if ( this.question.answers[j].answer_text === '' )
              {
                this.question.answers.splice( j, 1 );
              }
            }
            this.surveyQuestionsService.saveQuestionAnswers( this.question )
              .subscribe( data => {
                this.getQuestions();
              });
          } else {
            this.getQuestions();
          }
          this.clearForm();

          this.events.publish('app:log', {
            message: 'Question has been saved.',
            status: 'success',
            type: 'save survey question'
          });

        });
    }
  }

  editQuestion( question )
  {
    this.question = question;
    this.switchQuestionTypeForm( this.question.question_type );
  }

  deleteQuestion()
  {
    this.surveyQuestionsService.deleteQuestion( this.question )
      .subscribe( data => {
        this.getQuestions();

        this.events.publish('app:log', {
          message: 'Question has been deleted.',
          status: 'warning',
          type: 'delete survey question'
        });
      });
  }

  isValuesDisabled()
  {
    return this.valuesDisabled;
  }

  switchQuestionTypeForm( questionType )
  {
    this.activeFormComponent = questionType;
  }

  presentDeleteQuestionActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this question?',
      buttons: [
        {
          text: 'Delete question',
          role: 'destructive',
          handler: () => {
            this.deleteQuestion();
            this.clearForm();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  clearForm()
  {
    this.question = {
      survey_question_id: 0,
      survey_id: 0,
      question_text: '',
      question_type: '',
      extra: '',
      answers: []
    }
    this.activeFormComponent = null;
  }

  reorderItems( indexes ) {
    this.questions = reorderArray(this.questions, indexes);
    let i = 1;
    for ( let q of this.questions )
    {
      q.question_order = i;
      i++;
    }

    this.surveyQuestionsService.saveQuestionOrder( this.questions, this.survey.survey_id )
      .subscribe( data => {});
  }

  switchSurveyPage( pageNumber )
  {
    let i = 0;
    for ( let q of this.survey.question_pages )
    {
      if ( q.page === pageNumber )
      {
        this.questions = this.helperSurveyService.attachQuestionTypeDefinitionsToQuestions( this.survey.question_pages[i].questions );
      }
      i++;
    }

    this.activeSurveyPage = pageNumber;
  }

  isActivePageButton( pageNumber )
  {
    if ( pageNumber === this.activeSurveyPage )
    {
      return true;
    }
    return false;
  }

  addNewSurveyPage()
  {
    this.activeSurveyPage = this.pageButtons.length + 1;
    this.pageButtons.push( { buttonIndex: this.activeSurveyPage } );
    this.surveyPage.page = this.activeSurveyPage;
    this.survey.questions.push( this.surveyPage );
    this.switchSurveyPage( this.activeSurveyPage );
  }

  deleteSurveyPage()
  {
    if ( this.questions.length === 0 )
    {
      // Just remove the page button and go back 1 page.
      this.activeSurveyPage = this.activeSurveyPage - 1;
      this.pageButtons.pop();
      this.survey.questions.pop();
      this.switchSurveyPage( this.activeSurveyPage );
    } else {
      this.presentDeleteSurveyPagePopover();
    }
  }

  presentDeleteSurveyPagePopover()
  {
    let popover = this.popoverCtrl.create( DeleteSurveyPagePopoverPage, {
      activeSurveyPage: this.activeSurveyPage
    });
    popover.present();

    popover.onDidDismiss(data => {
      if ( data !== null )
      {
        if ( data.action === 'moveToPage' )
        {
          this.surveyQuestionsService.moveQuestionsToPage( this.activeSurveyPage, data.moveToPage, this.survey.survey_id )
            .subscribe ( data => {
              this.getQuestions();
            });
        } else if ( data.action === 'deleteQuestions' ) {
          this.surveyQuestionsService.deleteAllQuestionsOnPage( this.activeSurveyPage, this.survey.survey_id )
            .subscribe( data => {
              this.getQuestions();
            });
        }

        // Remove the page button and go back 1 page.
        this.activeSurveyPage = this.activeSurveyPage - 1;
        this.pageButtons.pop();
        this.survey.questions.pop();
        this.switchSurveyPage( this.activeSurveyPage );
      }
    });
  }
  
  ngOnInit()
  {
    // Get questions for page 1.
    if ( this.survey.question_pages[0] !== undefined )
    {
      this.questions = this.helperSurveyService.attachQuestionTypeDefinitionsToQuestions( this.survey.question_pages[0].questions );
    }
    
    for ( let a = 1; a <= this.survey.question_pages.length; a++ )
    {
      this.pageButtons.push( { buttonIndex: a } );
    }
  }
}