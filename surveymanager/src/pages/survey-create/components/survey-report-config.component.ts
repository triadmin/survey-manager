import { 
  Component, 
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  Events,
  reorderArray,
  ActionSheetController
} from 'ionic-angular';

// Services
import { HelperService } from '../../../providers/helpers/helper-service';
import { HelperSurveyService } from '../../../providers/helpers/helper-survey-service';
import { SurveyReportElementService } from '../../../providers/surveys/survey-report-element-service';
import { ReportElementTypeDefinitionService } from '../../../providers/surveys/report-element-type-definition-service';

@Component({
  selector: 'component-survey-report-config',
  templateUrl: 'survey-report-config.component.html'
})

export class SurveyReportConfigComponent
{
  @Input() survey: any;
  @Output() updateSurvey = new EventEmitter<string>();

  reportElements = [];
  reportElement: any = {};
  questions = [];
  selectedQuestion: any = {};
  reportElementTypeDefinitions = ReportElementTypeDefinitionService;

  constructor(
    public events: Events,
    public actionSheetCtrl: ActionSheetController,
    public helperService: HelperService,
    public surveyReportElementService: SurveyReportElementService,
    public helperSurveyService: HelperSurveyService
  )
  {
    this.reportElement.is_filter_toggle = false;
    this.reportElement.is_in_list_view_toggle = false;
    this.reportElement.is_on_dashboard_toggle = false;
  }

  showSelectedQuestion()
  {
    console.log(this.selectedQuestion);
  }

  saveReportElement()
  {
    this.reportElement.guage_metric_is_average = 'No';

    // Convert our toggles
    this.reportElement.is_filter = this.helperService.booleanEnumToggle( this.reportElement.is_filter_toggle, 'Yes', 'No' );
    this.reportElement.is_in_list_view = this.helperService.booleanEnumToggle( this.reportElement.is_in_list_view_toggle, 'Yes', 'No' );
    this.reportElement.is_on_dashboard = this.helperService.booleanEnumToggle( this.reportElement.is_on_dashboard_toggle, 'Yes', 'No' );
    this.reportElement.survey_id = this.survey.survey_id;

    // Special occasion for gauge + rating_scale.
    if ( 
      this.reportElement.chart_type === 'gauge' && 
      this.selectedQuestion.question_type === 'rating-scale'
    ) {
      this.reportElement.guage_metric_is_average = 'Yes';
    }

    // Set selected question_id
    this.reportElement.survey_question_id = this.selectedQuestion.survey_question_id;

    this.surveyReportElementService.saveReportElement( this.reportElement )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Report element has been saved.',
          status: 'success',
          type: 'save report element'
        });

        this.getReportElements();
        this.clearForm();
      });

  }

  editReportElement( reportElement )
  {
    this.reportElement = reportElement;

    // Convert our enum values to toggle boolean.
    this.reportElement.is_filter_toggle = this.helperService.booleanEnumToggle( this.reportElement.is_filter, 'Yes', 'No' );
    this.reportElement.is_in_list_view_toggle = this.helperService.booleanEnumToggle( this.reportElement.is_in_list_view, 'Yes', 'No' );
    this.reportElement.is_on_dashboard_toggle = this.helperService.booleanEnumToggle( this.reportElement.is_on_dashboard, 'Yes', 'No' );

    // Get question obj from question_id
    for ( let q of this.survey.questions )
    {
      if ( q.survey_question_id == this.reportElement.survey_question_id )
      {
        this.selectedQuestion = q;
      }
    }
    this.questions = this.helperSurveyService.attachQuestionTypeDefinitionsToQuestions( this.questions );

    // If there's an answer_id, get the answer obj.
  }

  getReportElements()
  {
    this.surveyReportElementService.getReportElements( this.survey.survey_id )
      .subscribe( data => {
        this.reportElements = this.helperSurveyService.attachReportElementTypeDefinitionsToReportElements( data );
      });
  }

  deleteReportElement()
  {
    this.surveyReportElementService.deleteReportElement( this.reportElement )
      .subscribe( data => {
        this.getReportElements();

        this.events.publish('app:log', {
          message: 'Report element has been deleted.',
          status: 'warning',
          type: 'delete report element'
        });
      });
  }

  setLabel()
  {
    this.reportElement.label = this.helperService.stripHtml( this.selectedQuestion.question_text );
  }

  clearForm()
  {
    this.selectedQuestion = {};
    this.reportElement = {
      survey_id: 0,
      label: '',
      chart_type: '',
      guage_metric_answer_id: 0,
      guage_warning: '',
      is_filter_toggle: false,
      is_in_list_view_toggle: false,
      is_on_dashboard_toggle: false
    }
  }

  presentDeleteReportElementActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this report element?',
      buttons: [
        {
          text: 'Delete report element',
          role: 'destructive',
          handler: () => {
            this.deleteReportElement();
            this.clearForm();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  reorderItems( indexes ) {
    this.reportElements = reorderArray( this.reportElements, indexes );
    let i = 1;
    for ( let re of this.reportElements )
    {
      re.element_order = i;
      i++;
    }

    this.surveyReportElementService.saveReportElementOrder( this.reportElements, this.survey.survey_id )
      .subscribe( data => {});
  }

  ngOnInit()
  {
    if ( this.survey.questions.length > 0 )
    {
      for ( let q of this.survey.questions )
      {
        this.questions.push( q );
      }
      this.questions = this.helperSurveyService.attachQuestionTypeDefinitionsToQuestions( this.questions );
    }

    this.getReportElements();
  }
}