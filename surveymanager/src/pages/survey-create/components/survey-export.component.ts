import { 
  Component, 
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  Events
} from 'ionic-angular';

// Services
import { SurveyAttemptsService } from '../../../providers/surveys/survey-attempts-service';
import { HelperService } from '../../../providers/helpers/helper-service';
import { HelperCsvService } from '../../../providers/helpers/helper-csv-service';
import { RegionsService } from '../../../providers/surveys/regions-service';

@Component({
  selector: 'component-survey-export',
  templateUrl: 'survey-export.component.html'
})

export class SurveyExportComponent
{
  @Input() survey: any;
  @Output() updateSurvey = new EventEmitter<string>();

  userRegionMapTnf = [
    {'usersId': 1374, 'region': 'Region 1'},
    {'usersId': 1441, 'region': 'Region 7'},
    {'usersId': 2156, 'region': 'Region 3'},
    {'usersId': 2152, 'region': 'Region 3'},
    {'usersId': 2172, 'region': 'Region 1'},
    {'usersId': 1944, 'region': 'Region 1'},
    {'usersId': 2195, 'region': 'Region 11'},
    {'usersId': 1977, 'region': 'Region 4'},
    {'usersId': 1978, 'region': 'Region 6'},
    {'usersId': 1991, 'region': 'Region 2'},
    {'usersId': 1994, 'region': 'Region 3'},
    {'usersId': 1996, 'region': 'Region 5'},
    {'usersId': 1997, 'region': 'Region 1'},
    {'usersId': 2003, 'region': 'Region 8'},
    {'usersId': 2095, 'region': 'Region 10'},
    {'usersId': 2352, 'region': 'Region 12'},
    {'usersId': 2364, 'region': 'Region 1'},
    {'usersId': 2415, 'region': 'Region 1'},
    {'usersId': 2419, 'region': 'Region 1'},
    {'usersId': 2420, 'region': 'Region 1'},
    {'usersId': 2509, 'region': 'Region 4'},
    {'usersId': 2341, 'region': 'Region 6'},
    {'usersId': 2354, 'region': 'Region 1'}
  ];

  userRegionMapOdds = [
    {'usersId': 2457, 'region': 'Eastern'},
    {'usersId': 2456, 'region': 'Central'},
    {'usersId': 2515, 'region': 'Mid Valley'},
    {'usersId': 2458, 'region': 'Mid Valley'},
    {'usersId': 2459, 'region': 'Mid Valley'},
    {'usersId': 2460, 'region': 'Metro'},
    {'usersId': 2461, 'region': 'Southern'},
    {'usersId': 2516, 'region': 'Mid Valley'},
    {'usersId': 2506, 'region': 'Mid Valley'},
    {'usersId': 2533, 'region': 'Northwest and North-Central'},
    {'usersId': 2534, 'region': 'Southern and Central'},
    {'usersId': 2535, 'region': 'Metro and Northeast'},
    {'usersId': 2536, 'region': 'Metro and Northeast'},
    {'usersId': 2551, 'region': 'Metro'}
  ];

  constructor(
    public events: Events,
    public helperService: HelperService,
    public surveyAttemptsService: SurveyAttemptsService,
    public helperCsvService: HelperCsvService,
    public regionsService: RegionsService
  )
  {
  }

  exportSurveyData()
  {
    // Get survey attempts
    this.surveyAttemptsService.getSurveyAttempts( this.survey.survey_id )
      .subscribe( data => {
        let csvFileName = this.survey.title + '.csv';
        this.helperCsvService.exportSurveyAttempts( this.survey, data, csvFileName );
      });
  }

  // TEMP METHOD.
  // Take location data from question responses and 
  // migrate it into location column in survey_attempts table.
  // For TNF and ODDS surveys.
  configureLocationData()
  {
    // Find AreaInvolved question type and get its ID.
    let areaInvolvedQId = 0;
    for ( let q of this.survey.questions )
    {
      if ( q.question_identifier === 'AreaInvolved' ) 
      {
        areaInvolvedQId = q.survey_question_id;
      }
    }

    // Get region set.
    this.regionsService.getRegion( this.survey.region_id )
      .subscribe( data => {
        this.getSurveyAttempts( data, areaInvolvedQId );
      });
  }

  getSurveyAttempts( regionSet, areaInvolvedQId )
  {
    let regionItems = regionSet.region_items;
    let updatedLocationArray = [];

    // Get all survey attempts.
    this.surveyAttemptsService.getSurveyAttempts( this.survey.survey_id )
      .subscribe( data => {
        for ( let sa of data )
        {
          for ( let r of sa.responses )
          {
            // Get user region
            let userRegion = '';
            for ( let ur of this.userRegionMapTnf )
            {
              if ( ur.usersId === sa.survey_user_id )
              {
                userRegion = ur.region;
              }

            }
            if ( r.survey_question_id === areaInvolvedQId )
            {
              let regionData = [];
              let place = '';
              let coordsObj = [];
              regionData.push({title: userRegion});
              let locationObj = JSON.parse(r.text);

              // Look for counties
              if ( locationObj.counties !== undefined )
              {
                for ( let c of locationObj.counties )
                {
                  regionData.push({title: c});
                }
              }

              // Look for districts
              if ( locationObj.districts !== undefined )
              {
                for ( let d of locationObj.districts )
                {
                  regionData.push({title: d});
                }
              }

              // Look for school
              if ( locationObj.school !== undefined )
              {
                place = locationObj.school;
              }

              // Look for coords
              if ( locationObj.coords !== undefined )
              {
                if ( locationObj.coords.county !== undefined )
                {
                  for( let cc of locationObj.coords.county )
                  {
                    coordsObj.push( cc );
                  }
                }
                if ( locationObj.coords.district !== undefined )
                {
                  for( let cd of locationObj.coords.district )
                  {
                    coordsObj.push( cd );
                  }
                }
                if ( locationObj.coords.school !== undefined )
                {
                  for( let cs of locationObj.coords.school )
                  {
                    coordsObj.push( cs );
                  }
                }
              }

              for( let ri of regionItems )
              {
                for ( let rd of regionData )
                {
                  if ( rd !== undefined )
                  {
                    if ( rd.title === ri.title )
                    {
                      rd.region_list_item_id = ri.region_list_item_id;
                    }
                  }
                }
              }

              let finalObj = {
                region_set_nodes: regionData,
                place: place,
                coords: coordsObj
              };
              sa.location = JSON.stringify(finalObj);
              /*
              console.log(finalObj);
              console.log(locationObj)
              */
            }
          }
        }

        this.updateSurveyAttempts(data);
      });
  }

  updateSurveyAttempts(surveyattempts)
  {
    this.surveyAttemptsService.updateSurveyAttemptsCollection( surveyattempts, this.survey.survey_id ).subscribe( data => {

    });
  }

  ngOnInit()
  {
  }
}