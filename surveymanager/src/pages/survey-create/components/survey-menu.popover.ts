import { Component } from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams,
  Events
} from 'ionic-angular';

@Component({
  template: `
    <ion-item>
      <button ion-button icon-start block color="super-light-gray" (click)="closePopover('view-survey-client')">
        <ion-icon name="search"></ion-icon> View Survey
      </button>
    </ion-item>
  `
})
export class SurveyMenuPopoverPage {
  // Permissions 
  //showCloseButton:boolean = false;
  //showNewSurveyButton:boolean = true;

  constructor(
    private viewCtrl: ViewController
  )
  {
    this.menuPermissions();
  }

  menuPermissions()
  {
    /*
    if ( this.userPermissionsService.userHasRole( ['survey-manager-admin','survey-manager-project-level-admin'] ) )
    {
      this.showCloseButton = true;
    }

    if ( this.userPermissionsService.userHasRole( ['survey-manager-read-only'] ) )
    {
      this.showNewSurveyButton = false;
    }
    */
  }

  closePopover( selectedItem = null )
  {
    this.viewCtrl.dismiss( selectedItem );
  }
}
