import { 
  Component, 
  EventEmitter, 
  Input, 
  Output 
} from '@angular/core';

@Component({
  selector: 'component-menu',
  templateUrl: 'menu.component.html'
})

//
// MenuComponent (child) communicates with the 
// InspectionAddEditComponent (parent) through EventEmitter.
// This seems ok because the parent should be in charge of which
// form is currently being displayed.
//
export class MenuComponent
{
  @Output() menuChange = new EventEmitter<string>();
  @Input() purpose: string;
  @Input() activeMenuItem: string = '';
  @Input() surveyId: number = 0;

  constructor() {
    
  }

  handleMenuClick( menuItem )
  {
    this.activeMenuItem = menuItem;
    this.menuChange.emit( menuItem );
  }

  ngOnInit()
  {
    // Which menu should we display?
    if ( this.purpose === 'survey-add-edit' )
    {
      this.activeMenuItem = 'surveyInfo';
    } else {
      this.activeMenuItem = 'classroomEvaluation';
    }
  }
}