import { Component } from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams 
} from 'ionic-angular';

@Component({
  template: `
    <ion-list>
      <ion-item>
        <h2>Move Questions?</h2>
        <p>You are trying to delete a page with questions. Would you like to move these questions to the previous page?</p>
      </ion-item>

      <ion-item text-center>
        <button ion-button (click)="closePopover('moveToPage')">Move Questions</button>
      </ion-item>

      <ion-item text-center>
        <button ion-button color="danger" (click)="closePopover('deleteQuestions')">Delete Questions Too</button>
      </ion-item>
    </ion-list>
  `
})
export class DeleteSurveyPagePopoverPage {
  pageToDelete;
  pageToMoveTo;

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  )
  {
    this.pageToDelete = this.navParams.get('activeSurveyPage');
    this.pageToMoveTo = this.pageToDelete - 1;
  }

  closePopover( action )
  {
    let data = {
      moveToPage: this.pageToMoveTo,
      action: action
    };
    this.viewCtrl.dismiss( data );
  }

}
