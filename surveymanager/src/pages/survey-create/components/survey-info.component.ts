import { 
  Component, 
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";
import {
  LoadingController, 
  Events,
  ActionSheetController,
  NavController
} from 'ionic-angular';

// Services
import { SurveysService } from '../../../providers/surveys/surveys-service';
import { Config } from '../../../providers/config/config';
import { FilesService } from '../../../providers/files/files-service';
import { HelperService } from '../../../providers/helpers/helper-service';
import { RegionsService } from '../../../providers/surveys/regions-service';

@Component({
  selector: 'component-survey-info',
  templateUrl: 'survey-info.component.html'
})

export class SurveyInformationComponent
{
  @Input() survey: any;
  @Output() updateSurvey = new EventEmitter<string>();

  // RichText Editor
  survey_description: FormControl;

  haveSurveyData:boolean = false;
  region_sets = [];

  logoFile:any = {
    file_thumb_url: ''
  };

  tri_projects:any = Config.TRI_PROJECTS;

  // Public Display Options
  websiteDisplayOptions: any = [
    {
      label: 'schedule',
      label_display: 'Session Schedule',
      value: true
    },
    {
      label: 'presenters',
      label_display: 'List of Presenters',
      value: false
    },
    {
      label: 'presenter_registration',
      label_display: 'Presenter Registration Form',
      value: false
    },
    {
      label: 'maps',
      label_display: 'Venue Maps',
      value: true
    },
    {
      label: 'lodging',
      label_display: 'Lodging Information',
      value: false
    },
    {
      label: 'evaluation',
      label_display: 'Session Evaluation',
      value: false
    },
    {
      label: 'vendors',
      label_display: 'List of Vendors',
      value: false
    },
    {
      label: 'vendor_registration',
      label_display: 'Vendor Registration Form',
      value: false
    }
  ];

  includeInMobileApp: boolean = true;

  // force_tab = true means option MUST be in a tab.
  // force_tab = false means option may be in a tab if tab count < 5.
  // Otherwise option is presented as a page in the OTHER option in top-right of app.
  mobileDisplayOptions: any = [
    {
      label: 'schedule',
      label_display: 'Session Schedule',
      label_tab: 'Schedule',
      force_tab: true,
      value: true
    },
    {
      label: 'presenters',
      label_display: 'List of Presenters',
      label_tab: 'Presenters',
      force_tab: false,
      value: false
    },
    {
      label: 'maps',
      label_display: 'Venue Maps',
      label_tab: 'Maps',
      force_tab: false,
      value: true
    },
    {
      label: 'lodging',
      label_display: 'Lodging Information',
      label_tab: 'Lodging',
      force_tab: false,
      value: false
    },
    {
      label: 'evaluation',
      label_display: 'Session Evaluation',
      label_tab: 'Evaluation',
      force_tab: false,
      value: false
    },
    {
      label: 'vendors-list',
      label_display: 'List of Vendors',
      label_tab: 'Vendors',
      force_tab: false,
      value: false
    },
    {
      label: 'vendors-passbook',
      label_display: 'Vendor Passbook',
      label_tab: 'Passbook',
      force_tab: false,
      value: false
    },
    {
      label: 'chat',
      label_display: 'Chat and Photo Stream',
      label_tab: 'Chat',
      force_tab: true,
      value: true
    },
    {
      label: 'dining',
      label_display: 'List of Restaurants',
      label_tab: 'Dining',
      force_tab: false,
      value: true
    }
  ];
  mobileColors: any = {
    headerBar: '',
    tabBar: '',
    buttons: '',
    color4: ''
  };

  constructor(
    public formBuilder: FormBuilder,
    public surveysService: SurveysService,
    public filesService: FilesService,
    public loadingCtrl: LoadingController,
    public events: Events,
    public helperService: HelperService,
    public actionSheetCtrl: ActionSheetController,
    public navCtrl: NavController,
    public regionsService: RegionsService
  )
  {}

  saveSurveyInformation()
  {
    // Get rich-text field value
    this.survey.description = this.survey_description.value;

    // Do some work on our website/mobile setting toggles.
    this.survey.include_in_mobile_app = 'Yes';
    if ( this.includeInMobileApp === false )
    {
      this.survey.include_in_mobile_app = 'No';
    }
    this.survey.config_mobile_colors = JSON.stringify( this.mobileColors );

    // If rich text field is empty and we're updating, GORM won't save the change
    // unless we put a space in the value.  
    if ( this.survey.description === null || this.survey.description.length === 0 )
    {
      this.survey.description = ' ';
    }

    // TRI project Id
    this.survey.tri_project_id = +this.survey.tri_project_id;
    // Region Id
    this.survey.region_id = +this.survey.region_id;

    // Copy this.survey object so we can remove elements
    // we don't want sent to the server for saving.
    let surveyCopy = JSON.parse( JSON.stringify( this.survey ) );
    delete surveyCopy.files;
    /*
    delete conferenceCopy.presenters;
    delete conferenceCopy.sessions;
    delete conferenceCopy.session_times;
    delete conferenceCopy.strands;
    delete conferenceCopy.venues;
    delete conferenceCopy.vendors;
    delete conferenceCopy.vendor_options;
    */

    this.surveysService.saveSurvey( surveyCopy )
      .subscribe( data => {
        this.haveSurveyData = true;
        this.updateSurvey.emit( this.survey );

        this.events.publish('app:log', {
          message: 'Survey information saved',
          status: 'success',
          type: 'save survey information'
        }); 
      });
  }  

  logoUploaded( fileObj )
  {
    let newFileObj:any = this.helperService.makeFileObject( fileObj );
    newFileObj.file_purpose = 'survey_logo';

    if ( this.haveSurveyData === false )
    {
      // Create new survey entry first and get conference_id
      this.surveysService.saveSurvey( this.survey )
        .subscribe( data => {
          this.survey = data;
          this.haveSurveyData = true;
          this.updateSurvey.emit( this.survey );

          newFileObj.survey_id = this.survey.survey_id;
          this.filesService.saveFileEntry( newFileObj )
            .subscribe( data => {
              this.logoFile = data;
            });
        });
    } else {
      newFileObj.survey_id = this.survey.survey_id;
      this.filesService.saveFileEntry( newFileObj )
        .subscribe( data => {
          this.logoFile = data;
        });
    }
  }

  deleteLogo()
  {
    this.filesService.deleteFileEntry( this.logoFile )
      .subscribe( data => {
        this.logoFile = {};
        this.logoFile.file_thumb_url = '';
      });
  }

  setRichText()
  {
    // Init the richText component.
    this.survey_description = this.formBuilder.control(this.survey.description);    
  }

  deleteSurvey()
  {
    this.survey.status = 'Deleted';
    this.surveysService.saveSurvey( this.survey )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Survey has been deleted.',
          status: 'warning',
          type: 'delete survey'
        });

        this.events.publish('survey:delete', {
          surveyId: this.survey.survey_id
        });
        this.navCtrl.pop();
      });
  }

  presentDeleteSurveyActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this survey?',
      buttons: [
        {
          text: 'Delete survey',
          role: 'destructive',
          handler: () => {
            this.deleteSurvey();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  toggleStatus()
  {
    if ( this.survey.status === 'Open' )
    {
      this.survey.status = 'Closed';
    } else {
      this.survey.status = 'Open';
    }
    this.saveSurveyInformation();
  }

  getStatusColor()
  {
    if ( this.survey.status === 'Closed' )
    {
      return 'warning';
    }
    return 'success';
  }

  getStatusIcon()
  {
    if ( this.survey.status === 'Closed' )
    {
      return 'ios-close-circle';
    }
    return 'ios-thumbs-up';
  }

  ngOnInit()
  {
    this.setRichText();
    if ( typeof this.survey.survey_id !== 'undefined' )
    {
      this.haveSurveyData = true;

      // Set toggles for website and mobile settings.      
      if ( this.survey.config_mobile_colors.length > 0 )
      {
        this.mobileColors = JSON.parse(this.survey.config_mobile_colors);
      }

      this.includeInMobileApp = true;
      if ( this.survey.include_in_mobile_app === 'No' )
      {
        this.includeInMobileApp = false;
      }

      // See if we have a conference logo to display.
      for ( let file of this.survey.files )
      {
        if ( file.file_purpose === 'survey_logo' )
        {
          this.logoFile = file;
        }
      }
    }

    // Get region sets
    this.regionsService.getRegions()
      .subscribe( data => {
        this.region_sets = data;
      });
  }
}  