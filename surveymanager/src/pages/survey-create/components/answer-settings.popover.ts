import { Component } from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams 
} from 'ionic-angular';

@Component({
  template: `
    <ion-list>
      <ion-item>
        <h2>We'll have some settings here soon</h2>
      </ion-item>
      <ion-item>
        <p>Selecting this answer skips to question:</p>
      </ion-item>
      <ion-item>
        <p>Selecting this answer exits the survey:</p>
      </ion-item>
      <ion-item text-center>
        <button ion-button (click)="closePopover()">Ok</button>
      </ion-item>
    </ion-list>
  `
})
export class AnswerSettingsPopoverPage {
  answer:any = {};

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  )
  {
    this.answer = this.navParams.get('answer');
  }

  closePopover( selectedItem )
  {
    this.viewCtrl.dismiss( selectedItem );
  }

}
