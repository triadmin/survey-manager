import { 
  Component, 
  EventEmitter,
  Input,
  Output
} from '@angular/core';
import {
  Events
} from 'ionic-angular';

// Services
import { HelperService } from '../../../providers/helpers/helper-service';

@Component({
  selector: 'component-survey-settings',
  templateUrl: 'survey-settings.component.html'
})

export class SurveySettingsComponent
{
  @Input() survey: any;
  @Output() updateSurvey = new EventEmitter<string>();

  constructor(
    public events: Events,
    public helperService: HelperService
  )
  {
  }

  saveSurvey()
  {
  }

  ngOnInit()
  {
  }
}