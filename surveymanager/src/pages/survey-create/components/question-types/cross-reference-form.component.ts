import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

// Providers
import { Config } from '../../../../providers/config/config';
import { UserPermissionsService } from '../../../../providers/users/user-permissions-service';
import { SurveysService } from '../../../../providers/surveys/surveys-service';
import { HelperSurveyService } from '../../../../providers/helpers/helper-survey-service';

@Component({
  selector: 'component-cross-reference-form',
  template: `
    <div margin-left>
      <ion-label color="label" stacked>Enter Question Text</ion-label>
      <rich-text 
        #richText 
        [editorHeight]="'20vh'" 
        [formControlItem]="rt_question_text" 
        placeholder=""
        (textChange)="getRichText($event)"
      ></rich-text>
    </div>
    <ion-item>
      <ion-label color="label" stacked>Select survey for cross reference.</ion-label>
      <ion-select
        [(ngModel)]="selectedSurvey" 
        [compareWith]="surveyCompareFn"
        (ionChange)="chooseSurvey($event)" 
      >
        <ion-option *ngFor="let survey of surveys" [value]="survey">{{ survey.title }}</ion-option>
      </ion-select>
    </ion-item>

    <ion-item>
      <ion-label color="label" stacked>Select question for cross reference.</ion-label>
      <ion-select [(ngModel)]="question.extra.questionId">
        <ng-container *ngFor="let q of questions">
          <ion-option *ngIf="q.questionTypeDefinition.reportable == 'Yes'" [value]="q.survey_question_id" [innerHTML]="q.question_text"></ion-option>
        </ng-container>
      </ion-select>
    </ion-item>

    <ion-item>
      <ion-label>Show only selected answers</ion-label>
      <ion-toggle [(ngModel)]="question.extra.showOnlySelectedAnswers" (ionChange)="!question.extra.showOnlySelectedAnswers"></ion-toggle>
    </ion-item>
  `
})

export class CrossReferenceFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  // RichText Editor
  rt_question_text: FormControl;

  config = Config;
  surveys = [];
  questions = [];
  selectedSurvey = {};

  constructor(
    private formBuilder: FormBuilder,
    private userPermissionsService: UserPermissionsService,
    private surveysService: SurveysService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  chooseSurvey( ev )
  {
    this.questions = [];
    this.question.extra = {
      surveyId: ev.survey_id,
      surveyTitle: ev.title
    }

    this.surveysService.getSurvey( ev.survey_id )
      .subscribe( data => {
        this.questions.length = 0;
        this.questions = this.helperSurveyService.attachQuestionTypeDefinitionsToQuestions( data.questions );
      });
  }

  getRichText()
  {
    this.question.question_text = this.rt_question_text.value;
  }

  setRichText()
  {
    // Init the richText component.
    this.rt_question_text = this.formBuilder.control(this.question.question_text);    
  }

  surveyCompareFn( s1, s2 )
  {
    return s1 && s2 ? s1.survey_id === s2.survey_id : s1 === s2;
  }

  ngOnInit()
  {
    // Determine which surveys we can choose from.
    if ( this.userPermissionsService.userHasRole( ['survey-manager-project-level-admin' ] ))
    {
      this.surveys = Config.USER_APP_RECORD.surveys;
    } else {
      this.surveys = Config.USER_APP_RECORD.surveys;
    }

    this.setRichText();

    if ( this.question.extra !== '' && this.question.extra !== null )
    {
      this.question.extra = JSON.parse( this.question.extra );
    }

    if ( this.question.extra.surveyId === undefined )
    {
      this.question.extra = {
        surveyId: 0,
        surveyTitle: '',
        showOnlySelectedAnswers: true
      };
    } else {
      for ( let survey of this.surveys )
      {
        if ( +this.question.extra.surveyId === survey.survey_id )
        {
          this.selectedSurvey = survey;
        }
      }
    }
  }
}
