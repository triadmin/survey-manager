import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

// Services
import { RegionsService } from '../../../../providers/surveys/regions-service';

@Component({
  selector: 'component-region-form',
  template: `
    <div margin-left>
      <ion-label color="label" stacked>Enter Question Text</ion-label>
      <rich-text 
        #richText 
        [editorHeight]="'20vh'" 
        [formControlItem]="rt_question_text" 
        placeholder=""
        (textChange)="getRichText($event)"
      ></rich-text>
    </div>
    <ion-item>
      <ion-label color="label" stacked>Select Region Set</ion-label>
      <ion-select [(ngModel)]="question.extra.region_set_id" (ionChange)="setQuestionExtra()">
        <ion-option *ngFor="let r of regionSets" value="{{ r.region_id }}">{{ r.title }}</ion-option>
      </ion-select>
    </ion-item>
    <ion-item>
      <ion-label>Limit selection to user assigned region</ion-label>
      <ion-toggle [(ngModel)]="question.extra.user_assigned_limit" (ionChange)="setQuestionExtra()"></ion-toggle>
    </ion-item>
  `
})

export class RegionFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  regionSets = [];
  user_assigned_limit = true;
  region_set_id = 0;

  // RichText Editor
  rt_question_text: FormControl;

  constructor(
    public formBuilder: FormBuilder,
    public regionsService: RegionsService
  ) {
  }

  getRegionSets()
  {
    this.regionsService.getRegions()
      .subscribe( data => {
        this.regionSets = data;
      });
  }

  setQuestionExtra()
  {
    /*
    let extra = {
      region_set_id: this.region_set_id,
      user_assigned_limit: this.user_assigned_limit
    };

    this.question.extra = extra;

    console.log(extra);
    */
  }

  getRichText()
  {
    this.question.question_text = this.rt_question_text.value;
  }

  setRichText()
  {
    // Init the richText component.
    this.rt_question_text = this.formBuilder.control(this.question.question_text);    
  }

  ngOnInit()
  {
    this.getRegionSets();
    this.setRichText();

    // Set form fields
    if ( this.question.extra.length > 0 )
    {
      this.question.extra = JSON.parse( this.question.extra );
    }
  }
}