import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";
import {
  reorderArray,
  PopoverController
} from 'ionic-angular';

// Providers
import { SurveyQuestionsService } from '../../../../providers/surveys/survey-questions-service';

// Popovers
import { AnswerSettingsPopoverPage } from '../answer-settings.popover';

@Component({
  selector: 'component-list-headers-form',
  template: `
  <div margin-left>
    <ion-label color="label" stacked>Enter Question Text</ion-label>
    <rich-text 
      #richText 
      [editorHeight]="'20vh'" 
      [formControlItem]="rt_question_text" 
      placeholder=""
      (textChange)="getRichText($event)"
    ></rich-text>
  </div>
  <ion-item>
    <ion-label color="label" stacked>Add Question Answers</ion-label>
  </ion-item>
  <ion-list reorder="true" (ionItemReorder)="reorderItems($event)">
    <ng-container *ngFor="let a of question.answers; let i = index;">
      <ion-item [style.background-color]="isHeader(a)">
        <ion-input type="text" [(ngModel)]="a.answer_text" [placeholder]="i + 1"></ion-input>
        <div item-end *ngIf="a.answer_text.length > 0">
          <button ion-button clear icon-only small (click)="presentSettingsPopover(a, $event)">
            <ion-icon name="settings" color="primary"></ion-icon>
          </button>
          <button ion-button clear icon-only small (click)="deleteAnswer(a)">
            <ion-icon name="trash" color="danger"></ion-icon>
          </button>
        </div>
      </ion-item>
      <ion-item *ngIf="a.survey_answer_id == 0">
        <ion-label>This item is a header</ion-label>
        <ion-toggle (ionChange)="toggleIsHeader($event, a)"></ion-toggle>
      </ion-item>
    </ng-container>
  </ion-list>
  <ion-item>
    <button ion-button small color="primary" (click)="addNewAnswer()">
      Add another answer
    </button>
  </ion-item>
  `
})

export class ListWithHeadersFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  // RichText Editor
  rt_question_text: FormControl;

  constructor(
    public formBuilder: FormBuilder,
    public surveyQuestionsService: SurveyQuestionsService,
    public popoverCtrl: PopoverController
  ) {
  }

  getRichText()
  {
    this.question.question_text = this.rt_question_text.value;
  }

  setRichText()
  {
    // Init the richText component.
    this.rt_question_text = this.formBuilder.control(this.question.question_text);    
  }

  addNewAnswer()
  {
    let answerObj = {
      survey_answer_id: 0,
      aid: this.question.answers.length + 1,
      answer_text: ''
    }
    this.question.answers.push( answerObj );
  }

  deleteAnswer( answer )
  {
    this.surveyQuestionsService.deleteAnswer( this.question, answer )
      .subscribe( data => {
        // Remove answer from collection.
        let j = this.question.answers.length;
        while ( j-- ) 
        {
          if ( this.question.answers[j].survey_answer_id === answer.survey_answer_id )
          {
            this.question.answers.splice( j, 1 );
          }
        }
      });
  }

  reorderItems( indexes )
  {
    this.question.answers = reorderArray(this.question.answers, indexes);
    let i = 1;
    for ( let a of this.question.answers )
    {
      a.answer_order = i;
      i++;
    }

    this.surveyQuestionsService.saveAnswerOrder( this.question )
      .subscribe( data => {});
  }

  presentSettingsPopover( answer, ev )
  {
    let popover = this.popoverCtrl.create( AnswerSettingsPopoverPage, {
      answer: answer
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {

    });
  }

  isHeader( a )
  {
    if ( a.extra === 'header' )
    {
      return '#eeeeee';
    }
    return '';
  }

  toggleIsHeader( ev, a )
  {
    if ( ev.checked )
    {
      a.extra = 'header';
    } else {
      a.extra = '';
    }
  }

  ngOnInit()
  {
    this.setRichText();

    // If we're adding a new question, pre-populate answers with blanks.
    if ( typeof this.question.survey_question_id === 'undefined' || this.question.survey_question_id === 0 )
    {
      this.question.answers = [];
      for ( var a = 0; a <= 4; a++ )
      {
        let answerObj = {
          aid: a,
          answer_text: ''
        }
        this.question.answers.push( answerObj );
      }
    }
  }
}