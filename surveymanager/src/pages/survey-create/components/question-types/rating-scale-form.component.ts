import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

@Component({
  selector: 'component-rating-scale-form',
  template: `
  <div margin-left>
    <ion-label color="label" stacked>Enter Question Text</ion-label>
    <rich-text 
      #richText 
      [editorHeight]="'20vh'" 
      [formControlItem]="rt_question_text" 
      placeholder=""
      (textChange)="getRichText($event)"
    ></rich-text>
  </div>
  <ion-item>
    <ion-label stacked color="label">Select scale size</ion-label>
    <ion-select [(ngModel)]="question.extra.scale_size">
      <ion-option value="3">3</ion-option>
      <ion-option value="4">4</ion-option>
      <ion-option value="5">5</ion-option>
      <ion-option value="6">6</ion-option>
    </ion-select>
  </ion-item>
  `
})

export class RatingScaleFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  // RichText Editor
  rt_question_text: FormControl;

  constructor(
    public formBuilder: FormBuilder
  ) {
  }

  getRichText()
  {
    this.question.question_text = this.rt_question_text.value;
  }

  setRichText()
  {
    // Init the richText component.
    this.rt_question_text = this.formBuilder.control(this.question.question_text);    
  }

  setScaleSize( scale_size )
  {
    this.question.extra.scale_size = scale_size;
  }

  ngOnInit()
  {
    if ( this.question.extra !== '' && this.question.extra !== null )
    {
      this.question.extra = JSON.parse( this.question.extra );
    }
    if ( this.question.extra.scale_size === undefined )
    {
      this.question.extra = {
        scale_size: '3'
      };
    }
    this.setRichText();
  }
}