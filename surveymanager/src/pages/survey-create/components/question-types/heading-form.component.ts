import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'component-heading-form',
  template: `
    <ion-item>
      <ion-label color="label" stacked>Add Heading Text</ion-label>
      <ion-input type="text" [(ngModel)]="question.question_text"></ion-input>
    </ion-item>
  `
})

export class HeadingFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();
}
