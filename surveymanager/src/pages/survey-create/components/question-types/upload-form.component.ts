import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

@Component({
  selector: 'component-upload-form',
  template: `
    <div margin-left>
      <ion-label color="label" stacked>Enter Question Text</ion-label>
      <rich-text 
        #richText 
        [editorHeight]="'20vh'" 
        [formControlItem]="rt_question_text" 
        placeholder=""
        (textChange)="getRichText($event)"
      ></rich-text>
    </div>
  `
})

export class UploadFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  // RichText Editor
  rt_question_text: FormControl;

  constructor(
    public formBuilder: FormBuilder
  ) {
  }

  getRichText()
  {
    this.question.question_text = this.rt_question_text.value;
  }

  setRichText()
  {
    // Init the richText component.
    this.rt_question_text = this.formBuilder.control(this.question.question_text);    
  }

  ngOnInit()
  {
    this.setRichText();
  }
}