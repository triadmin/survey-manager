import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'component-survey-attempt-form',
  template: `
    <ion-item>
      <ion-label>Show follow up questions</ion-label>
      <ion-toggle [(ngModel)]="question.extra.showFollowUp" (ionChange)="toggleFollowUp()"></ion-toggle>
    </ion-item>
  `
})

export class SurveyAttemptFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  constructor() {
  }

  toggleFollowUp()
  {
    !this.question.extra.showFollowUp;
  }

  ngOnInit()
  {
    if ( this.question.extra !== '' && this.question.extra !== null )
    {
      this.question.extra = JSON.parse( this.question.extra );
    }

    if ( this.question.extra.showFollowUp === undefined )
    {
      this.question.extra = {
        showFollowUp: false
      };
    }
  }
}