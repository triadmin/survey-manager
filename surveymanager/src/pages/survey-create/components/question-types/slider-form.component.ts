import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

@Component({
  selector: 'component-slider-form',
  template: `
    <div margin-left>
      <ion-label color="label" stacked>Enter Question Text</ion-label>
      <rich-text 
        #richText 
        [editorHeight]="'20vh'" 
        [formControlItem]="rt_question_text" 
        placeholder=""
        (textChange)="getRichText($event)"
      ></rich-text>
    </div>
    <ion-item>
      <ion-label stacked>Minimum Value</ion-label>
      <ion-input type="number" text-right [(ngModel)]="question.extra.valueMin"></ion-input>
    </ion-item>
    <ion-item>
      <ion-label stacked>Maximum Value</ion-label>
      <ion-input type="number" text-right [(ngModel)]="question.extra.valueMax"></ion-input>
    </ion-item>
    <ion-item>
      <ion-label stacked>Number of Steps</ion-label>
      <ion-input type="number" text-right [(ngModel)]="question.extra.valueSteps"></ion-input>
    </ion-item>
  `
})

export class SliderFormComponent {
  @Input() question: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  // RichText Editor
  rt_question_text: FormControl;

  constructor(
    public formBuilder: FormBuilder
  ) {
  }

  getRichText()
  {
    this.question.question_text = this.rt_question_text.value;
  }

  setRichText()
  {
    // Init the richText component.
    this.rt_question_text = this.formBuilder.control(this.question.question_text);    
  }

  ngOnInit()
  {
    if ( this.question.extra !== '' && this.question.extra !== null )
    {
      this.question.extra = JSON.parse( this.question.extra );
    } else {
      this.question.extra = {
        valueMin: 1,
        valueMax: 10,
        valueSteps: 5
      }
    }
    this.setRichText();
  }
}