import { 
  ChangeDetectorRef,
  Component, 
  ViewChild
} from '@angular/core';
import { 
  ActionSheetController, 
  Content,
  Events, 
  ModalController, 
  NavController,
  NavParams
} from 'ionic-angular';

// Services
import { SurveysService } from '../../providers/surveys/surveys-service';
import { HelperService } from '../../providers/helpers/helper-service';
import { RegionsService } from '../../providers/surveys/regions-service';

// Components
import { SurveyInformationComponent } from './components/survey-info.component';
import { SurveyQuestionComponent } from './components/survey-questions.component';
import { SurveyReportConfigComponent } from './components/survey-report-config.component';
import { SurveySettingsComponent } from './components/survey-settings.component';
import { SurveyExportComponent } from './components/survey-export.component';

// Pages
import { SurveyDataEntryPage } from '../survey-client/survey-data-entry/survey-data-entry';

@Component({
  selector: 'page-survey-add-edit',
  templateUrl: 'survey-add-edit.html'
})
export class SurveyAddEditPage 
{
  @ViewChild(Content) content: Content;

  @ViewChild('componentSurveyInfo') componentSurveyInfo: SurveyInformationComponent;
  @ViewChild('componentQuestions') componentQuestions: SurveyQuestionComponent;
  @ViewChild('componentReportConfig') componentReportConfig: SurveyReportConfigComponent;
  @ViewChild('componentSurveySettings') componentSurveySettings: SurveySettingsComponent;
  @ViewChild('componentSurveyExport') componentSurveyExport: SurveyExportComponent;

  headerData: any = { title: '' };
  menuPurpose: string = 'survey-add-edit';
  survey: any = {};
  edit: boolean = false;
  errorMessage: any = {};
  surveyLoaded: boolean = true;
  surveyDataIsDirty: boolean = false;
  dirtyDataObj: any = [];
  activeItem: any = {};

  displayForm: string;
  activeComponent: any;
  activeMenuItem: string;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public navParams: NavParams,
    public modalCtrl: ModalController,
    public actionSheetCtrl: ActionSheetController,
    public surveysService: SurveysService,
    public helperService: HelperService,
    public changeDetector: ChangeDetectorRef,
    public regionsService: RegionsService
  ) {
    if ( this.navParams.get('survey') )
    {
      this.edit = true;
      this.survey = this.navParams.get('survey');
      this.headerData = { title: 'Edit Survey: ' + this.survey.title };
    } else if ( this.navParams.get('surveyId') ) {
      // We've only passed an id in here.  Get the full survey.
      this.getSurveyData( this.navParams.get('surveyId') );
    } else {
      this.headerData = { title: 'New Survey' };
    }  

    this.headerData.button = {
      label: 'View Survey',
      icon: 'clipboard'
    };
  }

  getSurveyData( surveyId )
  {
    this.surveysService.getSurvey( surveyId )
      .subscribe( data => {
        this.survey = data;
        this.headerData = { title: 'Edit Survey: ' + this.survey.title };
      });
  }

  updateSurvey( survey )
  {
    this.survey = survey;
  }

  // Set the current form to display.
  // From MenuInspectionComponent (child)
  menuChange( menuItem )
  {
    // Initial form component is set in ngAfterViewInit
    // method at bottom of file.
    this.content.scrollToTop();
    this.displayForm = menuItem;
    this.changeDetector.detectChanges();
    this.activeMenuItem = menuItem;
    switch ( menuItem )
    {
      case 'surveyInfo':
        this.activeComponent = this.componentSurveyInfo;
        break;
      case 'questions':
        this.activeComponent = this.componentQuestions;
        break;
      case 'reportConfig':
        this.activeComponent = this.componentReportConfig;
        break;
      case 'settings':
        this.activeComponent = this.componentSurveySettings;
        break;
      case 'export':
        this.activeComponent = this.componentSurveyExport;
        break;
    }
  }

  showSurvey()
  {
    if ( this.survey.region_id > 0 )
    {
      this.regionsService.getRegion( this.survey.region_id )
        .subscribe( data => {
          this.survey.region_set = data;
          this.navCtrl.push( SurveyDataEntryPage, {
            survey: this.survey
          });
        });
    } else {
      this.navCtrl.push( SurveyDataEntryPage, {
        survey: this.survey
      });
    }
  }

  ngAfterViewInit() 
  {
    this.menuChange( 'surveyInfo' );
  }
}