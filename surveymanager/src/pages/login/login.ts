import { Component, ViewChild } from '@angular/core';
import { NavController, Nav, Events } from 'ionic-angular';

// Pages
import { DashboardPage } from '../../pages/dashboard-system/dashboard';

// Providers
import { LoginService } from '../../providers/login/login-service';
import { Config } from '../../providers/config/config';
import { HelperService } from '../../providers/helpers/helper-service';
import { UsersService } from '../../providers/users/users-service';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  headerData: any;
  login: any = {};
  errorMessage: string = '';
  showLogin: boolean = true;
  showForgotPassword: boolean = false;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public loginService: LoginService,
    public helperService: HelperService,
    public usersService: UsersService
  ) {
    this.headerData = { title: 'App Name Goes Here' };
  }

  submitLoginForm()
  {
    // Send to service
    this.loginService.login( this.login )
      .subscribe( data => {
        let userRecord:any = data;
        // Set user record in local storage 
        // so we can make API calls.
        // We'll update it in a bit...
        this.setUserRecord( userRecord );

        // Get user app record if it exists.
        this.usersService.getUserAppRecord( userRecord.user.user_id )
          .subscribe( data => {
            // If it exists, go update the record.
            // Update user app record on every login because the 
            // primary user record may have changed.  This method
            // will also create a user app record if one doesn't exist.
            userRecord.user.user_app_record = data;
            this.updateUserAppRecord( userRecord );
          },
          error => {
            if ( error.error.error === 'record not found' )
            {
              this.createUserAppRecord( userRecord );
            }
          });
      },
      error => {
        this.helperService.showErrors( error );
      }      
    );
  }

  createUserAppRecord( userRecord )
  {
    let userAppRecord = {
      first_name: userRecord.user.first_name,
      last_name: userRecord.user.last_name,
      email: userRecord.user.email,
      primary_user_id: userRecord.user.user_id,
      survey_user_id: 0,
      regions: [],
      surveys: []
    };
    this.usersService.saveAppUserRecord( userAppRecord )
      .subscribe( data => {
        userRecord.user_app_record = data;
        this.setUserRecord( userRecord );
        this.events.publish( 'user:login' );
      });
  }

  updateUserAppRecord( userRecord )
  {    
    this.usersService.saveAppUserRecord( userRecord.user.user_app_record )
      .subscribe( data => {
        this.setUserRecord( userRecord );
        this.events.publish( 'user:login' );
      });
  }

  setUserRecord( userRecord )
  {
    this.usersService.setUserInfo( userRecord );
  }

  showForgotPasswordForm()
  {
    this.showLogin = false;
    this.showForgotPassword = true;
  }

  showLoginForm()
  {
    this.showLogin = true;
    this.showForgotPassword = false;
  }
}  