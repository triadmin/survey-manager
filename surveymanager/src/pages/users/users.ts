import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';

// Pages
import { UserAccountPage } from '../../pages/user-account/user-account';

// Providers
import { UsersService } from '../../providers/users/users-service';
import { Config } from '../../providers/config/config';
import { FilesService } from '../../providers/files/files-service';
import { UserPermissionsService } from '../../providers/users/user-permissions-service';

@Component({
  selector: 'page-users',
  templateUrl: 'users.html'
})
export class UsersPage {
  headerData: any;

  users: any = [];
  userPhotos: any = [];
  adminProjects = Config.USER.projects;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public usersService: UsersService,
    public filesService: FilesService,
    public userPermissionsService: UserPermissionsService
  ) {
    this.headerData = { title: 'Users' };
  }

  getUsers()
  {
    this.usersService.getUsers()
      .subscribe( data => {
        // Determine which users we get to see.
        if ( this.userPermissionsService.userHasRole( ['survey-manager-project-level-admin' ] ))
        {
          // If we're a project level admin, then we can only view users
          // that are also assiged to one of our projects.
          // Remember, we've already filtered by app-base-gate in our AJAX call
          // so we're not pulling users that only belong to other apps.
          this.filterUsersByProject( data );
        } else {
          this.users = data;
        }
    });
  }

  filterUsersByProject( users )
  {
    // Find users that belong to the same project(s) that the 
    // project-level-admin belongs to as well.
    // These are the users that the project-level-admin will be able to see.
    let matchingUserProjects = [];
    for ( let user of users )
    {
      // NOTE: This is how we filter on 2 arrays of objects to see if
      //       there is a matching object in each array.
      matchingUserProjects = this.adminProjects.filter( project => {
        return user.projects.some( userProject => {
          return project.tri_project_id === userProject.tri_project_id
        });
      });
      
      if ( matchingUserProjects.length > 0 )
      {
        // Then add this user to the users list.
        this.users.push(user);
      }
    }
  }

  showUserAddEditPage( user )
  {
    this.navCtrl.push( UserAccountPage, {
      user: user,
      context: 'user-manager'
    });
  }

  ionViewDidEnter()
  {
    this.getUsers();
  }
}