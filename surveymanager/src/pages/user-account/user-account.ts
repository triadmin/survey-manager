import { Component } from '@angular/core';
import { 
  ActionSheetController,
  NavController,
  NavParams, 
  Events 
} from 'ionic-angular';

// Providers
import { UsersService } from '../../providers/users/users-service';
import { HelperService } from '../../providers/helpers/helper-service';
import { FilesService } from '../../providers/files/files-service';
import { Config } from '../../providers/config/config';
import { SurveysService } from '../../providers/surveys/surveys-service';
import { RegionsService } from '../../providers/surveys/regions-service';
import { UserPermissionsService } from '../../providers/users/user-permissions-service';

@Component({
  selector: 'page-user-account',
  templateUrl: 'user-account.html'
})
export class UserAccountPage {
  headerData: any;
  user: any = {};

  context: string = 'user-account';
  edit: boolean = false;
  haveUserData = false;

  // Containers for selectable items.
  gate_tags = Config.APP_RECORD.gate_tags;
  tri_projects = Config.TRI_PROJECTS;
  surveys = [];
  surveysForProjects = [];
  availableProjects:any = []; 

  // Containers for selected items.
  selectedProjects = [];
  selectedGateTags = [];
  selectedSurveys = [];
  selectedRegions = [];

  photoFile:any = {
    file_thumb_url: ''
  }; 

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public actionSheetCtrl: ActionSheetController,
    public usersService: UsersService,
    public helperService: HelperService,
    public filesService: FilesService,
    public surveysService: SurveysService,
    public regionsService: RegionsService,
    public userPermissionsService: UserPermissionsService
  ) {
    this.context = this.navParams.get('context');
    this.determineAvailableProjects();

    if ( this.navParams.get('user') )
    {
      this.user = this.navParams.get('user');
      this.headerData = { title: 'Edit User: ' + this.user.first_name + ' ' + this.user.last_name };
      this.edit = true;

      this.toggleUserProjects();
      this.toggleUserGateTags();
    } else if ( this.context === 'user-manager' ) {
      this.headerData = { title: 'Add New User' };
      this.user.avatar_file = {
        file_id: 0
      };
    } else {
      this.headerData = { title: 'My Account' };
    }
    this.getSurveys();
  }

  saveUser()
  {    
    this.usersService.saveRootUserRecord( this.user )
      .subscribe( data => {
        let d:any = data;
          // Save user gate tags.
          this.usersService.saveUserGateTags( this.user.user_gatetags, d.user_id )
            .subscribe( data => {
              // We cannot save anything else until our gatetag save completes.
              // Otherwise the server can throw Access Denied error because we're
              // trying to access routes with gates that aren't yet saved in the 
              // database.

              // Save user projects.
              this.usersService.saveUserProjects( this.user.projects, d.user_id )
              .subscribe( data => {});

              if ( this.edit === true && typeof this.user.password !== 'undefined' )
              {
                // Update user password too.
                this.usersService.updateUserPassword( this.user )
                  .subscribe( data => {});
              }

              // Save user app record.
              // Update user_app_record first.
              this.user.user_app_record.first_name = this.user.first_name;
              this.user.user_app_record.last_name = this.user.last_name;
              this.user.user_app_record.email = this.user.email;
              this.usersService.saveAppUserRecord( this.user.user_app_record )
              .subscribe( data => {
                let d:any = data;
                this.user.user_app_record.survey_user_id = d.survey_user_id;
              });
            });

          this.events.publish('app:log', {
            message: 'User has been saved.',
            status: 'success',
            type: 'save user'
          });
        },
        error => {
          this.helperService.showErrors( error );
        });
    
  }

  deleteUser( user )
  {
    this.usersService.removeUserFromApp( user.user_id ).subscribe( data => {
      this.events.publish('app:log', {
        message: 'User has been deleted.',
        status: 'warning',
        type: 'delete user'
      });
      this.dismissPage();
      },
      error => {
        this.helperService.showErrors( error );
      }  
    );      
  }

  dismissPage()
  {
    this.navCtrl.pop();
  }

  presentDeleteUserActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this user?',
      buttons: [
        {
          text: 'Delete user',
          role: 'destructive',
          handler: () => {
            this.deleteUser( this.user );
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  photoUploaded( fileObj )
  {
    let newFileObj:any = this.helperService.makeFileObject( fileObj );
    this.user.file_id = fileObj.id;
    this.user.avatar_file = newFileObj;
    this.saveUser();
  }

  deletePhoto( photo )
  {
    this.filesService.deleteUserAvatar( photo )
      .subscribe( data => {
        this.user.avatar_file.file_id = 0;
      });
  }

  getSurveysForAvailableProjects()
  {
    for ( let p of this.availableProjects )
    {
      for ( let s of this.surveys )
      {
        if ( +s.tri_project_id === +p.tri_project_id )
        {
          p.surveys.push( s );
        }
      }
    }
  }

  getRegionsForSurveys()
  {
    for ( let s of this.surveys )
    {
      if ( s.region_id > 0 )
      {
        this.regionsService.getRegion( s.region_id )
          .subscribe( data => {
            s.region = data;
          });
      }
    }
  }

  toggleUserProjects()
  {
    for ( let p of this.availableProjects )
    {
      p.isSelected = false;
      for ( let up of this.user.projects )
      {
        if ( up.tri_project_id === p.tri_project_id )
        {
          p.isSelected = true;
        }
      }
    }
  }

  toggleUserGateTags()
  {
    for ( let gt of this.gate_tags )
    {
      gt.isSelected = false;
      for ( let ugt of this.user.user_gatetags )
      {
        if ( gt.gate_tag_id === ugt.gate_tag_id )
        {
          gt.isSelected = true;
        }
      }
    }
  }

  toggleUserSurveys()
  {
    for ( let s of this.surveys )
    {
      s.isSelected = false;
      for ( let us of this.user.user_app_record.surveys )
      {
        if ( us.survey_id === s.survey_id )
        {
          s.isSelected = true;
        }
      }
    }
  }

  toggleSelectedProject( ev, project )
  {
    if ( ev.checked === true )
    {
      // Add project to user.projects.
      this.user.projects.push( project );
      project.isSelected = true;
    } else {
      project.isSelected = false;
      // Remove this project from user.projects array.
      let j = this.user.projects.length;
      while ( j-- ) 
      {
        if ( this.user.projects[j].tri_project_id === project.tri_project_id )
        {
          this.user.projects.splice( j, 1 );
        }
      }
    }
  }

  toggleSelectedSurvey( ev, survey )
  {
    if ( ev.checked === true )
    {
      // Add survey to user.user_app_record.surveys.
      survey.isSelected = true;
      this.user.user_app_record.surveys.push( survey );
    } else {
      survey.isSelected = false;
      // Remove this survey from user.user_app_record.surveys array.
      let j = this.user.user_app_record.surveys.length;
      while ( j-- ) 
      {
        if ( this.user.user_app_record.surveys[j].survey_id === survey.survey_id )
        {
          this.user.user_app_record.surveys.splice( j, 1 );
        }
      }
    }
  }

  toggleSelectedGateTag( ev, gateTag )
  {
    if ( ev.checked === true )
    {
      // Add gate_tag to user.user_gatetags.
      gateTag.isSelected = true;
      this.user.user_gatetags.push( gateTag );
    } else {
      gateTag.isSelected = false;
      // Remove this gate_tag from user.user_gatetags array.
      let j = this.user.user_gatetags.length;
      while ( j-- ) 
      {
        if ( this.user.user_gatetags[j].gate_tag_id === gateTag.gate_tag_id )
        {
          this.user.user_gatetags.splice( j, 1 );
        }
      }
    }
  }

  assignRegions( ev, survey )
  {
    // First remove any items from user_app_record.regions
    // with this survey_id
    let j = this.user.user_app_record.regions.length;
    while ( j-- ) 
    {
      if ( this.user.user_app_record.regions[j].survey_id === survey.survey_id )
      {
        this.user.user_app_record.regions.splice( j, 1 );
      }
    }

    for ( let region_list_item_id of ev )
    {
      let regionObj = {
        survey_user_id: this.user.user_app_record.survey_user_id,
        survey_id: survey.survey_id,
        region_list_item_id: region_list_item_id
      };
      this.user.user_app_record.regions.push( regionObj );
    }
  }

  isProjectSelected( project )
  {
    if ( project.isSelected )
    {
      return true;
    }
    return false;
  }

  isSurveySelected( survey )
  {
    if ( survey.isSelected )
    {
      return true;
    }
    return false;
  }

  isRegionSelected( region_list_item_id )
  {
    for ( let sr of this.selectedRegions )
    {
      if ( region_list_item_id === sr.region_list_item_id )
      {
        return true;
      }
    }
    return false;
  }

  determineAvailableProjects()
  {
    if ( this.userPermissionsService.userHasRole( ['survey-manager-admin' ] ))
    {
      for ( let p of this.tri_projects )
      {
        this.availableProjects.push({
          tri_project_id: p.tri_project_id, 
          tri_project_name: p.project_name
        });
      };
    } else if ( this.userPermissionsService.userHasRole( [ 'survey-manager-project-level-admin' ] )) {
      this.availableProjects = Config.USER.projects;
    }
    for ( let ap of this.availableProjects )
    {
      ap.surveys = [];
    }
  }

  getSurveys()
  {
    // Get all surveys (minimal)
    this.surveysService.getSurveys()
      .subscribe( data => {
        this.surveys = data;
        this.getSurveysForAvailableProjects();
        this.getRegionsForSurveys();
        
        // Once we get surveys, go get user_app_record.
        if ( this.edit === true )
        {
          this.getUserAppRecord();
        }
      });
  }

  stubUserAppRecord()
  {
    let userAppRecord = {
      first_name: this.user.first_name,
      last_name: this.user.last_name,
      email: this.user.email,
      primary_user_id: this.user.user_id,
      regions: [],
      surveys: []
    };
    this.user.user_app_record = userAppRecord;
  }

  getUserAppRecord()
  {
    this.usersService.getUserAppRecord( this.user.user_id )
      .subscribe( data => {
        this.user.user_app_record = data;
        this.selectedRegions = this.user.user_app_record.regions;

        if ( this.user.user_app_record.regions === null )
        {
          this.user.user_app_record.regions = [];
        }
        if ( this.user.user_app_record.surveys === null )
        {
          this.user.user_app_record.surveys = [];
        }
        this.toggleUserSurveys();
      },
      error => {
        if ( error.error.error === 'record not found' )
        {
          this.stubUserAppRecord();
        }
      });
  }

  regionSetCompareFn( r1, r2 )
  {
    return r1 && r2 ? r1.region_list_item_id === r2 : r1 === r2;
  }
}
