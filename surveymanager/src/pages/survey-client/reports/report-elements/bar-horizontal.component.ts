import { Component, Input, ViewChild, ElementRef } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { BaseChartDirective } from 'ng2-charts-x';

@Component({
  selector: 'bar-horizontal-component',
  template: `
    <div class="message" *ngIf="activities.length == 0">
      <ion-row>
        <ion-col col-auto><ion-icon name="alert" color="pink"></ion-icon></ion-col>
        <ion-col>There is no data for the selected date range or filters.</ion-col>
      </ion-row>
    </div>

    <canvas baseChart class="chart" id="chart_{{ index }}" #barChart="base-chart"
      *ngIf="chartData.data.length > 0"
      [datasets]="chartData.data"
      [labels]="chartData.labels"
      [options]="barChartOptions"
      [chartType]="barChartType">
    </canvas>
  `
})

export class BarHorizontalComponent
{
  @ViewChild(BaseChartDirective) _chart: BaseChartDirective;

  @Input() activities = [];
  @Input() activitiesCompare = [];

  @Input() reportElementConfig:any = {};
  @Input() index;
  @Input() filters:any = [];
  @Input() survey:any = {};

  chartData: any = {
    labels: [],
    data: []
  };

  barChartColors: any = [
    { backgroundColor: ['#a1d489', '#b1e3f6'] }
  ];
  barChartType:string = 'horizontalBar';
  barChartOptions = {
    responsive: false,
    events: ['click'],
    scales: {
      yAxes: [{
        ticks: {
          fontSize: 14
        }
      }],
      xAxes: [{
        ticks: {
          fontSize: 14,
          beginAtZero: true
        }
      }]
    },
    legend: {
      display: false
    }
  };

  constructor(
    private nav: NavController,
    private events: Events
  ) {}

  prepDataForChart( activities )
  {
    let responseTextArray = [], uniqueArray = [], chartDataTemp:any = [], dataSet = [];

    // Create our temp data Array based on labels.
    for ( let item of this.chartData.labels )
    {
      chartDataTemp.push({
        label: item,
        count: 0
      });
    }

    for ( let a of activities )
    {
      // For each activity, find the question that the 
      // charting component is responsible for and add its 
      // response text to array.
      for ( let r of a.responses )
      {
        if ( r.survey_question_id === this.reportElementConfig.survey_question_id )
        {
          // Add response text to our array.
          responseTextArray.push( r.text );
        }
      }
    }

    // Add up occurences of each response to get a count (e.g. Attend Training: 3)
    responseTextArray.forEach(function ( a ) {
      if ( a in uniqueArray ) uniqueArray[a]++;
      else uniqueArray[a] = 1;
    });

    // Deal with any filters we might have set.
    // NOTE: Even though this.activities is already filtered,
    // we need to remove items that aren't in the desired filter
    // from being rendered on the chart.
    let filter = this.filters.filter( f => 
      f.questionId === this.reportElementConfig.survey_question_id
    );

    if ( 
      filter[0] !== undefined && 
      filter[0].selections !== undefined && 
      filter[0].selections.length > 0 
    )
    {
      for ( let filterSelection of filter[0].selections )
      {
        let obj = chartDataTemp.find( item => item.label === filterSelection);
        obj.count = uniqueArray[filterSelection];
      }
    } else {
      for ( var key in uniqueArray )
      {
        let obj = chartDataTemp.find( item => item.label === key);
        if ( obj !== undefined )
        {
          obj.count = uniqueArray[key];
        }
      }
    }

    // put our labels and numbers into separate arrays.
    for ( let d of chartDataTemp )
    {
      dataSet.push( d.count );
    }

    return {
      data: dataSet,
      label: '03/01/19 - 04/21/2019'
    };
  }

  getBase64Image()
  {
    var canvas = document.getElementById('chart_' + this.index) as HTMLCanvasElement;
    var dataURL = canvas.toDataURL();
    return dataURL;
  }

  setChartLabels()
  {
    let question = this.survey.questions.filter( q => 
      q.survey_question_id === this.reportElementConfig.survey_question_id
    );

    for ( let a of question[0].answers )
    {
      this.chartData.labels.push( a.answer_text );
    }
  }

  ngOnInit()
  {
    this.setChartLabels();
  }

  ngOnChanges()
  {
    // This gets fired whenever chart data changes.
    this.chartData.data.length = 0;

    // Clone chartData.data
    const dataClone = JSON.parse( JSON.stringify( this.chartData.data ) );

    if ( this.activities.length > 0 )
    {
      dataClone.push( this.prepDataForChart( this.activities ) );
    }
    if ( this.activitiesCompare.length > 0 )
    {
      dataClone.push( this.prepDataForChart( this.activitiesCompare ) );
    }

    // Timeout for applying changes.
    setTimeout(() => {
      this.chartData.data = dataClone;
    }, 50);
  }
}