import { Component, Input, ViewChild,ElementRef } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

@Component({
  selector: 'gauge-component',
  template: `
    <div class="message" *ngIf="activities.length == 0">
      <ion-row>
        <ion-col col-auto><ion-icon name="alert" color="pink"></ion-icon></ion-col>
        <ion-col>There is no data for the selected date range or filters.</ion-col>
      </ion-row>
    </div>

    <ngx-gauge 
      id="chart_{{ index }}"
      #chart
      [type]="gaugeType" 
      [value]="gaugeValue" 
      [label]="gaugeLabel"  
      [append]="gaugeAppendText">
    </ngx-gauge>
  `
})

export class GaugeComponent
{
  @ViewChild( 'chart' ) guage: ElementRef; 

  @Input() activities = [];
  @Input() reportElementConfig:any = {};
  @Input() index;

  gaugeType = "semi";
  gaugeValue = 28.3;
  gaugeLabel = "Speed";
  gaugeAppendText = "km/hr";

  constructor(
    private nav: NavController,
    private events: Events
  ) {
  }

  prepDataForChart()
  {
    let responseTextArray = [], uniqueArray = [], chartDataTemp = [];

    for ( let a of this.activities )
    {
      // Get the questions that our charting component is
      // responsible for.
      for ( let r of a.responses )
      {
        if ( r.survey_question_id === this.reportElementConfig.survey_question_id )
        {
          // Add response text to our array.
          responseTextArray.push( r.text );
        }
      }
    }

    // Add up occurences of each response to get a count (e.g. Attend Training: 3)
    responseTextArray.forEach(function ( a ) {
      if ( a in uniqueArray ) uniqueArray[a]++;
      else uniqueArray[a] = 1;
    });

    // Turn uniqueArray into something we can sort
    for ( var key in uniqueArray )
    {
      var obj = {
        label: key,
        count: uniqueArray[key]
      };
      chartDataTemp.push( obj );
    }

    chartDataTemp.sort(function(a, b){
      return b.count - a.count;
    });

    // Now our counts are sorted highest to lowest,
    // put our labels and numbers into separate arrays.
    /*
    this.chartData.labels = [];
    this.chartData.data = [];
    for ( let d of chartDataTemp )
    {
      this.chartData.labels.push( d.label );
      this.chartData.data.push( d.count );
    }
    */
  }
  
  ngOnChanges()
  {
    //this.chartData.length = 0;
    if ( this.activities.length > 0 )
    {
      this.prepDataForChart();
    }
  }

  getBase64Image()
  {
    var gauge = document.getElementById('chart_' + this.index);
    console.log(gauge);
    var canvas = gauge.children[2] as HTMLCanvasElement;
    var dataURL = canvas.toDataURL();
    return dataURL;
  }
}
