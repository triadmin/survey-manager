import { Component, Input, ViewChild,ElementRef } from '@angular/core';
import { 
  NavController, 
  NavParams, 
  Events
} from 'ionic-angular';

// Providers 
import { SurveyAttemptsService } from '../../../../providers/surveys/survey-attempts-service';
import { Config } from '../../../../providers/config/config';

import moment from 'moment';

@Component({
  selector: 'goal-report-component',
  template: `
    <div class="muted">{{ goalDateString }}</div>
    <ngx-gauge
      (click)="presentGoalPopover($event, goal)"
      id="chart_{{ index }}"
      #chart
      [type]="gaugeType" 
      [value]="gaugeValue"  
      [append]="gaugeAppendText"
      [thresholds]="gaugeThresholds"
      [max]="gaugeMax"
      size="150"
      cap="round"
      thick="12"
    >
    </ngx-gauge>
    <div class="label">{{ gaugeLabel }}</div>
    <div class="criteria-container">
      <p><strong>GOAL</strong> {{ goalDescription }}</p>
      <p margin-top><strong>GOAL CRITERIA</strong></p>
      <p *ngFor="let c of goalCriteria">{{ c.questionText }}: <strong>{{ c.response }}</strong></p>
    </div>
  `
})

export class GoalReportComponent
{
  @ViewChild( 'chart' ) guage: ElementRef; 

  @Input() goal:any = {};
  @Input() reportElementConfig:any = {};
  @Input() index;

  config = Config;
  surveyAttemptCount = 0;

  gaugeType = 'semi';
  gaugeValue = 0;
  gaugeLabel = '';
  gaugeAppendText = '%';
  gaugeMax = 100;
  gaugeThresholds = {
    '0': {color: 'orangeRed'},
    '30': {color: 'orange'},
    '60': {color: 'green'}
  };

  goalDateString;
  goalCriteria;
  goalDescription;

  constructor(
    private nav: NavController,
    private events: Events,
    private surveyAttemptsService: SurveyAttemptsService
  ) {}

  prepDataForChart()
  {
    // Get total number of activities for goal time period.
    let filter = {
      dateStart: this.goal.start_date,
      dateEnd: this.goal.end_date,
      surveyUserId: this.goal.survey_user_id
    };
    
    this.surveyAttemptsService.getSurveyAttemptCount( this.goal.survey_id, filter )
      .subscribe( data => {
        this.surveyAttemptCount = data.survey_attempt_count;
        this.calculateGoalMetrics();
      });

    if ( moment( this.goal.start_date ).format('YYYY') !== moment( this.goal.end_date ).format('YYYY') )
    {
      this.goalDateString = moment( this.goal.start_date ).format('MMM D, YYYY') + ' - ' + moment( this.goal.end_date ).format('MMM D, YYYY')
    } else {
      this.goalDateString = moment( this.goal.start_date ).format('MMM D') + ' - ' + moment( this.goal.end_date ).format('MMM D')
    }

    this.goalCriteria = JSON.parse( this.goal.criteria );
    this.goalDescription = this.goal.description;
    console.log(this.goal);
  }

  calculateGoalMetrics()
  {
    if ( this.goal.goal_is_met_metric === 'count' )
    {
      // Find percentage of goal completion based on number of activities.
      setTimeout (() => {
        this.gaugeValue = +(( this.goal.goal_activities.length / this.goal.goal_is_met_size ) * 100 ).toFixed(0);
        this.gaugeLabel = (this.goal.goal_is_met_size - this.goal.goal_activities.length) + ' activities remaining to meet goal'
      }, 2000);
    } else {
      // Find percentage of activities toward goal as a percentage of all activities.
      // First calculate a count of activities based on user enter percentage.
      let goalActivityCount = +this.surveyAttemptCount * ( this.goal.goal_is_met_size / 100 );
      setTimeout (() => {
        this.gaugeValue = +(( this.goal.goal_activities.length / goalActivityCount ) * 100 ).toFixed(0);
        this.gaugeLabel = this.goal.goal_is_met_size + '% of activities to meet goal';
      }, 2000);
    }
  }
  
  ngOnChanges()
  {
    this.prepDataForChart();
  }

  getBase64Image()
  {
    var gauge = document.getElementById('chart_' + this.index);
    console.log(gauge);
    var canvas = gauge.children[2] as HTMLCanvasElement;
    var dataURL = canvas.toDataURL();
    return dataURL;
  }
}
