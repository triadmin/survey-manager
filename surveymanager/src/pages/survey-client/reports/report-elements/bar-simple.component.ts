import { Component, Input } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

@Component({
  selector: 'bar-simple-component',
  template: `
    <div class="message" *ngIf="activities.length == 0">
      <ion-row>
        <ion-col col-auto><ion-icon name="alert" color="pink"></ion-icon></ion-col>
        <ion-col>There is no data for the selected date range or filters.</ion-col>
      </ion-row>
    </div>
    <div *ngIf="activities.length > 0">
      <div *ngFor="let data of chartData; let i = index">
        <span class="muted">{{ data.label }}</span>
        <div class="bar-simple-container" (click)="viewReports(this.reportType, data.label)">
          <div class="bar" [style.width]="data.percentage + '%'">{{ data.count }}</div>
        </div>
      </div>
    </div>
  `
})

export class BarSimpleComponent
{
  @Input() activities = [];
  @Input() reportElementConfig:any = {};
  @Input() index;

  chartData = [];

  constructor(
    private nav: NavController,
    private events: Events
  ) {
  }

  prepDataForChart()
  {
    let responseTextArray = [], uniqueArray = [];

    for ( let a of this.activities )
    {
      // Get the questions that our charting component is
      // responsible for.
      for ( let r of a.responses )
      {
        if ( r.survey_question_id === this.reportElementConfig.survey_question_id )
        {
          //this.questionsToChart.push( r );
          // Add response text to our array.
          responseTextArray.push( r.text );
        }
      }
    }

    // Add up occurences of each response to get a count (e.g. Attend Training: 3)
    responseTextArray.forEach(function ( a ) {
      if ( a in uniqueArray ) uniqueArray[a]++;
      else uniqueArray[a] = 1;
    });

    // Turn uniqueArray into something we can sort
    for ( var key in uniqueArray )
    {
      var obj = {
        label: key,
        count: uniqueArray[key]
      };
      this.chartData.push( obj );
    }

    this.chartData.sort(function(a, b){
      return b.count - a.count;
    });

    // Now our counts are sorted highest to lowest.
    // Get our maximum.
    var maxValue = this.chartData[0].count;
    for ( let d of this.chartData )
    {
      d.percentage = Math.round( ( d.count / maxValue ) * 100 );
    }
  }
  
  ngOnChanges()
  {
    this.chartData.length = 0;
    if ( this.activities.length > 0 )
    {
      this.prepDataForChart();
    }
  }
}
