import { Component, Input } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';

@Component({
  selector: 'table-component',
  template: `
    <div class="message" *ngIf="activities.length == 0">
      <ion-row>
        <ion-col col-auto><ion-icon name="alert" color="pink"></ion-icon></ion-col>
        <ion-col>There is no data for the selected date range or filters.</ion-col>
      </ion-row>
    </div>

    <div *ngIf="activities.length > 0">
      <ion-item>
        <ion-row>
          <ion-col col-6><strong>Selection</strong></ion-col>
          <ion-col col-3 text-right><strong>Count</strong></ion-col>
          <ion-col col-3 text-right><strong>% of Total</strong></ion-col>
        </ion-row>
      </ion-item>
      <ion-item *ngFor="let data of chartData; let i = index">
        <ion-row>
          <ion-col col-6>{{ data.label }}</ion-col>
          <ion-col col-3 text-right>{{ data.count }}</ion-col>
          <ion-col col-3 text-right>{{ data.percentage }}%</ion-col>
        </ion-row>
      </ion-item>
    </div>
  `
})

export class TableComponent
{
  @Input() activities = [];
  @Input() reportElementConfig:any = {};
  @Input() index;

  chartData: any = [];

  constructor(
    private nav: NavController,
    private events: Events
  ) {
  }

  prepDataForChart()
  {
    let responseTextArray = [], uniqueArray = [];

    for ( let a of this.activities )
    {
      // Get the questions that our charting component is
      // responsible for.
      for ( let r of a.responses )
      {
        if ( r.survey_question_id === this.reportElementConfig.survey_question_id )
        {
          // Add response text to our array.
          responseTextArray.push( r.text );
        }
      }
    }

    // Add up occurences of each response to get a count (e.g. Attend Training: 3)
    responseTextArray.forEach(function ( a ) {
      if ( a in uniqueArray ) uniqueArray[a]++;
      else uniqueArray[a] = 1;
    });

    // Turn uniqueArray into something we can sort
    let countTotal = 0;
    for ( var key in uniqueArray )
    {
      var obj = {
        label: key,
        count: uniqueArray[key]
      };
      this.chartData.push( obj );
      countTotal += +uniqueArray[key];
    }

    this.chartData.sort(function(a, b){
      return b.count - a.count;
    });

    // Now our counts are sorted highest to lowest,
    // put our labels and numbers into separate arrays.
    for ( let d of this.chartData )
    {
      d.percentage = Math.round( ( d.count / countTotal ) * 100 );
    }
  }
  
  ngOnChanges()
  {
    this.chartData.length = 0;
    if ( this.activities.length > 0 )
    {
      this.prepDataForChart();
    }
  }
}
