import { 
  Component, 
  Input
} from '@angular/core';
import { 
  NavController, 
  NavParams, 
  Events 
} from 'ionic-angular';

// Providers
import { HelperService } from '../../../../providers/helpers/helper-service';
import { HelpersChartService } from '../../../../providers/helpers/helper-chart-service';

import moment from 'moment';

@Component({
  selector: 'trend-component',
  template: `
    <div class="message" *ngIf="activities.length == 0">
      <ion-row>
        <ion-col col-auto><ion-icon name="alert" color="pink"></ion-icon></ion-col>
        <ion-col>There is no data for the selected date range or filters.</ion-col>
      </ion-row>
    </div>

    <ion-row>
      <ion-col>
        <canvas baseChart class="chart" id="chart_{{ index }}" #chart
          *ngIf="chartData.data.length > 0"
          [datasets]="chartData.data"
          [labels]="chartData.labels"
          [options]="listChartOptions"
          [chartType]="lineChartType"
        >
        </canvas>
      </ion-col>
    </ion-row>
  `
})

export class TrendComponent
{
  @Input() activities = [];
  @Input() activitiesCompare = [];

  @Input() reportElementConfig:any = {};
  @Input() index;
  @Input() survey:any = {};

  @Input() filters:any = [];
  @Input() localDataFilters:any = [];
  @Input() compares: any = {};

  chartData: any = {
    labels: [],
    data: []
  };

  dateSegments = [];
  dateSegmentsCompare = [];
  yAxisMode = '';
  questionAnswers = [];

  lineChartColors: any = [
    { // red
      backgroundColor: 'rgba(255,0,0,0)',
      borderColor: 'red',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  lineChartType:string = 'line';
  lineChartOptions = {
    responsive: false,
    events: ['click'],
    legend: {
      display: true,
      fullWidth: true,
      position: 'bottom',
      labels: {
        fontSize: 14
      }
    }
  };

  iterableDiffer;

  constructor(
    private nav: NavController,
    private events: Events,
    private helpersService: HelperService,
    private helpersChartService: HelpersChartService
  ) {
    this.events.subscribe('charts:clear-compares', () => {
      this.activitiesCompare.length = 0;
      this.ngOnChanges();
    });
  }

  prepDataForChart( activities, compare = false )
  {
    // What are we doing here?
    // 1. Need to cluster our activities around scale dates.
    // 2. Then, we need to determine if our y-axis is a count or sum.
    // Count = like we've done other charts.  A simple count of all occurences of a response.
    // Sum = a sum of respons values.
    let activityClusters = this.clusterActivitiesByDate( activities, compare );

    if ( this.yAxisMode === 'sum' )
    {
      this.chartData.data.push( this.calcActivitySum( activityClusters, compare ) );
    } else if ( this.yAxisMode === 'count' ) {
      this.chartData.data = this.calcActivityCount( activityClusters, compare );
    }
  }

  clusterActivitiesByDate( activities, compare )
  {
    // xAxis dates are ordered Earliest to Latest.
    // Activity dates are ordered Latest to Earliest.

    // Clone activities because we're going to remove array elements
    // as we go.
    let activitiesClone = JSON.parse( JSON.stringify( activities ) );
    let activityClusters = [];

    let dateSegments = this.dateSegments;
    if ( compare )
    {
      dateSegments = this.dateSegmentsCompare;
    } 

    for ( let dateSegment of dateSegments )
    {
      // Loop over activities in reverse.
      let j = activitiesClone.length;
      let activityCluster = [];
      while ( j-- ) 
      {
        if ( moment( activitiesClone[j].date ).isSameOrBefore( moment( dateSegment ) ) )
        {
          activityCluster.push( activitiesClone[j] );
          activitiesClone.splice( j, 1 );
        }
      }

      activityClusters.push( activityCluster );
    }
    return activityClusters;
  }

  calcActivitySum( activityClusters, compare )
  {
    /* 
      For each activityCluster, we need to get a total (if possible) by adding up
      response from all activities.
      Our data will look like:
      { data: [65, 59, 80, 81, 56, 55, 40], label: '<no label unless we're doing a compare>, fill: false }
    */
    let answerData = {
      data: [],
      label: this.helpersChartService.getCompareLabels( compare ),
      fill: false
    };

    for ( let cluster of activityClusters )
    {
      let responseSum:number = 0;

      for ( let a of cluster )
      {
        // For each activity, find the question that the 
        // charting component is responsible for.
        for ( let r of a.responses )
        {
          if ( r.survey_question_id === this.reportElementConfig.survey_question_id )
          {
            // Let's see if we can get a number out of this response.
            // In either text or value.
            if ( r.value !== '' && r.value !== null && r.value !== 'undefined' && r.value !== undefined )
            {
              if ( !isNaN( r.value ) )
              {
                responseSum += +r.value;
              } else {
                responseSum += +this.helpersService.getNumberFromString( r.value );
              }
            } else {
              if ( !isNaN( r.text ) )
              {
                responseSum += +r.text;
              } else {
                responseSum += +this.helpersService.getNumberFromString( r.text );
              }
            }
          }
        }
      }

      answerData.data.push( responseSum );
    }

    return answerData;
  }

  calcActivityCount( activityClusters, compare )
  {
    /* 
      For each activityCluster, we need to get a count of all possible responses from answers.
      Our data will look like:
      { data: [65, 59, 80, 81, 56, 55, 40], label: 'Answer 1', fill: false },
      { data: [28, 48, 40, 19, 86, 27, 90], label: 'Answer 2', fill: false },
      { data: [28, 48, 40, 19, 86, 27, 90], label: 'Answer 3', fill: false }
    */
    let data = [];
    for ( let answer of this.questionAnswers )
    {
      let answerData = {
        data: [],
        label: answer.answerText,
        fill: false
      };
      for ( let cluster of activityClusters )
      {
        let responseCount = 0;

        for ( let a of cluster )
        {
          // For each activity, find the question that the 
          // charting component is responsible for.
          for ( let r of a.responses )
          {
            if ( r.survey_answer_id === answer.answerId )
            {
              // Add response text to our array.
              responseCount++;
            }
          }
        }

        answerData.data.push( responseCount );
      } 
      
      data.push( answerData );
    }

    return data;
  }

  setChartLabels( compare = false )
  {
    this.dateSegments = [];
    this.dateSegmentsCompare = [];

    // Get days between dates.
    let start, end, labels;
    
    if ( compare )
    {
      start = moment( this.compares.dateStart );
      end = moment( this.compares.dateEnd );
    } else {
      labels = [];
      start = moment( this.filters.dateStart );
      end = moment( this.filters.dateEnd );
    }
    
    let daysSpan = end.diff( start, 'days' ) + 1; 

    // If days between dates <= 100, labels will be weeks
    if ( daysSpan <= 100 )
    {
      while (end > start || start.format('w') === end.format('w')) 
      {
        if ( compare )
        {
          this.dateSegmentsCompare.push( start.endOf( 'week' ).format( 'YYYY-MM-DD' ) );
        } else {
          this.dateSegments.push( start.endOf( 'week' ).format( 'YYYY-MM-DD' ) );
          labels.push(start.format( 'MMM D' ));
        }
        start.add( 1, 'week' );
      }
    } else {
      // If days between dates > 100, labels will be months.
      while (end > start || start.format('M') === end.format('M')) 
      {
        if ( compare )
        {
          this.dateSegmentsCompare.push( start.endOf( 'month' ).format( 'YYYY-MM-DD' ) );
        } else {
          this.dateSegments.push( start.endOf( 'month' ).format( 'YYYY-MM-DD' ) );
          labels.push(start.format( 'MMM YYYY' ));
        }
        start.add( 1, 'month' );
      }
    }

    if ( !compare )
    {
      this.chartData.labels = labels;
    }
  }

  setQuestionAnswers()
  {
    this.questionAnswers = [];
    let question = this.survey.questions.filter( q => 
      q.survey_question_id === this.reportElementConfig.survey_question_id
    );

    for ( let a of question[0].answers )
    {
      this.questionAnswers.push({
        answerId: a.survey_answer_id,
        answerText: a.answer_text 
      });
    }
  }

  getBase64Image()
  {
    var canvas = document.getElementById('chart_' + this.index) as HTMLCanvasElement;
    var dataURL = canvas.toDataURL();
    return dataURL;
  }

  ngOnInit()
  {
    this.yAxisMode = this.reportElementConfig.trend_values;
  }
  
  ngOnChanges()
  {
    // This gets fired whenever chart data changes.
    this.chartData.data.length = 0;

    // Timeout for applying changes.
    setTimeout(() => {
      this.setQuestionAnswers();

      if ( this.activities.length > 0 )
      {
        this.setChartLabels();
        this.prepDataForChart( this.activities );
      }
      if ( this.activitiesCompare.length > 0 )
      {
        this.setChartLabels( true );
        this.prepDataForChart( this.activitiesCompare, true );
      }
    }, 500);
  }
}
