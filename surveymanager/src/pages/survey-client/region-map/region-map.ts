import { 
  ChangeDetectorRef,
  ElementRef,
  Component, 
  ViewChild
} from '@angular/core';
import { 
  Events, 
  PopoverController,
  NavController,
  NavParams,
  Nav,
  Popover
} from 'ionic-angular';

// HTML Map Marker Class
import createHTMLMapMarker from './html-map-marker';

// Services
import { SurveyData } from '../../../providers/surveys/survey-client-data';

// Pages
import { SurveyDataEntryPage } from '../survey-data-entry/survey-data-entry';

// Popovers
import { SurveyClientMenuPopoverPage } from '../../../components/survey-client/menu.component';
import { MapActivityPopoverPage } from '../../../components/popovers/map-activity.popover';

declare var google;

@Component({
  selector: 'page-region-map',
  templateUrl: 'region-map.html'
})
export class RegionMapPage 
{
  survey: any = {};
  surveyData = SurveyData;

  @ViewChild('activitymap') mapElement: ElementRef;
  map: any;
  mapCircleMarkers = [];
  mapMarkers = [];
  scrollable = '';

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public navParams: NavParams,
    public nav: Nav,
    public popoverCtrl: PopoverController
  ) {
    if ( this.navParams.get('survey') )
    {
      this.survey = this.navParams.get('survey');
    }

    this.events.unsubscribe('activities:loaded');

    // Log events
    this.events.subscribe('activities:loaded', ( logData ) => {
      this.updateMapMarkers();
    });
  }

  renderActivityMap()
  {    
    this.map = new google.maps.Map(this.mapElement.nativeElement, {
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    // Let's try a test with TNF Region 1 coords.
    var northEast = new google.maps.LatLng( '46.230612', '-121.798449' );
    var southWest = new google.maps.LatLng( '45.321670', '-124.011508' );
    var bounds = new google.maps.LatLngBounds();
    bounds.extend(southWest);
    bounds.extend(northEast);
    this.map.fitBounds(bounds);

    this.updateMapMarkers();
  }

  updateMapMarkers()
  {
    this.renderMapMarkers();
  }

  renderMapMarkers()
  {
    if ( this.mapMarkers.length > 0 )
    {
      // Remove existing markers before rendering new ones.
      for ( let m of this.mapMarkers )
      {
        m.setMap(null);
      }
    }

    let coordsArray = this.makeCoordinateObject();

    //let marker;
    for ( let ca of coordsArray )
    {
      var latLng = new google.maps.LatLng( ca.lat, ca.lng );
      let marker = createHTMLMapMarker({
        latlng: latLng,
        map: this.map,
        html: `
          <div class="pin">
            <div class="pin-text">` + ca.count + `</div>
            <div class="shadow"></div>
          </div>
        `
      });
      this.mapMarkers.push( marker );

      this.attachInfoWindow( marker, ca );
    }
  }

  attachInfoWindow( marker, coordObj )
  {
    let $_this = this;
    marker.addListener('click', () => {
      let markerPointObj = marker.getProjection().fromLatLngToContainerPixel(marker.getPosition());
      let point = {
        x: markerPointObj.x,
        y: markerPointObj.y
      };
      this.presentActivityPopover( coordObj.activities, point );
    });
  }

  presentActivityPopover( activities, point )
  {    
    // Create 'fake' ev to place our popover.
    // Offset popover in pixels so it appears at the
    // center bottom of the pin.
    let ev = {
      target : {
        getBoundingClientRect : () => {
          return {
            top: point.y + 95,
            left: point.x + 275
          };
        }
      }
    };
    let popover = this.popoverCtrl.create( MapActivityPopoverPage, {
      activities: activities
    });
    popover.present({
      ev
    });
  }

  // Here's what we're doing here.
  // 1. Go through all activities, get coords from region question type, and put the coords into an array.
  // 2. Go through this coords array and round all coords to 3 decimals.  We're then going to have a lot of duplicates.
  // 3. Go through array again and filter out duplicates but keep the count.
  // What we want is a unique array of coords with a count of how many activities were at the coords.  We're going
  // to display a circle on the map that's larger with the number of activities.
  // https://developers.google.com/maps/documentation/javascript/examples/circle-simple
  makeCoordinateObject()
  {
    let coordsArray = [], coordObj = { lat: 0, lng: 0, count: 0, activities: [] };
    for ( let a of this.surveyData.activities )
    {
      let locationObj = JSON.parse( a.location );
      if ( locationObj.coords.length > 0 )
      {
        for ( let c of locationObj.coords )
        {
          if ( c !== null )
          {
            let latNum = +c.lat;
            let lngNum = +c.lng;
            coordObj = {
              lat: +latNum.toFixed(2),
              lng: +lngNum.toFixed(2),
              count: 1,
              activities: []
            };

            // Does a duplicate of this coordObj already exist in coordsArray?
            // If we don't have any items in our coordsArray yet, 
            // go ahead and push this object onto it.
            if ( coordsArray.length === 0 )
            {
              coordObj.activities.push( a );
              coordsArray.push( coordObj );
            }

            let found = false;
            for ( let ca of coordsArray )
            {
              if ( ca.lat === coordObj.lat && ca.lng === coordObj.lng )
              {
                ca.activities.push( a );
                ca.count++;
                found = true;
                break;
              } 
            }

            if ( !found )
            {
              coordObj.activities.push( a );
              coordsArray.push( coordObj );
            }
          }
        }
      }
    }

    return coordsArray;
  }

  presentMenuPopover( ev, purpose )
  {
    let popover = this.popoverCtrl.create( SurveyClientMenuPopoverPage, {
      purpose: purpose
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      switch( data )
      {
        case 'new-activity':
          this.showSurveyDataEntryPage();
          break;
        case 'reports':
          this.showSurveyDataEntryPage();
          break;
      }
    });
  }

  showSurveyDataEntryPage()
  {
    this.navCtrl.push( SurveyDataEntryPage, {
      survey: this.survey
    });
  }

  // Disable scrolling when dragging slider.
  sliderDrag( ev )
  {
    if ( ev )
    {
      this.scrollable = 'no-scroll';
    } else {
      this.scrollable = 'fixed';
    }
  }

  ionViewDidLoad()
  {
    this.events.publish( 'survey:load-data' );
    google.maps.event.addDomListener(window, 'load', this.renderActivityMap());
  }
}
  