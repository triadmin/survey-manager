import { Component } from '@angular/core';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";
import { 
  NavController, 
  Events,
  NavParams
} from 'ionic-angular';

// Providers
import { GoalsService } from '../../../providers/goals/goals-service';
import { SurveyAttemptsService } from '../../../providers/surveys/survey-attempts-service';
import { SurveyData } from '../../../providers/surveys/survey-client-data';
import { HelperSurveyService } from '../../../providers/helpers/helper-survey-service';
import { Config } from '../../../providers/config/config';

@Component({
  selector: 'page-goal-add-edit',
  templateUrl: 'goal-add-edit.html'
})
export class GoalAddEditPage {
  headerData: any;
  goal:any = {};
  autocompleteItems = [];
  survey = SurveyData.survey;
  config = Config;
  noActivitiesError = false;
  goalCriteria = [];

  // RichText Editor
  goal_description: FormControl;

  constructor(
    public formBuilder: FormBuilder,
    public navCtrl: NavController,
    public events: Events,
    public goalsService: GoalsService,
    public surveyAttemptsService: SurveyAttemptsService,
    public helperSurveyService: HelperSurveyService,
    public navParams: NavParams
  ) {
    if ( this.navParams.get('goal') )
    {
      this.goal = this.navParams.get('goal');
      this.headerData = { title: 'Edit Goal: ' + this.goal.title };
      this.goalCriteria = JSON.parse( this.goal.criteria );
    } else {
      this.headerData = { title: 'New Goal' };
      this.goal.goal_is_met_metric = 'count';
    }
  }

  doActivitySearch( ev )
  {
    if ( ev.target.value.length < 4 ) {
      this.autocompleteItems = [];
      return;
    }

    let filter = {
      search: ev.target.value
    };
    this.surveyAttemptsService.searchSurveyAttempts( this.survey.survey_id, filter )
      .subscribe( data => {
        if ( data.length === 0 )
        {
          this.noActivitiesError = true;
        } else {
          this.autocompleteItems = data;
          this.noActivitiesError = false;
        }
      });
  }

  selectActivityForGoal( activity )
  {
    // Now we display the reponses for this activity in the 
    // right-hand pane so we can configure our goal criteria.

    // First clear activity list and goal criteria array.
    this.autocompleteItems = [];

    // Remove this if we want multiple activities to contribute to 
    // goal criteria.
    this.goalCriteria = [];

    // Match survey questions with responses 
    let questions = this.helperSurveyService.matchSurveyQuestionsWithResponses( this.survey, activity );
    // Now, map question type definitions
    questions = this.helperSurveyService.attachQuestionTypeDefinitionsToQuestions( this.survey.questions );

    for ( let q of questions )
    {
      if ( q.questionTypeDefinition.reportable === 'Yes' && q.responses.length > 0 )
      {
        for ( let r of q.responses )
        {
          let goalCriteraObj = {
            questionId: q.survey_question_id,
            questionText: q.question_text,
            questionIdentifier: q.question_identifier,
            response: r.text,
            value: '',
            response_id: r.survey_response_id
          };

          if ( r.value !== 'undefined' && r.value !== '' && r.value !== null )
          {
            goalCriteraObj.value = r.value;
          }
          this.goalCriteria.push( goalCriteraObj );
        }
      }
    }
  }

  saveGoal()
  {
    // Stringify goal criteria.
    this.goal.criteria = JSON.stringify( this.goalCriteria );

    this.goal.goal_is_met_size = +this.goal.goal_is_met_size;

    // Get rich-text field value
    this.goal.description = this.goal_description.value;
    this.goal.survey_id = this.survey.survey_id;
    this.goal.survey_user_id = this.config.USER_APP_RECORD.survey_user_id;
    this.goal.status = 'Active';
    this.goalsService.saveGoal( this.goal )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Goal has been saved.',
          status: 'success',
          type: 'save survey goal'
        });
      });

    this.events.publish( 'goals:refresh' );
  }

  deleteGoal()
  {
    this.goalsService.deleteGoal( this.goal.goal_id )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Goal has been deleted.',
          status: 'warning',
          type: 'delete survey goal'
        });
      });
  }

  deleteGoalCriteria( goalCriteria )
  {
    let j = this.goalCriteria.length;
    while ( j-- ) 
    {
      if ( this.goalCriteria[j].response_id === goalCriteria.response_id )
      {
        this.goalCriteria.splice( j, 1 );
      }
    }
  }

  setRichText()
  {
    // Init the richText component.
    this.goal_description = this.formBuilder.control(this.goal.description);    
  }

  ngOnInit()
  {
    this.setRichText();
  }
}