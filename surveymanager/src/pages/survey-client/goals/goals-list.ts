import { Component } from '@angular/core';
import { NavController, Events } from 'ionic-angular';

// Pages
import { GoalAddEditPage } from '../goals/goal-add-edit'

// Providers
import { GoalsService } from '../../../providers/goals/goals-service';
import { Config } from '../../../providers/config/config';
import { SurveyData } from '../../../providers/surveys/survey-client-data';

@Component({
  selector: 'page-goals',
  templateUrl: 'goals-list.html'
})
export class GoalsPage {
  headerData: any;
  goals = [];
  user = Config.USER_APP_RECORD;
  survey = SurveyData.survey;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public goalsService: GoalsService
  ) {
    this.headerData = { title: 'Goals' };
  }

  getGoals()
  {
    this.goalsService.getGoalsForUserForSurvey( this.user.survey_user_id, this.survey.survey_id  )
      .subscribe( data => {
        this.goals = data;
    });
  }  

  showGoalAddEditPage( goal )
  {
    this.navCtrl.push( GoalAddEditPage, {
      goal: goal
    });
  }

  ionViewDidEnter()
  {
    this.getGoals();
  }
}