import { 
  Component, 
  ElementRef,
  EventEmitter, 
  Input, 
  Output,
  ViewChild
} from '@angular/core';

import { 
  LoadingController,
  Events
} from 'ionic-angular';
import { VirtualScrollerComponent } from 'ngx-virtual-scroller';

// Providers
import { SurveyAttemptsService } from '../../../../../providers/surveys/survey-attempts-service';
import { Config } from '../../../../../providers/config/config';
import { UserPermissionsService } from '../../../../../providers/users/user-permissions-service' 

import moment from 'moment';
import { s } from '@angular/core/src/render3';

@Component({
  selector: 'survey-client-left-column-parent',
  templateUrl: 'survey-client-left-column-parent.html'
})

export class SurveyClientLeftColumnParentComponent
{
  @Input() surveyData: any = {};
  @Input() survey: any = {};

  @Output() sliderDrag = new EventEmitter<any>();

  @ViewChild( VirtualScrollerComponent ) public virtualScroller: VirtualScrollerComponent;
  @ViewChild('Bounds') sliderContainer: ElementRef;

  vertPos = 0;
  sliderHeight = 0;
  pxToMoveToNextIndex:number = 0;
  indexToMoveTo = 0;
  draggerLabel = '';
  isDragging = false;
  config = Config;

  constructor(
    private events: Events,
    private surveyAttemptsService: SurveyAttemptsService,
    private loadingCtrl: LoadingController,
    private userPermissionsService: UserPermissionsService
  ) {}

  surveyDataInit()
  {
    // Set initial filter date range (last 30 days) for client and get surveys.
    let dateStart = moment().subtract(90, 'days').format('YYYY-MM-DD');
    let dateEnd = moment().format('YYYY-MM-DD');
    this.surveyData.filter.dateStart = dateStart;
    this.surveyData.filter.dateEnd = dateEnd;

    // If we have a region set, determine available reggions.
    if ( this.surveyData.survey.region_set !== undefined )
    {
      this.surveyData.filter.location = [];
      let filteredRegionSet = this.userPermissionsService.filterRegionSetBasedOnUserPermissions( this.surveyData.survey.region_set );
      for ( let rs of filteredRegionSet )
      {
        this.surveyData.filter.location.push( rs.list_item.title );
      }
    }
    this.getSurveyAttempts();
  }

  getSurveyAttempts( searching = false, comparing = false)
  {
    let loader = this.loadingCtrl.create({
      content: "Getting data..."
    });
    if ( !searching )
    {
      loader.present();
    }

    this.surveyAttemptsService.getSurveyAttempts( this.survey.survey_id, comparing )
      .subscribe( data => {
        let filteredActivities = this.filterSurveyAttempts( data );
        if ( comparing )
        {
          this.surveyData.compareActivities = filteredActivities;
        } else {
          this.surveyData.activities = filteredActivities;
          this.pxToMoveToNextIndex = this.sliderHeight / data.length;
          this.events.publish('activities:loaded');
        }
        loader.dismiss();
      });
  }

  // TODO: Refactor this using .filter and .map.  It's really ugly.
  filterSurveyAttempts( activities )
  {
    let filteredActivities = [];

    for ( let a of activities )
    {
      for ( let df of this.surveyData.localDataFilter )
      {
        if ( df.selections.length > 0 )
        {
          for ( let r of a.responses )
          {
            if ( r.survey_question_id === df.questionId )
            {
              for ( let s of df.selections )
              {
                if ( s === r.text )
                {
                  filteredActivities.push( a );
                }
              }
            }
          }
        }
      }
    }
    if ( filteredActivities.length > 0 )
    {
      activities.length = 0;
      for ( let a of filteredActivities )
      {
        activities.push( a );
      }
    }

    return activities;
  }

  // Slider methods
  onMoving( ev )
  {
    this.vertPos = ev.y;
    this.indexToMoveTo = Math.floor( ev.y / this.pxToMoveToNextIndex );
    this.virtualScroller.scrollToIndex(this.indexToMoveTo);
    this.draggerLabel = moment( this.surveyData.activities[this.indexToMoveTo].date ).format( 'MMMM YYYY' );
    this.isDragging = true;
    this.sliderDrag.emit(true);
  }

  onStopMoving( ev )
  {
    this.isDragging = false;
    this.sliderDrag.emit(false);
  }

  showSurveyAttemptPage( surveyAttempt )
  {
    this.events.publish('survey-attempt:view', {
      surveyAttempt: surveyAttempt
    });
  }

  ngOnInit()
  {
    this.sliderHeight = this.sliderContainer.nativeElement.offsetHeight;

    this.events.unsubscribe( 'survey:load-data' );
    this.events.unsubscribe( 'survey:update-survey-list' );

    this.events.subscribe( 'survey:load-data', () => {
      this.surveyDataInit();
    });

    // Event fired after survey attempt is added or edited.
    this.events.subscribe( 'survey:update-survey-list', ( surveyAttemptObj ) => {
      console.log(surveyAttemptObj);

      // If we were editing a surveyAttempt, we know it's currently in the surveyData collection.
      // Go ahead and update it.
      if ( surveyAttemptObj.edit === true )
      {
        let replaceIndex = this.surveyData.activities.map( function( activity ) 
        { 
          return activity.survey_attempt_id; 
        }).indexOf( surveyAttemptObj.surveyAttempt.survey_attempt_id );
        this.surveyData.activities[ replaceIndex ] = surveyAttemptObj.surveyAttempt;
      } else {
        this.surveyData.activities.unshift( surveyAttemptObj.surveyAttempt );
      }

      this.virtualScroller.refresh();
    });
  }
}