import { 
  Component, 
  EventEmitter, 
  Input, 
  Output,
  ViewChild
} from '@angular/core';

import { 
  Events
} from 'ionic-angular';

import { RegonSetFilterComponent } from './region-set-filter.component';
import moment from 'moment';

@Component({
  selector: 'survey-activity-list-search-filter-component',
  templateUrl: 'survey-activity-list-search-filter.component.html'
})

export class SurveyActivityListSearchFilterComponent
{
  @ViewChild( RegonSetFilterComponent ) regionSetFilterComponent: RegonSetFilterComponent;

  @Input() surveyData:any = {};
  @Output() doSearch = new EventEmitter<any>();
  @Output() doCompare = new EventEmitter<any>();

  showFilters = false;
  showCompares = false;
  definedFilters = [];
  
  constructor(
    public events: Events
  ) {
  }

  searchLogEntries( ev )
  {
    let search_term = ev.target.value;

    if ( search_term && search_term.length > 3 )
    {
      this.surveyData.filter.search = search_term;
    } else {
      this.surveyData.filter.search = '';
    }
    this.emitSearchEvent( true );
  }

  emitSearchEvent( searching = false )
  {
    this.doSearch.emit( searching );
  }

  emitCompareEvent()
  {
    this.doCompare.emit();
  }

  setRegionSetFilter( locationArray )
  {
    this.surveyData.filter.location = locationArray;
  }

  toggleFilterPanel()
  {
    this.showCompares = false;
    this.showFilters = !this.showFilters;
  }

  toggleComparePanel()
  {
    this.showFilters = false;
    this.showCompares = !this.showCompares;
  }

  updateCompareDateRange( ev )
  {
    let dateStart = moment( this.surveyData.filter.dateStart );
    let dateEnd = moment( this.surveyData.filter.dateEnd );

    let diff, newDateStart, newDateEnd;
    switch ( ev )
    {
      case 'period':
        diff = dateEnd.diff( dateStart, 'days' );
        newDateStart = moment( this.surveyData.filter.dateStart ).subtract( diff, 'days' ).format('YYYY-MM-DD');
        newDateEnd = moment( this.surveyData.filter.dateEnd ).subtract( diff, 'days' ).format('YYYY-MM-DD');
        break;
      case 'year':
        newDateStart = moment( this.surveyData.filter.dateStart ).subtract( 1, 'years' ).format('YYYY-MM-DD');
        newDateEnd = moment( this.surveyData.filter.dateEnd ).subtract( 1, 'years' ).format('YYYY-MM-DD');
        break;
    }

    this.surveyData.compares.dateRange = ev;
    this.surveyData.compares.dateStart = newDateStart;
    this.surveyData.compares.dateEnd = newDateEnd;  
  }

  updateCompareRegion( ev )
  {
    this.surveyData.compares.location = [];
    this.surveyData.compares.location.push( ev );
    this.surveyData.compares.dateStart = this.surveyData.filter.dateStart;
    this.surveyData.compares.dateEnd = this.surveyData.filter.dateEnd;
  }

  updateLocalFilter( selections, questionId, index )
  {
    // Clear filter entry first.
    if ( selections.length > 0 )
    {
      this.surveyData.localDataFilter[index].selections = selections
    } else {
      this.surveyData.localDataFilter[index].selections = [];
    }
  }

  clearFilters()
  {
    for ( let f of this.surveyData.localDataFilter )
    {
      f.selections = [];
    }
    if ( this.surveyData.survey.region_id > 0 )
    {
      this.surveyData.filter.location = [];
      this.regionSetFilterComponent.clearSelection();
    }
  }

  clearCompares()
  {
    // Not sure why clearing the object this way works
    // and this.surveyData.compares = {} doesn't work.
    this.surveyData.compares.location[0] = '';
    this.surveyData.compares.dateRange = '';
    this.surveyData.compares.dateStart = '';
    this.surveyData.compares.dateEnd = '';

    // Remove any compare data we have.
    this.surveyData.compareActivities.length = 0;

    this.events.publish('charts:clear-compares');
  }

  ngOnInit()
  {
    this.surveyData.compares.location = [];
    this.clearCompares();

    this.events.subscribe( 'survey:load-data', ( data ) => {
      // Do we have any defined filters for this survey?
      if ( this.surveyData.survey.report_elements.length > 0 )
      {
        for ( let re of this.surveyData.survey.report_elements )
        {
          if ( re.is_filter === 'Yes' )
          {
            let filterQuestion = this.surveyData.survey.questions.filter( question =>
              question.survey_question_id === re.survey_question_id
            ); 

            this.definedFilters.push({
              label: re.label,
              filterOptions: filterQuestion[0].answers,
              questionId: re.survey_question_id
            });

            this.surveyData.localDataFilter.push({
              questionId: re.survey_question_id,
              selections: []
            });
          }
        }
      }
    });
  }
}