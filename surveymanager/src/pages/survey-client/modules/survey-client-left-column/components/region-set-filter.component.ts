import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

// Providers
import { UserPermissionsService } from '../../../../../providers/users/user-permissions-service';
import { Config } from '../../../../../providers/config/config';

@Component({
  selector: 'region-set-filter',
  template: `
    <ng-container *ngIf="childrenArray.length > 0">
      <ion-item no-padding *ngFor="let children of childrenArray; let i = index;">
        <ion-select 
          multiple 
          class="full-width"
          [(ngModel)]="selectedChildrenArray[i]" 
          (ionChange)="showChildSelect($event, i)" 
          placeholder="{{ children[0].list_item.descriptor }}"
        >
          <ion-option *ngFor="let child of children" [value]="child">
            {{ child.list_item.title }}
          </ion-option>
        </ion-select>
      </ion-item>
    </ng-container>
  `
})

export class RegonSetFilterComponent
{
  @Output() regionSetFilterChange = new EventEmitter<any>();
  @Input() surveyData:any = {};

  regionSet:any = {};
  childrenArray = [];
  selectedChildrenArray = [];

  config = Config;

  constructor(
    private userPermissionsService: UserPermissionsService
  ) {
  }

  showChildSelect( nodes, index )
  {   
    console.log(nodes);
    // Get rid of all elements in childrenArray > index.
    this.childrenArray.length = index + 1;

    // If we haven't selected any nodes from a child select,
    // then clear the NEXT child select so we're not passing 
    // in values placed there before.
    if ( nodes.length === 0 )
    {
      this.selectedChildrenArray.length = index + 1;
    }  else {
      // Get children of all selected nodes.
      let selectedNodeChildren = [];
      for ( let n of nodes )
      {
        if ( n.children )
        {
          for ( let c of n.children )
          {
            selectedNodeChildren.push( c ); 
          }
        }
      }

      if ( selectedNodeChildren.length > 0 )
      {
        this.childrenArray.push( selectedNodeChildren );
      }
    
      // Convert selectedChildrenArray into something we can pass to the server.
      let selectedRegionItems = [];
      for ( let sca of this.selectedChildrenArray )
      {
        for ( let child of sca )
        {
          selectedRegionItems.push( child.list_item.title );
        }
      }
      this.regionSetFilterChange.emit( selectedRegionItems );
    }
  }

  clearSelection()
  {
    // childrenArray is array of region children (eg Counties, Districts)
    // selectedChildrenArray is array of region selections made by user.
    let j = this.childrenArray.length;
    while ( j-- ) 
    {
      this.selectedChildrenArray[j] = null;

      // Don't go all the way to 0 or else we'll remove first Region parent too.
      if ( j > 0 )
      {
        this.childrenArray.splice( j, 1 );
      }
    }
    this.selectedChildrenArray[0] = null;
  }

  ngOnInit()
  {
    this.regionSet = this.surveyData.survey.region_set;

    // Determine available regions.
    this.childrenArray.push( this.userPermissionsService.filterRegionSetBasedOnUserPermissions( this.regionSet ) );
  }
}
