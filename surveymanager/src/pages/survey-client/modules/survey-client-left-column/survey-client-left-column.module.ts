import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

// Views
import { SurveyClientLeftColumnParentComponent } from './views/survey-client-left-column-parent';

// Components
import { RegonSetFilterComponent } from './components/region-set-filter.component';
import { SurveyActivityListSearchFilterComponent } from './components/survey-activity-list-search-filter.component';
//import { CustomDatePickerComponent } from '../../../../components/date-picker/date-picker.component';

// Modules
import { CustomDatePickerComponentModule } from '../../../../components/date-picker/date-picker.module';
import { VirtualScrollerModule } from 'ngx-virtual-scroller';
import { AngularDraggableModule } from 'angular2-draggable';
import { UserDetailsComponentModule } from '../../../../components/user-details/user-details.module';

// Providers

@NgModule({
  declarations: [
    SurveyClientLeftColumnParentComponent,
    SurveyActivityListSearchFilterComponent,
    RegonSetFilterComponent
  ],
  imports: [
    CustomDatePickerComponentModule,
    VirtualScrollerModule,
    AngularDraggableModule,
    UserDetailsComponentModule,
    IonicPageModule.forChild(SurveyClientLeftColumnParentComponent)
  ],
  providers: [
  ],
  exports: [
    SurveyClientLeftColumnParentComponent
  ]
})
export class SurveyClientLeftColumnModule {}
