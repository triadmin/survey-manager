import { 
  Component,
  ViewChildren,
  QueryList
} from '@angular/core';
import { 
  NavController, 
  Events,
  PopoverController,
  ModalController,
  NavParams,
  Nav
} from 'ionic-angular';

// Services
import { SurveyAttemptsService } from '../../../providers/surveys/survey-attempts-service';
import { HelperSurveyService } from '../../../providers/helpers/helper-survey-service';
import { Config } from '../../../providers/config/config';
import { UserPermissionsService } from '../../../providers/users/user-permissions-service';
import { UsersService } from '../../../providers/users/users-service';
import { SurveyData } from '../../../providers/surveys/survey-client-data';
import { PdfService } from '../../../providers/pdf/pdf-service';
import { RegionsService } from '../../../providers/surveys/regions-service';
import { MessagesService } from '../../../providers/messages/messages-service';
import { TasksService } from '../../../providers/tasks/tasks-service';
import { GoalsService } from '../../../providers/goals/goals-service';

// Pages
import { SurveysPage } from '../../survey-create/surveys';
import { SurveyViewPage } from '../survey-attempt-view/survey-view';
import { SurveyDataEntryPage } from '../survey-data-entry/survey-data-entry';
import { RegionMapPage } from '../region-map/region-map';
import { SelectSurveyPage } from '../select-survey/select-survey';
import { GoalsPage } from '../goals/goals-list';

// Report element components
import { DonutComponent } from '../reports/report-elements/donut.component';
import { PieComponent } from '../reports/report-elements/pie.component';
import { TableComponent } from '../reports/report-elements/table.component';
import { BarHorizontalComponent } from '../reports/report-elements/bar-horizontal.component';
import { GaugeComponent } from '../reports/report-elements/gauge.component';
import { TrendComponent } from '../reports/report-elements/trend.component';
import { GoalReportComponent } from '../reports/report-elements/goal-report.component';

// Popovers
import { SurveyClientMenuPopoverPage } from '../../../components/survey-client/menu.component';
import { CompareRegionPopoverPage } from '../../../components/survey-client/compare-region.popover';
import { CompareDateRangePopoverPage } from '../../../components/survey-client/compare-date-range.popover';

import moment from 'moment';

@Component({
  selector: 'page-dashboard-survey',
  templateUrl: 'dashboard-survey.html'
})
export class DashboardSurveyClientPage {
  // Get handles to our report element component.
  @ViewChildren( DonutComponent ) donutCharts: QueryList<DonutComponent>;
  @ViewChildren( PieComponent ) pieCharts: QueryList<PieComponent>;
  @ViewChildren( TableComponent ) tableCharts: QueryList<TableComponent>;
  @ViewChildren( BarHorizontalComponent ) barHorizontalCharts: QueryList<BarHorizontalComponent>;
  @ViewChildren( GaugeComponent ) gaugeCharts: QueryList<GaugeComponent>;
  @ViewChildren( TrendComponent ) trendCharts: QueryList<TrendComponent>;

  headerData: any;
  survey: any = {};
  surveyData: any = SurveyData;
  config: any = Config;
  reportElements = [];
  scrollable = 'fixed';

  constructor(
    public navCtrl: NavController,
    public surveyAttemptsService: SurveyAttemptsService,
    public userPermissionsService: UserPermissionsService,
    public events: Events,
    public navParams: NavParams,
    public nav: Nav,
    public popoverCtrl: PopoverController,
    public pdfService: PdfService,
    public regionsService: RegionsService,
    public usersService: UsersService,
    public messagesService: MessagesService,
    public tasksService: TasksService,
    public helperSurveyService: HelperSurveyService,
    public goalsService: GoalsService
  ) {
  }

  setUpEvents()
  {
    // Set our survey attempt list menu.
    this.events.publish( 'menu:close', {
      menuType: 'surveyList'
    });

    this.events.subscribe( 'task:add', ( data ) => {
      this.getOpenTasks();
    });

    this.events.subscribe( 'goals:refresh', ( data ) => {
      this.getGoalsForSurveyForUser();
    });
  }

  presentMenuPopover( ev, purpose )
  {
    let popover = this.popoverCtrl.create( SurveyClientMenuPopoverPage, {
      purpose: purpose
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      switch( data )
      {
        case 'new-activity':
          this.showSurveyDataEntryPage();
          break;
        case 'reports':
          this.showSurveyDataEntryPage();
          break;
        case 'print':
          this.printReportPdf();
          break;
        case 'goals':
          this.showGoalsPage();
          break;
        case 'select-survey':
          this.showSelectSurveyPage();
          break;
      }
    });
  }

  surveyInit()
  {
    // Parse render strings.
    let viewListTemplate = 'default-survey-list';
    if ( this.survey.config_rendering !== '' && this.survey.config_rendering !== null )
    {
      let renderObj = JSON.parse( this.survey.config_rendering );
      viewListTemplate = renderObj.viewList;
    }

    this.events.publish( 'surveyList:init', {
      surveyId: this.survey.survey_id
    });

    // Get region set if we have one.
    if ( this.survey.region_id > 0 )
    {
      this.regionsService.getRegion( this.survey.region_id )
        .subscribe( data => {
          this.survey.region_set = data;
          this.events.publish( 'survey:load-data' );
        });
    } else {
      setTimeout(() => {
        this.events.publish( 'survey:load-data' );
      }, 500);
    }
    
    this.surveyData.survey = this.survey;

    // Get survey users so we can implement messages.
    this.usersService.getUsersForSurvey( this.survey.survey_id )
      .subscribe( data => {
        this.surveyData.users = data;
      });

    this.getUnreadMessages();
    this.getOpenTasks();
    this.findDashboardReportElements();
    this.listenForSurveyAttemptClick();
    this.getGoalsForSurveyForUser();
  }

  getUnreadMessages()
  {
    // Get any unread messages for this user.
    this.messagesService.getMessagesToUserForSurvey( this.config.USER.user_app_record.survey_user_id, this.survey.survey_id, 'Unread' )
      .subscribe( data => {
        this.config.USER.unread_messages = data;

        // Emit unread messages so we have them in our user details component.
        this.events.publish( 'messages:unread-loaded', {
          messages: data
        });
      });
  }

  getGoalsForSurveyForUser()
  {
    this.goalsService.getGoalsForUserForSurvey( this.config.USER.user_app_record.survey_user_id, this.survey.survey_id  )
      .subscribe( data => {
        this.surveyData.goals = data;
    });
  }

  getOpenTasks()
  {
    // Get any open tasks for this user.
    this.tasksService.getTasksForUserForSurvey( this.config.USER.user_app_record.survey_user_id, this.survey.survey_id, 'Open' )
      .subscribe( data => {
        this.config.USER.open_tasks = data;

        // Emit open tasks so we have them in our user details component.
        this.events.publish( 'tasks:open-loaded', {
          tasks: data
        });
      });
  }

  findDashboardReportElements()
  {
    for ( let re of this.survey.report_elements )
    {
      if ( re.is_on_dashboard === 'Yes' )
      {
        this.reportElements.push( re );
      }
    }
  }

  listenForSurveyAttemptClick()
  {
    // Unsubscribe first so we don't end up with multiple subscriptions.
    this.events.unsubscribe('survey-attempt:view');

    // Listen for survey attempt list click.
    this.events.subscribe('survey-attempt:view', ( data ) => {
     // REMOVE: this is just to test the goal algorithm.
     this.goalsService.testActivityAgainstGoals( data.surveyAttempt ); 


      // Do we already have an instance of SurveyViewPage on the stack?
      let views = this.navCtrl.getViews();
      let viewToMoveTo = views.find((viewController) => viewController.instance instanceof SurveyViewPage);
      
      if (viewToMoveTo === undefined) {
        // If the page is not yet on the navControllers stack, push it
        this.navCtrl.push( SurveyViewPage, {
          surveyAttempt: data,
          survey: this.survey
        });
      } else {
        // If the page is on the stack, pop it off first 
        // before pushing a new one on the stack.
        this.navCtrl.pop();
        this.navCtrl.push(SurveyViewPage, {
          surveyAttempt: data,
          survey: this.survey
        });
      }
    });
  }

  showSelectSurveyPage()
  {
    this.navCtrl.setRoot( SelectSurveyPage );
  }

  showRegionMapPage()
  {
    this.navCtrl.push( RegionMapPage, {
      survey: this.survey
    });
  }

  showSurveyDataEntryPage()
  {
    this.navCtrl.push( SurveyDataEntryPage, {
      survey: this.survey
    });
  }

  printReportPdf()
  {
    let report = {
      charts: {
        tables: this.tableCharts.toArray(),
        donuts: this.donutCharts.toArray(),
        pies: this.pieCharts.toArray(),
        barsHorizontal: this.barHorizontalCharts.toArray(),
        gauges: this.gaugeCharts.toArray(),
        trends: this.trendCharts.toArray()
      },
      reportConfig: this.reportElements,
      survey: this.survey
    };
    this.pdfService.createReportPDFDocDefinition( report );
  }

  // Disable scrolling when dragging slider.
  sliderDrag( ev )
  {
    if ( ev )
    {
      this.scrollable = 'no-scroll';
    } else {
      this.scrollable = 'fixed';
    }
  }

  presentCompareRegionPopover( ev )
  {
    let popover = this.popoverCtrl.create( CompareRegionPopoverPage, {
      regions: this.survey.region_set
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      console.log(data);
    });
  }

  presentCompareDateRangePopover( ev )
  {
    let popover = this.popoverCtrl.create( CompareDateRangePopoverPage, {
      
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      console.log(data);
    });
  }

  showGoalsPage()
  {
    this.navCtrl.push( GoalsPage );
  }

  ionViewDidLoad()
  {
    this.helperSurveyService.clearSurveyData();

    this.setUpEvents();

    if ( this.navParams.get('survey') )
    {
      this.survey = this.navParams.get('survey');
      this.surveyInit();
    } else {
      this.events.publish('app:log', {
        message: 'We need a survey first!',
        status: 'error',
        type: 'no survey'
      }); 
    }
  }
}
