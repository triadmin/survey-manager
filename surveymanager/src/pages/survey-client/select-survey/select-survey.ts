import { 
  Component
} from '@angular/core';
import { 
  NavController,
  Events
} from 'ionic-angular';

// Providers
import { Config } from '../../../providers/config/config';
import { SurveysService } from '../../../providers/surveys/surveys-service';

// Pages
import { DashboardSurveyClientPage } from '../dashboard/dashboard-survey';

@Component({
  selector: 'page-select-survey',
  templateUrl: 'select-survey.html'
})
export class SelectSurveyPage 
{
  surveys = [];
  config = Config;

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public surveysService: SurveysService
  ) {
    // Close left hand menu.
    this.events.publish( 'menu:close', {
      menuType: 'surveyList'
    });
  }

  showSurveyDashboardPage( survey )
  { 
    console.log(survey);
    this.navCtrl.setRoot( DashboardSurveyClientPage, {
      survey: survey
    });
  }

  ionViewDidLoad()
  { 
    // Get full survey records.
    for ( let s of Config.USER_APP_RECORD.surveys )
    {
      this.surveysService.getSurvey( s.survey_id )
        .subscribe( data => {
          let d = this.surveysService.setProjectObj( data );
          this.surveys.push( d );
        });
    }
  }
}