import { Component } from '@angular/core';
import { 
  NavController, 
  Events,
  NavParams,
  ModalController,
  Platform,
  PopoverController,
  ActionSheetController
} from 'ionic-angular';

// Ionic Native
//import { Keyboard } from '@ionic-native/keyboard';

import { SurveyAttemptsService } from '../../../providers/surveys/survey-attempts-service';
import { Config } from '../../../providers/config/config';
import { GoalsService } from '../../../providers/goals/goals-service';

// Popovers
import { SurveyClientMenuPopoverPage } from '../../../components/survey-client/menu.component';

import moment from 'moment';

@Component({
  selector: 'page-survey-data-entry',
  templateUrl: 'survey-data-entry.html'
})
export class SurveyDataEntryPage {
  headerData: any;
  survey: any = {};
  surveyAttempt: any = {};
  edit: boolean = false;
  currentPage: number = 0;
  questions: any = [];
  config = Config;
  pageTitle = 'New Survey Entry';

  constructor(
    public navCtrl: NavController,
    public surveyAttemptsService: SurveyAttemptsService,
    public events: Events,
    public navParams: NavParams,
    public platform: Platform,
    public popoverCtrl: PopoverController,
    public actionSheetCtrl: ActionSheetController,
    public goalsService: GoalsService
  ) {
    if ( this.navParams.get('survey') )
    {
      this.survey = this.navParams.get('survey');
      console.log(this.survey);
    } else {
      this.events.publish('app:log', {
        message: 'We need a survey first!',
        status: 'error',
        type: 'no survey'
      }); 
    }

    // Have we passed in a surveyAttempt? If so, we're in edit mode.
    if ( this.navParams.get('surveyAttempt') )
    {
      this.surveyAttempt = this.navParams.get('surveyAttempt');
      this.pageTitle = 'Editing ' + this.surveyAttempt.title;
      this.edit = true;
      console.log('editing survey attempt');
      console.log(this.surveyAttempt);

      // Set initial questions page.
      this.switchPage();
    } else {
      console.log('creating new survey attemopt');
      this.createSurveyAttempt();
    }
  }

  presentMenuPopover( ev, purpose )
  {
    let popover = this.popoverCtrl.create( SurveyClientMenuPopoverPage, {
      purpose: purpose
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      switch( data )
      {
        case 'cancel':
          this.presentDeleteActivityActionSheet();
          break;
      }
    });
  }

  handleCancel()
  {
    if ( !this.edit )
    {
      // Then we ask if we want to discard this activity.
    }
  }

  deleteActivity()
  {
    this.surveyAttemptsService.deleteSurveyAttempt( this.surveyAttempt )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Activity has been deleted!',
          status: 'warning',
          type: 'delete activity'
        });
        this.navCtrl.pop();
      });
  }

  presentDeleteActivityActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to cancel this activity?',
      buttons: [
        {
          text: 'Cancel activity',
          role: 'destructive',
          handler: () => {
            this.deleteActivity();
          }
        },
        {
          text: 'Keep going',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  createSurveyAttempt()
  {
    let attempt = {
      survey_id: this.survey.survey_id,
      survey_user_id: this.config.USER_APP_RECORD.survey_user_id,
      title: '[Title]',
      date: moment().format('YYYY-MM-DD'),
      status: 'Active'
    }
    this.surveyAttemptsService.saveSurveyAttempt( attempt )
      .subscribe( data => {
        this.surveyAttempt = data;
        console.log(this.surveyAttempt);
        // Set initial questions page.
        // We do this in the callback so that the new
        // surveyAttemptId can be set on the question 
        // components on the first page.
        this.switchPage();
      });
  }  

  switchPage( direction = null )
  {
    if ( direction === 'next' )
    {
      this.currentPage++;
    } else if ( direction === 'prev') {
      this.currentPage--;
    }

    // Set questions page.
    this.questions = this.survey.question_pages[ this.currentPage ].questions;
    console.log(this.questions);

    if ( this.platform.is('cordova') )
    {
      //this.keyboard.close();
    }
  }

  leaveSurveyTasks()
  {
    // Get full survey attempt.
    this.surveyAttemptsService.getSurveyAttempt( this.survey.survey_id, this.surveyAttempt.survey_attempt_id )
      .subscribe( data => {
        this.goalsService.testActivityAgainstGoals( data );

        let surveyAttemptObj = {
          surveyAttempt: data,
          edit: this.edit
        }

        this.events.publish( 'survey:update-survey-list', surveyAttemptObj );

        this.navCtrl.pop();
      });
  }

  ngOnInit()
  {
  }
}