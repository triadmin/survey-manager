import { 
  Component, 
  Input,
  ViewChildren,
  QueryList
} from '@angular/core';

// Question type render components
import { CheckboxComponent } from '../../../../components/question-types/checkbox';
import { RadioComponent } from '../../../../components/question-types/radio';
import { TextComponent } from '../../../../components/question-types/text';
import { TextareaComponent } from '../../../../components/question-types/textarea';
import { SelectComponent } from '../../../../components/question-types/select';
import { SelectmultiComponent } from '../../../../components/question-types/selectmulti';
import { SurveyAttemptComponent } from '../../../../components/question-types/survey-attempt';
import { ListWithHeadersTextComponent } from '../../../../components/question-types/list-with-headers-text';

@Component({
  selector: 'survey-view-default-template',
  templateUrl: 'survey-view.default.template.html'
})

export class SurveyViewDefaultComponent {
  // Get handles to our report element component.
  @ViewChildren( CheckboxComponent ) checkboxes: QueryList<CheckboxComponent>;
  @ViewChildren( RadioComponent ) radios: QueryList<RadioComponent>;
  @ViewChildren( TextComponent ) texts: QueryList<TextComponent>;
  @ViewChildren( TextareaComponent ) textareas: QueryList<TextareaComponent>;
  @ViewChildren( SelectComponent ) selects: QueryList<SelectComponent>;
  @ViewChildren( SelectmultiComponent ) selectmulti: QueryList<SelectmultiComponent>;
  @ViewChildren( SurveyAttemptComponent ) surveyAttempts: QueryList<SurveyAttemptComponent>;
  @ViewChildren( ListWithHeadersTextComponent ) listWithHeadersTexts: QueryList<ListWithHeadersTextComponent>;

  @Input() surveyAttempt: any = {};
  @Input() survey: any = {};

  questions = [];

  constructor(
  ) {
  }

  ngOnInit()
  {
    // Format questions.
    this.questions = this.survey.questions;
  }
}