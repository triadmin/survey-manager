import { 
  Component, 
  Input,
  Output,
  EventEmitter,
  ViewChildren,
  QueryList
} from '@angular/core';

// Question type render components
import { CheckboxComponent } from '../../../../../components/question-types/checkbox';
import { RadioComponent } from '../../../../../components/question-types/radio';
import { TextComponent } from '../../../../../components/question-types/text';
import { TextareaComponent } from '../../../../../components/question-types/textarea';
import { SelectComponent } from '../../../../../components/question-types/select';
import { SelectmultiComponent } from '../../../../../components/question-types/selectmulti';
import { SurveyAttemptComponent } from '../../../../../components/question-types/survey-attempt';
import { ListWithHeadersTextComponent } from '../../../../../components/question-types/list-with-headers-text';

@Component({
  selector: 'survey-view-tnf-template',
  templateUrl: 'survey-view.tnf.template.html'
})

export class SurveyViewTnfComponent {
  // Get handles to our report element component.
  @ViewChildren( CheckboxComponent ) checkboxes: QueryList<CheckboxComponent>;
  @ViewChildren( RadioComponent ) radios: QueryList<RadioComponent>;
  @ViewChildren( TextComponent ) texts: QueryList<TextComponent>;
  @ViewChildren( TextareaComponent ) textareas: QueryList<TextareaComponent>;
  @ViewChildren( SelectComponent ) selects: QueryList<SelectComponent>;
  @ViewChildren( SelectmultiComponent ) selectmulti: QueryList<SelectmultiComponent>;
  @ViewChildren( SurveyAttemptComponent ) surveyAttempts: QueryList<SurveyAttemptComponent>;
  @ViewChildren( ListWithHeadersTextComponent ) listWithHeadersTexts: QueryList<ListWithHeadersTextComponent>;

  @Input() surveyAttempt: any = {};
  @Input() survey: any = {};
  @Output() dirtyData = new EventEmitter<any>();

  constructor(
  ) {
  }

  ngOnInit()
  {}
}