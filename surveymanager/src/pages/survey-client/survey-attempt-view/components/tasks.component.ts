import { 
  Component, 
  Output,
  EventEmitter,
  Input
} from '@angular/core';
import {
  ModalController,
  ActionSheetController,
  Events
} from 'ionic-angular';

import { TasksService} from '../../../../providers/tasks/tasks-service';

// Modals
import { NewTaskModalPage } from '../../../../components/modals/new-task.modal';

@Component({
  selector: 'tasks',
  template: `
    <ion-card no-margin>
      <ion-card-header>
        <ion-row>
          <ion-col col-8>Tasks</ion-col>
          <ion-col col-4 no-padding>
            <button ion-button clear icon-start float-right no-margin (click)="presentNewTaskModal()">
              <ion-icon name="checkmark-circle-outline"></ion-icon> New Task
            </button>
          </ion-col>
        </ion-row>
      </ion-card-header>
      <ion-card-content>
        <ion-list>
          <ion-item *ngFor="let t of tasks" [class.task-completed]="isTaskComplete(t)">
            <button 
              item-start 
              icon-only 
              (click)="toggleTaskComplete(t)"
            >
              <ion-icon name="checkmark-circle-outline" [color]="isTaskComplete(t) ? 'light-gray' : 'success'"></ion-icon>
            </button>  

            <small class="muted">Created {{ t.created_at | moment_from_now }}</small>
            <p [innerHTML]="t.task_text" margin-top></p>

            <button 
              item-end 
              icon-only 
              (click)="presentDeleteTaskActionSheet(t)"
            >
              <ion-icon name="trash" color="danger"></ion-icon>
            </button>
          </ion-item>
        </ion-list>
      </ion-card-content>
    </ion-card>
  `
})

export class TasksComponent
{
  @Input() surveyAttempt;
  tasks = [];

  constructor(
    private modalCtrl: ModalController,
    private actionSheetCtrl: ActionSheetController,
    private tasksService: TasksService,
    private events: Events
  ) {
    this.events.unsubscribe('task:add');

    this.events.subscribe('task:add', ( data ) => {
      this.tasks.unshift( data.task );
    });
  }
  
  getTasks()
  {
    this.tasksService.getTasksForSurveyAttempt( this.surveyAttempt.survey_id, this.surveyAttempt.survey_attempt_id )
      .subscribe( data => {
        this.tasks = data;
      });
  }

  presentNewTaskModal()
  {
    let newTaskModal = this.modalCtrl.create( NewTaskModalPage, {
      surveyAttemptId: this.surveyAttempt.survey_attempt_id
    });
    newTaskModal.present();
  }

  toggleTaskComplete( task )
  {
    if ( task.status === 'Open' )
    {
      task.status = 'Completed';
    } else {
      task.status = 'Open';
    }

    // Save task.
    this.tasksService.saveTask( task )
      .subscribe( data => {

      });
  }

  isTaskComplete( task )
  {
    if ( task.status === 'Open' )
    {
      return false;
    }
    return true;
  }

  deleteTask( task )
  {
    this.tasksService.deleteTask( task )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Task has been deleted.',
          status: 'warning',
          type: 'delete task'
        });

        let j = this.tasks.length;
        while ( j-- ) 
        {
          if ( this.tasks[j].task_id === task.task_id )
          {
            this.tasks.splice( j, 1 );
          }
        }
      });
  }

  presentDeleteTaskActionSheet( task )
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this task?',
      buttons: [
        {
          text: 'Delete task',
          role: 'destructive',
          handler: () => {
            this.deleteTask( task );
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  ngOnInit()
  {
    this.getTasks();
  }
}
