import { 
  Component, 
  Input
} from '@angular/core';
import {
  ModalController,
  Events
} from 'ionic-angular';

import { SurveyData } from '../../../../providers/surveys/survey-client-data';
import { MessagesService } from '../../../../providers/messages/messages-service';

// Modals
import { NewMessageModalPage } from '../../../../components/modals/new-message.modal';

@Component({
  selector: 'messages',
  template: `
    <ion-card no-margin>
      <ion-card-header>
        <ion-row>
          <ion-col col-8>Messages</ion-col>
          <ion-col col-4 no-padding>
            <button ion-button clear icon-start float-right no-margin (click)="presentNewMessageModal()">
              <ion-icon name="chatbubbles"></ion-icon> New Message
            </button>
          </ion-col>
        </ion-row>
      </ion-card-header>
      <ion-card-content>
        <ion-list style="height: 20rem; overflow: scroll;">
          <ion-item *ngFor="let m of messages">
            <button icon-only item-start (click)="markAsRead(m)">
              <ion-icon name="checkmark-circle-outline"></ion-icon>
            </button>
            <small class="muted">From <b>{{ m.first_name }} {{ m.last_name }}</b> {{ m.created_at | moment_from_now }}</small>
            <p [innerHTML]="m.message_text" margin-top></p>
          </ion-item>
        </ion-list>
      </ion-card-content>
    </ion-card>
  `
})

export class MessagesComponent
{
  @Input() surveyAttempt:any = {};
  messages = [];

  constructor(
    private modalCtrl: ModalController,
    private messagesService: MessagesService,
    private events: Events
  ) {
    this.events.unsubscribe( 'messages:create' );

    this.events.subscribe('messages:create', ( message ) => {
      if ( message.message.survey_attempt_id === this.surveyAttempt.survey_attempt_id )
      {
        this.getMessages();
      }
    });
  }

  getMessages()
  {
    this.messagesService.getMessagesForSurveyAttempt( this.surveyAttempt.survey_id, this.surveyAttempt.survey_attempt_id )
      .subscribe( data => {
        this.messages = data;
      });
  }

  presentNewMessageModal()
  {
    let newMessageModal = this.modalCtrl.create( NewMessageModalPage, {
      surveyAttemptId: this.surveyAttempt.survey_attempt_id
    });
    newMessageModal.present();
  }

  ngOnInit()
  {
    this.getMessages();
  }
}
