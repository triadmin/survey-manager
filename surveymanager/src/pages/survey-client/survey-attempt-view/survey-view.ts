import { 
  Component, 
  ViewChild,
  ElementRef
} from '@angular/core';
import { 
  Events, 
  PopoverController,
  NavController,
  NavParams,
  Nav,
  ActionSheetController
} from 'ionic-angular';

// Services
import { SurveyAttemptsService } from '../../../providers/surveys/survey-attempts-service';
import { HelperService } from '../../../providers/helpers/helper-service';
import { HelperSurveyService } from '../../../providers/helpers/helper-survey-service';
import { HelperPDFService } from '../../../providers/helpers/helper-pdf-service';
import { PdfService } from '../../../providers/pdf/pdf-service';
import { SurveyData } from '../../../providers/surveys/survey-client-data';

// Pages
import { SurveyDataEntryPage } from '../survey-data-entry/survey-data-entry';

// Popovers
import { SurveyClientMenuPopoverPage } from '../../../components/survey-client/menu.component';

// View templates
import { SurveyViewDefaultComponent } from './templates/survey-view.default.template';
import { SurveyViewTnfComponent } from './templates/custom/survey-view.tnf.template';

@Component({
  selector: 'page-survey-view',
  templateUrl: 'survey-view.html'
})
export class SurveyViewPage 
{
  //@ViewChild( SurveyViewDefaultComponent ) surveyViewDefault: SurveyViewDefaultComponent;
  //@ViewChild( SurveyViewTnfComponent ) surveyViewTnf: SurveyViewTnfComponent;
  @ViewChild( 'surveyViewDetailTemplate' ) surveyViewDetailTemplate: ElementRef;

  surveyAttempt: any = {};
  survey: any = {};
  surveyData = SurveyData;
  viewTemplate: string = '';
  viewTemplateChild:any;

  constructor(
    public navCtrl: NavController,
    public surveyAttemptsService: SurveyAttemptsService,
    public events: Events,
    public navParams: NavParams,
    public nav: Nav,
    public popoverCtrl: PopoverController,
    public helperService: HelperService,
    public helperSurveyService: HelperSurveyService,
    public pdfService: PdfService,
    public actionSheetCtrl: ActionSheetController,
    public helperPdfService: HelperPDFService
  ) {
    // Swap out left menu.
    this.events.publish( 'menu:close', {
      menuType: 'surveyList'
    });

    this.survey = SurveyData.survey;

    if ( this.navParams.get('surveyAttempt') )
    {
      let data = this.navParams.get('surveyAttempt');
      this.surveyAttempt = data.surveyAttempt;

      // Get full survey attempt (messages, tasks, etc.)
      this.getFullSurveyAttempt( this.surveyAttempt.survey_attempt_id );
    }
  }

  getFullSurveyAttempt( surveyAttemptId )
  {
    this.surveyAttemptsService.getSurveyAttempt( this.survey.survey_id, surveyAttemptId )
      .subscribe( data => {
        this.surveyAttempt = data;
        this.survey = this.helperSurveyService.matchSurveyQuestionsWithResponses( this.survey, this.surveyAttempt );
      });
  }

  presentMenuPopover( ev, purpose )
  {
    let popover = this.popoverCtrl.create( SurveyClientMenuPopoverPage, {
      purpose: purpose
    });
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      switch( data )
      {
        case 'edit-activity':
          this.showSurveyDataEntryPage( 'edit' );
          break;
        case 'new-activity':
          this.showSurveyDataEntryPage();
          break;
        case 'print':
          this.printActivityPdf();
          break;
        case 'delete':
          this.presentDeleteActivityActionSheet();
          break;
      }
    });
  }

  deleteActivity()
  {
    this.surveyAttemptsService.deleteSurveyAttempt( this.surveyAttempt )
      .subscribe( data => {
        this.events.publish('activities:delete', {
          surveyAttemptId: this.surveyAttempt.survey_attempt_id
        });
        this.events.publish('app:log', {
          message: 'Activity has been deleted!',
          status: 'warning',
          type: 'delete activity'
        });

        // Remove survey attempt from in-memory collection.
        let j = this.surveyData.activities.length;
        while ( j-- ) 
        {
          if ( this.surveyData.activities[j].survey_attempt_id === this.surveyAttempt.survey_attempt_id )
          {
            this.surveyData.activities.splice( j, 1 );
          }
        }

        this.navCtrl.pop();
      });
  }

  presentDeleteActivityActionSheet()
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this activity?',
      buttons: [
        {
          text: 'Delete activity',
          role: 'destructive',
          handler: () => {
            this.deleteActivity();
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {}
        }
      ]
    });

    actionSheet.present();
  }

  showSurveyDataEntryPage( action = null )
  {
    if ( action === 'edit' )
    {
      this.navCtrl.push( SurveyDataEntryPage, {
        survey: this.survey,
        surveyAttempt: this.surveyAttempt
      });
    } else {
      this.navCtrl.push( SurveyDataEntryPage, {
        survey: this.survey
      });
    }
  }

  printActivityPdf()
  {
    let template:any = this.surveyViewDetailTemplate;
    let questionComponentArray = [];

    for ( let q of template.checkboxes.toArray() )
    {
      questionComponentArray.push( q );
    }
    for ( let q of template.radios.toArray() )
    {
      questionComponentArray.push( q );
    }
    for ( let q of template.textareas.toArray() )
    {
      questionComponentArray.push( q );
    }
    for ( let q of template.texts.toArray() )
    {
      questionComponentArray.push( q );
    }
    for ( let q of template.listWithHeadersTexts.toArray() )
    {
      questionComponentArray.push( q );
    }
    for ( let q of template.selects.toArray() )
    {
      questionComponentArray.push( q );
    }

    // Order by page and question order.
    this.survey.questions.sort(function( a, b ){
      return a.page - b.page || a.question_order - b.question_order;
    });

    // Now make the final, ordered array of question components
    // to send to our PDF doc function.
    let finalQuestionComponentArray = [];
    for ( let q of this.survey.questions )
    {
      for ( let qc of questionComponentArray )
      {
        if ( q.survey_question_id === qc.question.survey_question_id )
        {
          finalQuestionComponentArray.push( qc );
        }
      }
    }

    let activityForPrint = {
      questionComponents: finalQuestionComponentArray,
      survey: this.survey,
      surveyAttempt: this.surveyAttempt
    };
    
    this.pdfService.createActivityPDFDocDefinition( activityForPrint );
  }

  ionViewDidLoad()
  {
    if ( this.survey.config_rendering !== '' && this.survey.config_rendering !== null )
    {
      let renderObj = JSON.parse( this.survey.config_rendering );
      this.viewTemplate = renderObj.viewDetail;
    }
  }
}