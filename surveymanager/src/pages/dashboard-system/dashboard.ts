import { Component } from '@angular/core';
import { 
  NavController, 
  Events,
  ModalController
} from 'ionic-angular';

import { Config } from '../../providers/config/config';

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  headerData: any;
  version: string = Config.APP_VERSION;
  constructor(
    public navCtrl: NavController,
    public events: Events,
    public modalCtrl: ModalController
  ) {
    this.headerData = { title: 'System Dashboard' };
  }
}
