import { Pipe, PipeTransform } from '@angular/core';
import moment from 'moment';
import { DomSanitizer } from '@angular/platform-browser';

//
// Converts newlines into html breaks
//
@Pipe({
  name: 'newline'
})

export class NewlinePipe implements PipeTransform {
  transform(value: string, args: string[]): any {
      return value.replace(/(?:\r\n|\r|\n)/g, '<br />');
  }
}

//
// Convert newlines to <p></p>
// Here a link for reference when we want to implement this:
// https://jsfiddle.net/CoryDanielson/aYwLg/ 
//
/*
@Pipe({
  name: 'newp'
})

export class NewpPipe implements PipeTransform {
  transform(value: string, args: string[]): any {
    return value;
  }
}  
*/

@Pipe({
  name: 'fileextension'
})

export class FileextensionPipe implements PipeTransform {
  transform(value: string): any {
    return value.split('.').pop();
  }
}

@Pipe({
  name: 'replace'
})

export class ReplacePipe implements PipeTransform {
  // value: string to modify
  // expr: string in which to search
  // replace: replacement string
  transform(value: string, expr: any, replace: string): any {
    if (!value)
      return value;

    return value.replace(new RegExp(expr, 'gi'), replace);
  }
}

@Pipe({
  name: 'moment_from_now',
})
export class MomentFromNowPipe implements PipeTransform {
  /**
   * Takes a date value and returns a pretty string from current time, 
   * for instance: "four hours ago" or "in eleven minutes".
   */
  transform(value: string, ...args) {
    return moment(value).fromNow();
  }
}

@Pipe({
  name: 'moment_mysql_date',
})
export class MomentConvertMySqlPipe implements PipeTransform {
  /**
   * Takes a MySql date in the form YYYY-MM-DD and
   * converts it to MM/DD/YYYY
   */
  transform( value: string, format: string = null, ...args ) {
    if ( value === '0001-01-01' || value === '' || value === '0000-00-00 00:00:00' )
    {
      return '';
    } 
    if ( format === null )
    {
      format = 'L';
    }
    return moment(value).format(format);
  }
}

@Pipe({name: 'safe_html'})
export class SafeHtmlPipe {
  constructor( private sanitizer:DomSanitizer ){}

  transform( html ) {
    return this.sanitizer.bypassSecurityTrustResourceUrl( html );
  }
}