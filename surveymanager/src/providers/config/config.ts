//
// DEV environment variables
// APP_CLIENT_ID: Identifies this client app to the API
// APP_BASE_GATE: User authenticating must have this base gate
//                in order to log in. 
export let Config = {
  APP_VERSION: '1.00',
  API_ROOT_CORE: 'http://localhost:5080/',
  API_ROOT_SURVEYS: 'http://localhost:5080/api/v1/survey-manager/',
  API_ROOT_APP_ADMIN: 'http://localhost:5080/api/v1/tri-app-admin/',
  ACCESS_TOKEN: '',
  APP_CLIENT_ID: 'WWx97Gf6Uu54DNb',
  APP_NAME: 'survey-manager',
  APP_BASE_GATE: 'survey-manager',
  PLATFORM_WIDTH: 0,
  USER: <any>{},
  USER_APP_RECORD: <any>{},
  TRI_PROJECTS: <any>[],
  USER_APPS: [],
  APP_RECORD: <any>{}
};

//
// PROD environment variables
//
/*
export let Config = {
  APP_VERSION: '1.00',
  API_ROOT_CORE: 'https://api.triwou.org/',
  API_ROOT_SURVEYS: 'https://api.triwou.org/api/v1/survey-manager/',
  API_ROOT_ADMIN: 'https://api.triwou.org/api/v1/tri-app-admin/',
  ACCESS_TOKEN: '',
  APP_CLIENT_ID: 'WWx97Gf6Uu54DNb',
  APP_NAME: 'survey-manager',
  APP_BASE_GATE: 'survey-manager',
  PLATFORM_WIDTH: 0,
  USER: <any>{},
  USER_APP_RECORD: <any>{},
  TRI_PROJECTS: <any>[],
  USER_APPS: [],
  APP_RECORD: <any>{}
};
*/