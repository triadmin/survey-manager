import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';
import moment from 'moment';
import { Config } from '../config/config';

@Injectable()
export class HelperService {
  constructor(
    public events: Events
  ) {}
  
  isObjectEmpty( obj )
  {
    // Returns true if obj is empty {}
    for( var key in obj ) {
      if( obj.hasOwnProperty(key) )
        return false;
    }
    return true;
  }

  makeQuestionIdentifier( question )
  {
    // Remove spaced from question_text.
    let textPart = question.question_text.split(' ').join('');
    // Truncate to 15 characters.
    let textPartTruncated = textPart.substring( 0, 15 );
    // Append 5 character random string and return.
    return textPartTruncated + this.randomString( 5 );
  }

  randomString( length = 15 ) {
    let str = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for( let i = 0; i < length; i++ ) {
        str += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return str;
  }

  getNumberFromString( str )
  {
    let num = parseFloat( str.replace( /[^\d\.]*/g, '') );
    return num;
  }

  getDateToday()
  {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var month: string = String(mm);
    var day: string = String(dd);

    if(dd<10) {
      day = '0'+dd
    }

    if(mm<10) {
      month = '0'+mm
    } 
    let dateToday = yyyy + '-' + month + '-' + day;
    return dateToday;
  }

  getLastDayOfMonth()
  {
    var today = new Date();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var lastDay = new Date(yyyy, mm, 0); 
    return moment(lastDay).format('YYYY-MM-DD');
  }

  //
  // Return a reasonable year range for our date pickers.
  // +- 10 years from current year.
  //
  getYearRange()
  {
    var today = new Date();
    var yyyy = today.getFullYear();
    let dateRange = {
      min: yyyy - 10,
      max: yyyy + 10
    };
    return dateRange;
  }

  //
  // Get number of days between 2 dates in 
  // an array of dates.
  //
  getDaysBetweenDates( datesArray )
  {
    // Order the dates in the array DESC.
    datesArray.sort((a, b) => {
      return new Date(b).getTime() - new Date(a).getTime();
    });

    var start = moment(datesArray[0]);
    var end = moment(datesArray[datesArray.length - 1]);
    return start.diff( end, 'days' ) + 1;
  }

  //
  // Create array of selected days between 2 dates.
  // e.g. "All Mondays, Wednesdays, Fridays between Oct 1 and Dec 15"
  // in MySQL date format.
  //
  getDaysOfWeekBetweenDates( dateStart, dateEnd, selectedWeekdays )
  {
    let start = moment( dateStart );
    let end = moment( dateEnd );
    var datesArray = [];
    // selectedWeekdays is a simple array [0,1,2,3,etc]
    for ( let dayIndex of selectedWeekdays )
    { 
      let tmp = start.clone().day(dayIndex);
      if( tmp.isAfter(start, 'd') ){
        datesArray.push(tmp.format('YYYY-MM-DD'));
      }
      while( tmp.isBefore(end) ){
        tmp.add(7, 'days');
        datesArray.push(tmp.format('YYYY-MM-DD'));
      }
    }

    // Finally, order the dates in the array DESC.
    datesArray.sort((a, b) => {
      return new Date(b).getTime() - new Date(a).getTime();
    });

    return datesArray;
  }

  //
  // Check for empty dates (this is a Go thing 0001-01-01).
  //
  checkEmptyDate( date )
  {
    if ( date === '0001-01-01' )
    {
      return '';
    }
    return date;
  }

  // This is a better version of checkEmptyDate.
  /*
  checkEmptyDate( date )
  {
    if ( date === '0001-01-01' || date === '' || date === '0000-00-00 00:00:00' || typeof date === 'undefined' )
    {
      return false;
    } else {
      return true;
    }  
  }
  */
  
  plural( count, word )
  {
    if ( count !== 1 )
    {
      return word + 's';
    }
    return word;
  }

  showErrors( error )
  {
    let errorMessage = '';
    var errorObj;
    // Single error (error.error.error)
    if ( error.error.error )
    {
      errorMessage = error.error.error;
      errorObj = errorMessage;
    } else {
      // Multiple errors (error.error.errors)
      Object.keys( error.error.errors ).forEach(function(key) {
        errorMessage += error.error.errors[key] + ' ';
      }); 
      errorObj = error.error.errors;
    }  

    this.events.publish('app:log', {
      message: errorMessage,
      status: 'error',
      type: 'form errors'
    });

    // Return errorObj to show form errors
    return errorObj;
  }

  getAvailableApps( user )
  {
    let apps = [];
    if ( typeof user.gate_tag_application !== 'undefined' && user.gate_tag_application.length > 0 )
    {
      let appName = '';
      let appObj:any = {};
      for ( let a of user.gate_tag_application )
      {
        if ( a.application_name !== appName )
        {
          appObj = {
            name: a.application_name,
            url: a.base_url
          }
          apps.push( appObj );
          appName = a.application_name;
        }
      }
    }
    return apps;
  }

  makeFileObject( file )
  {
    let fileObj = {
      file_url: file.url,
      file_path: file.path,
      file_name: file.name,
      file_type: this.getFileTypeFromFilename( file.name ),
      file_id: file.id,
      file_thumb_path: file.thumbnail_path,
      file_thumb_url: file.thumbnail_url
    };
    return fileObj;
  }

  getFileTypeFromFilename( fileName )
  {
    let fileExtension = fileName.split('.').pop().toLowerCase();
    let fileType = '';
    switch ( fileExtension ) {
      case 'doc':
      case 'docx':
        fileType = 'word';
        break;
      case 'pdf':
        fileType = 'pdf';
        break;
      case 'xls':
      case 'xlsx':
      case 'csv':
        fileType = 'excel';
        break;
      case 'png':
      case 'jpg':
      case 'jpeg':
        fileType = 'image';
        break;
      default:
        fileType = 'image';
    }
    return fileType;
  }

  //
  // getOptionColor is a method to return
  // consistend status and payment type colors 
  // throughout the app.
  //
  getOptionColor( option )
  {
    let color:string = '';
    switch( option )
    {
      case 'Registered':   
        color = 'medium-blue';       
        break;
      case 'Paid':
        color = 'success'; 
        break;
      case 'Cancelled':
        color = 'warning'; 
        break; 
      case 'Credit Card':
        color = 'success'; 
        break; 
      case 'Check':
        color = 'light-gray'; 
        break;
      case 'Invoice':
        color = 'light-gray'; 
        break;
      case 'None':
        color = 'warning'; 
        break;
      case 'Active':
        color = 'medium-blue'; 
        break;
      case 'In Progress':
        color = 'medium-blue'; 
        break;
      case 'Archived':
        color = 'light'; 
        break;
      case 'Completed':
        color = 'success'; 
        break;
      case 'Cancelled':
        color = 'danger'; 
        break;
      default:
        color = 'medium-blue';      
    }

    return color;
  }

  stripHtml( html )
  {
    if ( html === '' || typeof html === 'undefined' )
    {
      return null;
    }
    var doc = new DOMParser().parseFromString(html, 'text/html');
    return doc.body.textContent || "";
  }

  getProjectObjFromProjectId( projectId )
  {
    for ( let p of Config.TRI_PROJECTS )
    {
      if ( p.tri_project_id === projectId )
      {
        return p;
      }
    }
  }

  // Convert an enum value to boolean for toggle controls.
  // Convert toggle control boolean to enum value.
  booleanEnumToggle( value, trueValue, falseValue )
  {
    var newValue;
    if ( typeof value !== 'boolean' )
    {
      // We're converting from enum to toggle bool.
      newValue = false;
      if ( value === trueValue )
      {
        newValue = true;
      }
    } else {
      // We're converting from toggle bool to enum value.
      newValue = falseValue;
      if ( value )
      {
        newValue = trueValue;
      }
    }

    return newValue;
  }

  jsonToQueryString( json ) {
    return '?' + 
      Object.keys(json).map(function(key) {
        return encodeURIComponent(key) + '=' +
            encodeURIComponent(json[key]);
      }).join('&');
  }
}  