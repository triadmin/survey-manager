import { Injectable } from '@angular/core';

//import { ImageService } from '../../providers/image-service/image-service';

@Injectable()
export class HelperPDFService {

  constructor(
    //private imageService: ImageService
  ) {

  }

  makePDFHeader( data )
  {
    var header = [
      {
        columns: [
          {
            width: 150,
            stack: [
              /*
              {
                image: data.logoImage,
                width: 125,
                height: 53
              }
              */
          ]
          },
          {
            width: 'auto',
            text: [
              { text: data.reportTitle + '\n', style: 'header' },
              { text: data.reportSubTitle + '\n\n\n', style: 'subheader' }
            ]
          }
        ],
        columnGap: 10
      }
    ];

    return header;
  }

  makeFrequencyTables( data )
  {
    var frequencyTables = [
      {
        columns: [
          {
            width: '33%',
            table: {
              headerRows: 1,
              widths: [ 'auto', 'auto' ],
              body: data.dataActivityTable
            },
            layout: 'lightHorizontalLines'
          },
          {
            width: '33%',
            table: {
              headerRows: 1,
              widths: [ 'auto', 'auto' ],
              body: data.dataDeliverablesTable
            },
            layout: 'lightHorizontalLines'
          },
          {
            width: '33%',
            table: {
              headerRows: 1,
              widths: [ 'auto', 'auto' ],
              body: data.dataAudienceTable
            },
            layout: 'lightHorizontalLines'
          }
        ]
      }
    ];

    return frequencyTables;
  }

  makeAreaTables( data )
  {
    var areaTables = [
      {
        columns: [
          {
            width: '33%',
            table: {
              headerRows: 1,
              widths: [ 'auto', 'auto' ],
              body: data.dataAreaCountyTable
            },
            layout: 'lightHorizontalLines'
          },
          {
            width: '33%',
            table: {
              headerRows: 1,
              widths: [ 'auto', 'auto' ],
              body: data.dataAreaDistrictTable
            },
            layout: 'lightHorizontalLines'
          }
        ]
      }
    ];

    return areaTables;
  }

  makeActivityEntryTable( data )
  {
    // Date | Title | Time | TA Mode | Location(s)
    console.log(data);
    var activityEntryTable = [
      {
        table: {
          headerRows: 1,
          widths: [ '20%', '28%', '12%', '12%', '28%' ],
          body: data
        },
        layout: 'lightHorizontalLines'
      }
    ];

    return activityEntryTable;
  }

  makeChart( data )
  {
    var chart = [
      {
        image: data,
        width: 500,
        alignment: 'center',
        margin: [0,10]
      }
    ];

    return chart;
  }

  // PDF Styles here
  getPDFStyles()
  {
    let styles = {
      header: {
        fontSize: 18
      },
      subheader: {
        fontSize: 12,
        bold: false,
        color: '#666'
      },
      normal: {
        fontSize: 10,
        bold: false
      },
      tableHeader: {
        bold: true,
        fontSize: 13,
        color: '#666'
      }
    };

    return styles;
  }

  getPDFDefaultStyles()
  {
    let defaultStyles = {
      columnGap: 20,
      lineHeight: 1.1,
      fontSize: 11
    };

    return defaultStyles;
  }
}
