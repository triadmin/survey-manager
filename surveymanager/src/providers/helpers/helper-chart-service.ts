import { Injectable } from '@angular/core';

import { SurveyData } from '../../providers/surveys/survey-client-data';
import moment from 'moment';

@Injectable()
export class HelpersChartService {
  surveyData:any = SurveyData;

  constructor() {}

  // getCompareLabels returns 1 label for charts
  // based on whether or not we have comparison data.
  // If no comparison data, return label for original data set.
  getCompareLabels( labelCompare = false )
  {
    let label = '';
    let filter:any = this.surveyData.filter;
    let locationString = '';

    if ( labelCompare )
    {
      filter = this.surveyData.compares; 
    }
    
    if ( filter.dateStart !== '' && filter.dateEnd !== '' )
    {
      label += moment( filter.dateStart ).format('MMM D, YYYY') + ' - ' + moment( filter.dateEnd ).format('MMM D, YYYY')
    }
    if ( filter.location.length > 0 )
    {
      locationString = filter.location.join(', ');
      if ( filter.location.length > 3 )
      {
        locationString = filter.location[0] + '...' + filter.location[filter.location.length - 1];
      }

      if ( label !== '' && locationString !== '' )
      {
        label += ' : ' + locationString;
      } else {
        label += locationString;
      }
    }
    return label;
  }
}