import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

@Injectable()
export class HelperCsvService {
  constructor(
    public events: Events
  ) {}

  exportSurveyAttempts( survey, surveyAttempts, fileName )
  {
    // Make headers from question identifier.
    let data = [];
    let header = [];
    let relevantQuestionIds = [];
    for ( let page of survey.questions )
    {
      for ( let q of page.questions )
      {
        if ( q.question_identifier !== '' )
        {
          header.push( q.question_identifier );
          relevantQuestionIds.push( q.survey_question_id );
        }
      }
    }

    data.push( header );

    // Now make our data rows.
    for ( let sa of surveyAttempts )
    {
      var tempSurveyAttemptData = [];
      if ( sa.responses.length > 0 )
      {
        // Loop over all of our question id's and see if we have a response.
        for ( let qid of relevantQuestionIds )
        {
          var responseString = '';
          var responseArray = [];

          // Loop over all of our responses and make a temp array.
          // We'll concat responses with same question id's.
          for ( let r of sa.responses )
          {            
            if ( r.survey_question_id === qid )
            {
              if ( this.hasValue( r.text ) && this.hasValue( r.value ) )
              {
                responseArray.push( r.text + ' ' + r.value );
              } else if ( this.hasValue( r.value ) ) {
                responseArray.push( r.value );
              } else if ( this.hasValue( r.text ) ) {
                responseArray.push( r.text );
              }
            }
          }
          tempSurveyAttemptData.push( responseArray.join(', ') );
        }
      }
      if ( tempSurveyAttemptData.length > 0 )
      {
        data.push( tempSurveyAttemptData );
      }
    }
    
    this.exportCsvFile( data, fileName );
  }

  hasValue( v )
  {
    if ( v !== null && v !== '' && v !== 'undefined' && typeof v !== 'undefined')
    {
      return true;
    }
    return false;
  }

  exportCsvFile( data, filename )
  {
    var processRow = function (row) {
      var finalVal = '';
      for (var j = 0; j < row.length; j++) {
          var innerValue = row[j] === null ? '' : row[j].toString();
          if (row[j] instanceof Date) {
              innerValue = row[j].toLocaleString();
          };
          var result = innerValue.replace(/"/g, '""');
          if (result.search(/("|,|\n)/g) >= 0)
              result = '"' + result + '"';
          if (j > 0)
              finalVal += ',';
          finalVal += result;
      }
      return finalVal + '\n';
    };

    var csvFile = '';
    for (var i = 0; i < data.length; i++) {
      csvFile += processRow(data[i]);
    }

    var blob = new Blob([csvFile], { type: 'text/csv;charset=utf-8;' });
    if (navigator.msSaveBlob) { // IE 10+
      navigator.msSaveBlob(blob, filename);
    } else {
      var link = document.createElement("a");
      if (link.download !== undefined) { // feature detection
          // Browsers that support HTML5 download attribute
          var url = URL.createObjectURL(blob);
          link.setAttribute("href", url);
          link.setAttribute("download", filename);
          link.style.visibility = 'hidden';
          document.body.appendChild(link);
          link.click();
          document.body.removeChild(link);
      }
    }
  }
}