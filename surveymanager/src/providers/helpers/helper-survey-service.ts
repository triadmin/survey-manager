import { Injectable } from '@angular/core';
import { Events } from 'ionic-angular';

import { QuestionTypeDefinitionService } from '../../providers/surveys/question-type-definition-service';
import { ReportElementTypeDefinitionService } from '../../providers/surveys/report-element-type-definition-service';

import { SurveyData } from '../../providers/surveys/survey-client-data';

@Injectable()
export class HelperSurveyService {
  questionTypeDefinitions = QuestionTypeDefinitionService;
  reportElementTypeDefinitions = ReportElementTypeDefinitionService;

  constructor(
    public events: Events
  ) {}

  // Since our survey questions are in a different
  // JSON object than our responses coming from the
  // server, we need to map the response objects to 
  // their question objects.

  // This method is a problem because it dirties our survey object
  // with responses and it shold remain clean.  A survey should not be
  // associated with responses, only a surveyAttempt should contain
  // responses.
  matchSurveyQuestionsWithResponses( survey, surveyAttempt )
  {
    for ( let question of survey.questions )
    {
      question.responses = [];
      for ( let response of surveyAttempt.responses )
      {
        if ( response.survey_question_id === question.survey_question_id )
        {
          question.responses.push( response );
        }
      }
    }

    return survey;
  }

  formatQuestionPages( questions )
  {
    let initPage = 0;
    let questionPages = [];
    let count = 0;
    for ( let q of questions )
    {
      if ( q.page !== initPage )
      {
        if ( initPage > 0 )
        {
          count++;
        }
        initPage = q.page;
        questionPages.push( { page: initPage, questions: [] } );
      }
      questionPages[count].questions.push( q );
    }

    return questionPages;
  }

  getQuestionFromQuestionIdentifier( questions, questionIdentifier )
  {
    for ( let q of questions )
    {
      if ( q.question_identifier === questionIdentifier )
      {
        return q;
      }
    }
    
    return false;
  }

  attachQuestionTypeDefinitionsToQuestions( questions )
  {
    for ( let q of questions )
    {
      for ( let qt of this.questionTypeDefinitions )
      {
        if ( q.question_type === qt.id )
        {
          q.questionTypeDefinition = qt;
        }
      }
    }

    return questions;
  }

  attachReportElementTypeDefinitionsToReportElements( report_elements )
  {
    for ( let re of report_elements )
    {
      for ( let ret of this.reportElementTypeDefinitions )
      {
        if ( re.chart_type === ret.id )
        {
          re.reportElementTypeDefinition = ret;
        }
      }
    }

    return report_elements;
  }

  clearSurveyData()
  {
    SurveyData.activities.length = 0;
    SurveyData.filter = {};
    SurveyData.survey = {};
  }
}