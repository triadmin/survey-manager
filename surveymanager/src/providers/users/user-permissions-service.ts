import { Injectable } from '@angular/core';

// Config 
import { Config } from '../config/config';

@Injectable()
export class UserPermissionsService {

  constructor() {}

  userHasToken() 
  {
    let access_token = localStorage.getItem('access_token');
    
    if ( access_token !== null )
    {
      // TODO: Check token in localStorage against server.  It may have
      // been revoked at some point.
      let token_is_valid = true;
      if ( token_is_valid === true )
      {
        return true;
      }
    }

    return false;
  }

  userHasRole( roles )
  {
    let user:any = Config.USER;

    for ( let r of roles )
    {
      for ( let ugt of user.gate_tag_application )
      {
        if ( r === ugt.gate_tag_name )
        {
          return true;
        }
      }
    }

    return false;
  }

  filterRegionSetBasedOnUserPermissions( regionSet )
  {
    let availableRegions = [];
    let userRegions = Config.USER_APP_RECORD.regions;

    if ( !this.userHasRole( ['survey-manager-admin','survey-manager-project-level-admin'] ) )
    {
      for ( let ur of userRegions )
      {
        for ( let child of regionSet.children )
        {
          if ( ur.region_list_item_id === child.list_item.region_list_item_id )
          {
            availableRegions.push( child );
          }
        }
      }
    } else {
      availableRegions = regionSet.children;
    }

    return availableRegions;
  }
}