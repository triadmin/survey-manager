import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';
import { HelperService } from '../helpers/helper-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class UsersService {
  data: any;

  constructor( 
    public http: HttpClient,
    public helperService: HelperService
  ) {
  }

  saveRootUserRecord( user )
  {
    user.client_id = Config.APP_CLIENT_ID;
    user.grant_type = 'password';
    user.base_gate = Config.APP_BASE_GATE;

    if ( user.user_id > 0 )
    {
      // Edit user
      return this.http.put( Config.API_ROOT_CORE + 'users/' + user.user_id, JSON.stringify(user), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_CORE + 'register', JSON.stringify(user), httpOptions );
    } 
  }

  saveAppUserRecord( userAppRecord )
  {
    if ( userAppRecord.survey_user_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'users/' + userAppRecord.survey_user_id, JSON.stringify(userAppRecord), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'users', JSON.stringify(userAppRecord), httpOptions );
    }
  }

  updateUserPassword( user )
  {
    return this.http.put( Config.API_ROOT_CORE + 'user-password/' + user.user_id, JSON.stringify(user), httpOptions );
  }

  saveUserGateTags( gateTags, userId )
  {
    return this.http.post( Config.API_ROOT_CORE + 'gate-tags/' + userId, JSON.stringify(gateTags), httpOptions );
  }

  saveUserProjects( projects, userId )
  {
    return this.http.post( Config.API_ROOT_CORE + 'user-projects/' + userId, JSON.stringify(projects), httpOptions );
  }

  getUsers()
  {
    return this.http.get( Config.API_ROOT_CORE + 'users?app_base_gate=' + Config.APP_BASE_GATE )
      .pipe( map( this.processData, this ));
  }

  removeUserFromApp( userId )
  {
    let user:any = {};
    user.user_id = userId;
    user.app_base_gate = Config.APP_BASE_GATE;

    return this.http.post( Config.API_ROOT_CORE + 'remove-user-from-app', JSON.stringify(user), httpOptions );
  }

  getUserAppRecord( userId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'users/' + userId )
      .pipe( map( this.processData, this ));
  }

  getUsersForSurvey( surveyId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/users'  )
      .pipe( map( this.processData, this ));
  }

  setUserInfo( data:any = null )
  {
    // If we're passing in a data object, then that means we've logged in.
    // Otherwise, you just get the user object from local storage.
    var access_token, user;
    if ( data === null )
    {
      access_token = localStorage.getItem('access_token');
      user = JSON.parse( localStorage.getItem('user') );
    } else {
      access_token = data.access_token;
      user = data.user;

      // Set local storage data
      localStorage.setItem('access_token', access_token); 
      localStorage.setItem('user', JSON.stringify( user ));
    }
    Config.ACCESS_TOKEN = access_token;
    Config.USER = user;
    Config.USER_APP_RECORD = user.user_app_record;
    let fullName = user.first_name + ' ' + user.last_name;
    let initials = fullName.match(/\b\w/g) || [];
    Config.USER.userInitials = ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
    Config.USER_APPS = this.helperService.getAvailableApps( user );
  }

  processData(data: any) 
  {
    this.data = data;

    return this.data;
  } 
}  