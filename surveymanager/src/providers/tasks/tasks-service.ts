import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Services
import { HelperService } from '../helpers/helper-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class TasksService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService
  ) {
  }

  getTasksForUser( userId, status = null )
  {
    let statusQuery = '';
    if ( status !== null )
    {
      statusQuery = '?status=' + status;
    }
    return this.http.get( Config.API_ROOT_SURVEYS + 'users/' + userId + '/tasks' + statusQuery )
      .pipe( map( this.processData, this ));
  }

  getTasksForUserForSurvey( userId, surveyId, status = null )
  {
    let statusQuery = '';
    if ( status !== null )
    {
      statusQuery = '?status=' + status;
    }
    return this.http.get( Config.API_ROOT_SURVEYS + 'users/' + userId + '/tasks/' + surveyId + statusQuery )
      .pipe( map( this.processData, this ));
  }

  getTasksForSurveyAttempt( surveyId, surveyAttemptId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts/' + surveyAttemptId + '/tasks' )
      .pipe( map( this.processData, this ));
  }

  saveTask( task )
  {    
    if ( task.task_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'tasks/' + task.task_id, JSON.stringify(task), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'tasks', JSON.stringify(task), httpOptions );
    } 
  }

  deleteTask( task )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'tasks/' + task.task_id, httpOptions );
  }

  processData(data: any) 
  {
    this.data = data;
    return this.data;
  } 
}  