import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { Config } from '../config/config';

export class TokenInterceptor implements HttpInterceptor 
{
  //
  // Construct
  //
  constructor() {}
  
  //
  // Run intercept
  //
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    //let access_token = localStorage.getItem('access_token');
    let access_token = Config.ACCESS_TOKEN;

    // Add in the headers for the API.
    request = request.clone({ setHeaders: { Authorization: `Bearer ${access_token}` } });

    // Let the request continue.
    return next.handle(request);
  }
}