export let ReportElementTypeDefinitionService = [
  {
    'id': 'pie',
    'name': 'Pie Chart',
    'icon': 'svgicon-rt-pie'
  },
  {
    'id': 'bar-vertical',
    'name': 'Bar Chart Vertical',
    'icon': 'svgicon-rt-bar-vertical'
  },
  {
    'id': 'bar-horizontal',
    'name': 'Bar Chart Horizontal',
    'icon': 'svgicon-rt-bar-horizontal'
  },
  {
    'id': 'donut',
    'name': 'Donut Chart',
    'icon': 'svgicon-rt-donut'
  },
  {
    'id': 'table',
    'name': 'Data Table',
    'icon': 'svgicon-rt-data-table'
  },
  {
    'id': 'trend',
    'name': 'Trend Chart',
    'icon': 'svgicon-rt-trend'
  },
  {
    'id': 'total_ave',
    'name': 'Total & Average',
    'icon': 'svgicon-rt-total-ave'
  },
  {
    'id': 'gauge',
    'name': 'Gauge',
    'icon': 'svgicon-rt-gauge'
  }
]