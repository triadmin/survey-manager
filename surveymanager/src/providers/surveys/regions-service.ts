import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Providers
import { HelperService } from '../helpers/helper-service';
import { diPublicInInjector } from '@angular/core/src/render3/di';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class RegionsService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService
  ) {
  }

  getRegions()
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'regions' )
      .pipe( map( this.processData, this ));
  }

  getRegion( regionSetId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'regions/' + regionSetId )
      .pipe( map( this.processData, this ));
  }

  saveRegion( region )
  {
    if ( region.region_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'regions/' + region.region_id, JSON.stringify(region), httpOptions )
        .pipe( map( this.processData, this ));
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'regions', JSON.stringify(region), httpOptions )
        .pipe( map( this.processData, this ));
    } 
  }

  deleteRegion( regionId )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'regions/' + regionId, httpOptions );
  }

  deleteRegionListItem( regionListItem )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'regions/' + regionListItem.region_id + '/list-items/' + regionListItem.region_list_item_id, httpOptions );
  }

  saveRegionListItem( rli )
  {
    if ( rli.region_list_item_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'regions/' + rli.region_id + '/list-items/' + rli.region_list_item_id, JSON.stringify(rli), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'regions/' + rli.region_id + '/list-items', JSON.stringify(rli), httpOptions );
    }
  }

  processData(data: any) 
  {
    this.data = data;

    if ( typeof this.data.length === 'undefined' )
    {
      this.data = this.formatRegionListItems( this.data );
    } else {
      this.data.forEach((item) => {
        this.formatRegionListItems( item );
      });
    }

    return this.data;
  }

  // This method takes all the list items for
  // a region and formats them into a tree.
  formatRegionListItems( region )
  {
    let item_level = 0;
    let region_items = region.region_items;
    var roots = [], children = {};

    if ( region_items !== null )
    {
      // find the top level nodes and hash the children based on parent
      for (var i = 0; i < region_items.length; ++i) {
        var item = region_items[i],
          p = item.parent_id,
          target = !p ? roots : (children[p] || (children[p] = []));
        item.level = item_level;
        target.push({ list_item: item });
      }

      // function to recursively build the tree
      var findChildren = function( parent ) {
        if ( children[parent.list_item.region_list_item_id] ) {
          parent.children = children[parent.list_item.region_list_item_id];
          parent.open = true;
          for ( var i = 0; i < parent.children.length; ++i ) {
            item_level++;
            findChildren( parent.children[i] );
          }
        }
      };

      // enumerate through to handle the case where there are multiple roots
      for (var j = 0; j < roots.length; ++j) {
        findChildren(roots[j]);
      }
      region.children = roots;
    }
    return region;
  }
}