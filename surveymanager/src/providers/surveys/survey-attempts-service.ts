import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Providers
import { HelperService } from '../helpers/helper-service';
import { SurveyData } from '../surveys/survey-client-data';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class SurveyAttemptsService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService
  ) {
  }

  getSurveyAttempts( surveyId, compare = false )
  {
    var queryString;
    if ( compare )
    {
      queryString = this.helperService.jsonToQueryString( SurveyData.compares );
    } else {
      queryString = this.helperService.jsonToQueryString( SurveyData.filter );
    }
  
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts' + queryString )
      .pipe( map( this.processData, this ));
  }

  searchSurveyAttempts( surveyId, filter )
  {
    let queryString = this.helperService.jsonToQueryString( filter );
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts' + queryString )
      .pipe( map( this.processData, this )); 
  }

  getSurveyAttemptCount( surveyId, filter )
  {
    let queryString = this.helperService.jsonToQueryString( filter );
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempt-count' + queryString )
      .pipe( map( this.processData, this )); 
  }

  getSurveyAttempt( surveyId, surveyAttemptId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts/' + surveyAttemptId )
      .pipe( map( this.processData, this ));
  }

  getSurveyAttemptsForFollowUp( surveyId )
  {
    var queryString = this.helperService.jsonToQueryString( SurveyData.filter );
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts-for-follow-up' + queryString )
      .pipe( map( this.processData, this ));
  }

  saveSurveyAttempt( attempt )
  {if ( attempt.survey_attempt_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + attempt.survey_id + '/attempts/' + attempt.survey_attempt_id, JSON.stringify(attempt), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + attempt.survey_id + '/attempts', JSON.stringify(attempt), httpOptions )
      ;
    }
  }

  deleteSurveyAttempt( attempt )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + attempt.survey_id + '/attempts/' + attempt.survey_attempt_id, httpOptions );
  }

  updateSurveyAttemptsCollection( surveyAttempts, surveyId )
  {
    return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts-collection', JSON.stringify(surveyAttempts), httpOptions );
  }

  processData(data: any) 
  {
    this.data = data;

    return this.data;
  }
}