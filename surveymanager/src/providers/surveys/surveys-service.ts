import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Services
import { HelperService } from '../helpers/helper-service';
import { HelperSurveyService } from '../helpers/helper-survey-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class SurveysService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  getSurveys()
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys' )
      .pipe( map( this.processData, this ));
  }

  getSurvey( survey_id )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + survey_id )
      .pipe( map( this.processData, this ));
  }

  saveSurvey( survey )
  {
    if ( survey.survey_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + survey.survey_id, JSON.stringify(survey), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'surveys', JSON.stringify(survey), httpOptions );
    } 
  }

  getActiveSurveys()
  {
    let dateToday = this.helperService.getDateToday();
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys?end_date=' + dateToday )
      .pipe( map( this.processData, this ));
  }

  searchSurveys( search_term )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys?search=' + search_term )
      .pipe( map( this.processData, this ));
  }

  deleteSurvey( survey )
  {
    // We aren't actually deleting the survey data.
    // We are just updating the status to DELETED.
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + survey.survey_id, httpOptions );
  }

  processData(data: any) 
  {
    let mappedData;
    if ( data.length === undefined )
    {
      let survey = this.setStatusColor( data );
      survey = this.formatQuestionPages( survey );
      survey = this.setProjectObj( survey );
      mappedData = survey;
    } else {
      mappedData = data.map(( survey ) => { 
        survey = this.setStatusColor( survey );
        survey = this.formatQuestionPages( survey );
        survey = this.setProjectObj( survey );
        return survey;
      });
    }
    return mappedData;
  } 

  setProjectObj( item:any )
  {
    let tri_project = {};
    if ( item.tri_project_id > 0 )
    {
      tri_project = this.helperService.getProjectObjFromProjectId( item.tri_project_id );
    }
    item.tri_project = tri_project;
    return item;
  }

  setStatusColor( item:any )
  {
    let status = item.status;
    item.StatusColor = this.helperService.getOptionColor( status );
    return item;
  }

  formatQuestionPages( item:any )
  {
    item.question_pages = this.helperSurveyService.formatQuestionPages( item.questions );
    return item;
  }
}  