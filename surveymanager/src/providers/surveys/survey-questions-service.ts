import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Services
import { HelperService } from '../helpers/helper-service';
import { HelperSurveyService } from '../helpers/helper-survey-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class SurveyQuestionsService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveQuestion( question )
  {
    if ( question.survey_question_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id, JSON.stringify(question), httpOptions );
    } else {
      // Create semi-random question_identifier
      question.question_identifier = this.helperService.makeQuestionIdentifier( question ); 
      return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions', JSON.stringify(question), httpOptions );
    } 
  }

  saveQuestionAnswers( question )
  {
    return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id + '/answers', JSON.stringify(question.answers), httpOptions );
  }

  deleteAnswer( question, answer )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id + '/answers/' + answer.survey_answer_id );
  }

  getQuestions( surveyId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/questions' )
      .pipe( map( this.processData, this ));
  }

  deleteQuestion( question )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id )
  }

  moveQuestionsToPage( pageFrom, pageTo, surveyId )
  {
    return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/move-questions/' + pageFrom + '/' + pageTo, '', httpOptions );
  }

  deleteAllQuestionsOnPage( page, surveyId )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/all-questions/' + page )
  }

  saveQuestionOrder( questions, surveyId )
  {
    return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/questions-order', JSON.stringify(questions), httpOptions );
  }

  saveAnswerOrder( question )
  {
    return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id + '/answers-order', JSON.stringify(question.answers), httpOptions );
  }

  processData(data: any) 
  {
    this.data = data;

    // Format question pages.
    let newData = [];
    newData = this.helperSurveyService.formatQuestionPages( this.data );
    
    return newData;
  } 
}