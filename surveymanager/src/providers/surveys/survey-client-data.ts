export let SurveyData = {
  survey: <any>{},
  activities: [],
  compareActivities: [],
  filter: <any>{},
  compares: <any>{},
  localDataFilter: <any>[],
  goals: <any>[]
};