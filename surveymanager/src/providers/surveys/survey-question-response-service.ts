import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Providers
import { HelperService } from '../helpers/helper-service';
import { SurveyData } from './survey-client-data';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class SurveyQuestionResponseService {
  data: any;
  surveyData = SurveyData;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService
  ) {
  }

  saveResponse( question, response, qtype )
  {
    console.log(question);
    console.log(response);
    if ( response.survey_response_id > 0 )
    {
      // Then update the response
      return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id + '/responses/' + response.survey_response_id, JSON.stringify(response), httpOptions );
    } else {
      // Save new response.
      return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id + '/responses', JSON.stringify(response), httpOptions );
    }
  }

  deleteResponse( question, response )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + question.survey_id + '/questions/' + question.survey_question_id + '/responses/' + response.survey_response_id );
  }
}