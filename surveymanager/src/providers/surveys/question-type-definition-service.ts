export let QuestionTypeDefinitionService = [
  {
    'id': 'survey-attempt',
    'name': 'Survey Attempt',
    'icon': 'clipboard',
    'reportable': 'No'
  },
  {
    'id': 'textbox',
    'name': '1 Line Textbox',
    'icon': 'svgicon-qt-textbox',
    'reportable': 'No'
  },
  {
    'id': 'textarea',
    'name': 'Multi-Line Textarea',
    'icon': 'svgicon-qt-textarea',
    'reportable': 'No'
  },
  {
    'id': 'select',
    'name': 'Select List, Single',
    'icon': 'svgicon-qt-select-single',
    'reportable': 'Yes'
  },
  {
    'id': 'select-multi',
    'name': 'Select List, Multiple',
    'icon': 'svgicon-qt-select-multiple',
    'reportable': 'Yes'
  },
  {
    'id': 'checkboxes',
    'name': 'Checkboxes',
    'icon': 'checkbox',
    'reportable': 'Yes'
  },
  {
    'id': 'radio',
    'name': 'Radio Buttons',
    'icon': 'radio-button-on',
    'reportable': 'Yes'
  },
  {
    'id': 'list-with-headers-text',
    'name': 'Textbox List with Headers',
    'icon': 'svgicon-qt-textbox-list-with-headers',
    'reportable': 'Yes'
  },
  {
    'id': 'list-with-headers-checkbox',
    'name': 'Checkbox List with Headers',
    'icon': 'svgicon-qt-checkbox-list-with-headers',
    'reportable': 'Yes'
  },
  {
    'id': 'datetime',
    'name': 'Date and Time',
    'icon': 'svgicon-qt-date-time',
    'reportable': 'No'
  },
  {
    'id': 'rating-scale',
    'name': 'Point Rating Scale',
    'icon': 'star',
    'reportable': 'Yes'
  },
  {
    'id': 'session-list',
    'name': 'Conference Session List',
    'icon': 'remove',
    'reportable': 'No'
  },
  {
    'id': 'heading',
    'name': 'Heading',
    'icon': 'svgicon-qt-heading',
    'reportable': 'No'
  },
  {
    'id': 'spacer',
    'name': 'Spacer',
    'icon': 'svgicon-qt-spacer',
    'reportable': 'No'
  },
  {
    'id': 'content',
    'name': 'Content Block',
    'icon': 'svgicon-qt-content-block',
    'reportable': 'No'
  },
  {
    'id': 'region-set',
    'name': 'Region Set',
    'icon': 'pin',
    'reportable': 'Yes'
  },
  {
    'id': 'upload',
    'name': 'Photo or Document Upload',
    'icon': 'svgicon-qt-upload',
    'reportable': 'No'
  },
  {
    'id': 'cross-reference',
    'name': 'Cross Reference Another Survey',
    'icon': 'svgicon-qt-cross-reference',
    'reportable': 'Yes'
  },
  {
    'id': 'toggle',
    'name': 'Toggle Button',
    'icon': 'svgicon-qt-toggle',
    'reportable': 'Yes'
  },
  {
    'id': 'slider',
    'name': 'Slider Control',
    'icon': 'svgicon-qt-slider',
    'reportable': 'Yes'
  }
];