import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Services
import { HelperService } from '../helpers/helper-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class SurveyReportElementService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService
  ) {
  }

  getReportElements( surveyId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/report-elements' )
      .pipe( map( this.processData, this ));
  }

  saveReportElement( reportElement )
  {
    if ( reportElement.survey_report_element_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'surveys/' + reportElement.survey_id + '/report-elements/' + reportElement.survey_report_element_id, JSON.stringify(reportElement), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + reportElement.survey_id + '/report-elements', JSON.stringify(reportElement), httpOptions );
    } 
  }

  deleteReportElement( reportElement )
  {
    // We aren't actually deleting the survey data.
    // We are just updating the status to DELETED.
    return this.http.delete( Config.API_ROOT_SURVEYS + 'surveys/' + reportElement.survey_id + '/report-elements/' + reportElement.survey_report_element_id, httpOptions );
  }

  saveReportElementOrder( reportElements, surveyId )
  {
    return this.http.post( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/report-elements-order', JSON.stringify(reportElements), httpOptions );
  }

  processData(data: any) 
  {
    this.data = data;
    
    return this.data;
  } 
}  