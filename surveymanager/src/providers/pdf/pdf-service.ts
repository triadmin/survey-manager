import { Injectable, TestabilityRegistry } from '@angular/core';
import { Platform } from 'ionic-angular';

// Services
import { HelperPDFService } from '../helpers/helper-pdf-service';

// Ionic Native
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

// Config
import { Config } from '../config/config';

// PDF Make
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';

@Injectable()
export class PdfService {
  dd: any;

  constructor( 
    private helperPDFService: HelperPDFService,
    private platform: Platform,
    private file: File,
    private fileOpener: FileOpener
  ) {}

  createReportPDFDocDefinition( report )
  {
    var docContent = [];

    for ( let rc of report.reportConfig )
    { 
      docContent.push( { text: rc.label, style: 'header', alignment: 'center', margin: [0,5] } );
      switch ( rc.chart_type )
      {
        case 'donut':
          for ( let d of report.charts.donuts )
          {
            if ( d.reportElementConfig.survey_question_id === rc.survey_question_id )
            {
              docContent.push( this.helperPDFService.makeChart( d.getBase64Image() ) );
            }
          }
          break;
        case 'pie':
          for ( let d of report.charts.pies )
          {
            if ( d.reportElementConfig.survey_question_id === rc.survey_question_id )
            {
              docContent.push( this.helperPDFService.makeChart( d.getBase64Image() ) );
            }
          }
          break;
        case 'bar-horizontal':
          for ( let d of report.charts.barsHorizontal )
          {
            if ( d.reportElementConfig.survey_question_id === rc.survey_question_id )
            {
              docContent.push( this.helperPDFService.makeChart( d.getBase64Image() ) );
            }
          }
          break;
        case 'gauge':
          for ( let d of report.charts.gauges )
          {
            if ( d.reportElementConfig.survey_question_id === rc.survey_question_id )
            {
              docContent.push( this.helperPDFService.makeChart( d.getBase64Image() ) );
            }
          }
          break;
        case 'table':
          for ( let d of report.charts.tables )
          {
            if ( d.reportElementConfig.survey_question_id === rc.survey_question_id )
            {
              var table = this.makeTable( d.chartData );
              var tableDef = {
                columns:[
                  { width: '30%', text: '' },
                  {
                    width: 'auto',
                    table: table,
                    layout: 'lightHorizontalLines'
                  }
                ]                
              };
              docContent.push( tableDef );
            }
          }
          break;
        case 'trend':
          for ( let d of report.charts.trends )
          {
            if ( d.reportElementConfig.survey_question_id === rc.survey_question_id )
            {
              docContent.push( this.helperPDFService.makeChart( d.getBase64Image() ) );
            }
          }
          break;
      }
    }

    this.dd = {
      content: docContent,
      styles: this.helperPDFService.getPDFStyles(),
      defaultStyle: this.helperPDFService.getPDFDefaultStyles()
    };

    this.generatePDFFile( report.survey.title + '.pdf' );
  }

  createActivityPDFDocDefinition( activityForPrint )
  {
    let sa = activityForPrint.surveyAttempt;
    let survey = activityForPrint.survey;

    // See if we have a survey logo file.
    let logoFile = '';
    for ( let file of survey.files )
    {
      if ( file.file_purpose === 'survey_logo' )
      {
        logoFile = file.file_thumb_url;
      }
    }
  
    var docContent = [];
    let headerData = {
      logoImage: logoFile,
      reportTitle: sa.title,
      reportSubTitle: 'By ' + sa.survey_user.first_name + ' ' + sa.survey_user.last_name + ' on ' + sa.date
    };

    docContent.push( this.helperPDFService.makePDFHeader( headerData ) );

    for ( let qc of activityForPrint.questionComponents )
    { 
      let data = qc.getDataForPdf();

      docContent.push( { text: data.label, bold: true, margin: [0,5] } );
      for ( let r of data.responses )
      {
        docContent.push( { text: r } );
      }
    }

    this.dd = {
      content: docContent,
      styles: this.helperPDFService.getPDFStyles(),
      defaultStyle: this.helperPDFService.getPDFDefaultStyles()
    };

    this.generatePDFFile( activityForPrint.surveyAttempt.title + '.pdf' );
  }

  generatePDFFile( fileName )
  {
    if ( this.platform.is('cordova') )
    {
      pdfMake.createPdf( this.dd ).getBuffer((buffer) => {
        var utf8 = new Uint8Array(buffer); // Convert to UTF-8...
        let binaryArray = utf8.buffer; // Convert to Binary...

        let saveDir = this.file.dataDirectory;

        this.file.createFile(saveDir, fileName, true).then((fileEntry) => {
          fileEntry.createWriter((fileWriter) => {
            fileWriter.onwriteend = () => {
              // Get rid of all the toast crap and just
              // display the PDF.
              this.fileOpener.open( saveDir+fileName, 'application/pdf');
            };

            fileWriter.onerror = (e) => {
              console.log('file writer - error event fired: ' + e.toString());
            };

            fileWriter.write(binaryArray);

           });
         });
      });
    } else {
      pdfMake.createPdf( this.dd ).download( fileName );
    }
  }

  makeTable( data )
  {
    let table = {
      headerRows: 1,
      body: []
    };

    table.body.push( ['Selection', 'Count', '% of Total'] );
    for ( let d of data )
    {
      table.body.push( [
        d.label, 
        { text: d.count, alignment: 'right' }, 
        { text: d.percentage + '%', alignment: 'right' } 
      ] );
    }
    return table;
  }
}