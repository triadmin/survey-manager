import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../config/config';

// Services
import { HelperService } from '../helpers/helper-service';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class MessagesService {
  data: any;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService
  ) {
  }

  getMessagesToUser( userId, status = null )
  {
    let statusQuery = '';
    if ( status !== null )
    {
      statusQuery = '?status=' + status;
    }
    return this.http.get( Config.API_ROOT_SURVEYS + 'users/' + userId + '/messages' + statusQuery )
      .pipe( map( this.processData, this ));
  }

  getMessagesToUserForSurvey( userId, surveyId, status = null )
  {
    let statusQuery = '';
    if ( status !== null )
    {
      statusQuery = '?status=' + status;
    }
    return this.http.get( Config.API_ROOT_SURVEYS + 'users/' + userId + '/messages/' + surveyId + statusQuery )
      .pipe( map( this.processData, this ));
  }

  getMessagesForSurveyAttempt( surveyId, surveyAttemptId )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + surveyId + '/attempts/' + surveyAttemptId + '/messages' )
      .pipe( map( this.processData, this ));
  }

  getMessage( survey_id )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'surveys/' + survey_id )
      .pipe( map( this.processData, this ));
  }

  saveMessage( message )
  {    
    if ( message.message_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'messages/' + message.message_id, JSON.stringify(message), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'messages', JSON.stringify(message), httpOptions );
    } 
  }

  deleteMessage( message )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'messages/' + message.message_id, httpOptions );
  }

  processData(data: any) 
  {
    this.data = data;
    return this.data;
  } 
}  