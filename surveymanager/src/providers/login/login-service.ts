import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

// Config
import { Config } from '../../providers/config/config';

@Injectable()
export class LoginService {
  data: any;

  constructor( 
    private http: HttpClient
  ) {
  }

  login( creds )
  {
    creds.client_id = Config.APP_CLIENT_ID;
    creds.grant_type = 'password';
    creds.base_gate = Config.APP_BASE_GATE;

    return this.http.post( Config.API_ROOT_CORE + 'oauth/token', JSON.stringify(creds), httpOptions );
  }

  logout() {

  }
}  