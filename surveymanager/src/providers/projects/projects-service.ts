import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

// Config
import { Config } from '../config/config';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class ProjectsService {
  data: any;

  constructor( 
    public http: HttpClient
  ) {}

  getTriProjects()
  {
    return this.http.get( Config.API_ROOT_CORE + 'tri-projects' );
  }

  getApp()
  {
    return this.http.get( Config.API_ROOT_CORE + 'tri-apps/' + Config.APP_CLIENT_ID  );
  }
}