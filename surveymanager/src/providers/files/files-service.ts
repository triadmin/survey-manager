import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';

// Config
import { Config } from '../../providers/config/config';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class FilesService {
  data: any;

  constructor( 
    public http: HttpClient
  ) {
  }

  getFile( file_id )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'files/' + file_id )
      .pipe( map( this.processData, this ));
  }

  getFiles( file_purpose )
  {
    return this.http.get( Config.API_ROOT_SURVEYS + 'files?file_purpose' + file_purpose)
      .pipe( map( this.processData, this ));
  }

  uploadFile( file )
  {
    return this.http.post( Config.API_ROOT_CORE + 'api/v1/files/upload', file );
  }

  saveFileEntry( file )
  {
    return this.http.post( Config.API_ROOT_SURVEYS + 'files', JSON.stringify(file), httpOptions );
  }

  deleteFileEntry( file )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'files/' + file.file_primary_id, httpOptions );
  }

  deleteUserAvatar( file )
  {
    return this.http.delete( Config.API_ROOT_CORE + 'api/v1/files/' + file.file_id, httpOptions );
  }

  processData(data: any) 
  {
    this.data = data;

    return this.data;
  }
}