import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Events } from 'ionic-angular';

// Config
import { Config } from '../config/config';
// Services
import { HelperService } from '../helpers/helper-service';
import { SurveyAttemptsService } from '../surveys/survey-attempts-service';
import { SurveyData } from '../surveys/survey-client-data';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json; charset=UTF-8;'
  })
}

@Injectable()
export class GoalsService {
  data: any;
  surveyData = SurveyData;

  constructor( 
    private http: HttpClient,
    private helperService: HelperService,
    private surveyAttemptsService: SurveyAttemptsService,
    private events: Events
  ) {
  }

  getGoalsForUserForSurvey( userId, surveyId, status = null )
  {
    let statusQuery = '';
    if ( status !== null )
    {
      statusQuery = '?status=' + status;
    }
    return this.http.get( Config.API_ROOT_SURVEYS + 'users/' + userId + '/goals/' + surveyId + statusQuery )
      .pipe( map( this.processData, this ));
  }

  saveGoal( goal )
  {
    if ( goal.goal_id > 0 )
    {
      return this.http.put( Config.API_ROOT_SURVEYS + 'goals/' + goal.goal_id, JSON.stringify(goal), httpOptions );
    } else {
      return this.http.post( Config.API_ROOT_SURVEYS + 'goals', JSON.stringify(goal), httpOptions );
    } 
  }

  saveGoalActivity( goalActivityObj )
  {
    return this.http.post( Config.API_ROOT_SURVEYS + 'goals/' + goalActivityObj.goal_id + '/activity', JSON.stringify(goalActivityObj), httpOptions );
  }

  deleteGoal( goalId )
  {
    return this.http.delete( Config.API_ROOT_SURVEYS + 'goals/' + goalId, httpOptions );
  }

  testActivityAgainstGoals( surveyAttempt )
  {
    if ( this.surveyData.goals.length > 0 )
    {
      for ( let goal of this.surveyData.goals )
      {
        // Parse goal criteria.
        let goalCriteria = JSON.parse( goal.criteria );
        let activityContributesToGoal = true;

        for ( let gc of goalCriteria )
        {
          let goalCriteriaMet = false;
          for ( let r of surveyAttempt.responses )
          {
            if ( gc.questionId === r.survey_question_id && gc.response === r.text )
            {
              goalCriteriaMet = true;
            }
          }

          if ( !goalCriteriaMet )
          {
            activityContributesToGoal = false;
            break;
          }
        }

        // If the activity passes all the checks in the goal,
        // then save to goal_activities.
        console.log(goalCriteria); 
        console.log(surveyAttempt);
        console.log(activityContributesToGoal);
        if ( activityContributesToGoal === true )
        {
          let goalActivityObj = {
            goal_id: goal.goal_id,
            survey_attempt_id: surveyAttempt.survey_attempt_id
          };
    
          this.saveGoalActivity( goalActivityObj ).subscribe();
          this.events.publish( 'goals:refresh' );
        }
      }
    }
  }

  processData(data: any) 
  {
    this.data = data;
    return this.data;
  } 
}