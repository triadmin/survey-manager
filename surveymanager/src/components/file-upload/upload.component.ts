// For this project, file uploader is dumb. 
// All it does is upload file(s) and send file 
// object back to the parent component.
// Parent component handles:
// - Whether or not uploader is displayed,
// - Saving file obj for specific app purpose (e.g. conference logo),
// - Deleting a file,
// - Determining number of simultaneous uploads.

import {
  Component,
  EventEmitter,
  OnInit,
  Input,
  Output
} from '@angular/core';
import {
  UploadOutput,
  UploadInput,
  UploadFile,
  humanizeBytes,
  UploaderOptions,
  UploadStatus
} from 'ngx-uploader';
import { 
  Platform,
  Events,
  LoadingController
} from 'ionic-angular';
import { 
  Camera, 
  CameraOptions 
} from '@ionic-native/camera';
import { 
  FileTransfer, 
  FileUploadOptions, 
  FileTransferObject 
} from '@ionic-native/file-transfer';

// Providers
import { FilesService } from '../../providers/files/files-service';
import { HelperService } from '../../providers/helpers/helper-service';
import { Config } from '../../providers/config/config';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html'
})
export class UploadComponent implements OnInit {
  @Input() uploadLabel = 'Drag and Drop Files Here to Upload';
  @Input() enabled = true;
  @Input() maxUploads = 1;
  @Input() public = 'No';

  @Output() fileRemoved = new EventEmitter<any>();
  @Output() fileAdded = new EventEmitter<any>();
  @Output() fileUploaded = new EventEmitter<any>();

  // File uploader stuff
  uploaderOptions: UploaderOptions;
  formData: FormData;
  files: UploadFile[];
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  filePurpose = 'file';
  fileError = false;
  fileErrorMessage = '';
  fileUploading = false;
  fileDisplay: any = {
    file: []
  };

  isCordova:boolean = false;
  imageURI:string;

  constructor(
    private filesService: FilesService,
    private helperService: HelperService,
    public camera: Camera,
    public fileTransfer: FileTransfer,
    public platform: Platform,
    public events: Events,
    public loadingCtrl: LoadingController
  ) {
    /*
    this.uploaderOptions = { 
      concurrency: 1, 
      maxUploads: this.maxUploads 
    };
    */

    // local uploading files array
    this.files = [];

    // input events, we use this to emit data to ngx-uploader
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;

    if ( this.platform.is('cordova') )
    {
      this.isCordova = true;
    }
  }

  // ------------  BROWSER BASED FILE UPLOAD  ------------ //
  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {
      this.fileError = false;
      this.fileUploading = true;
      this.startUpload();
    } else if (output.type === 'addedToQueue'  && typeof output.file !== 'undefined') { // add file to array when added
      this.files.push(output.file);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      // update current data in files array for uploading file
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'removed') {
      // remove file from array when removed
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    } else if (output.type === 'dragOver') {
      this.dragOver = true;
    } else if (output.type === 'dragOut') {
      this.dragOver = false;
    } else if (output.type === 'drop') {
      this.dragOver = false;
    } else if (output.type === 'done') {
      const fileObj = output.file.response;
      this.fileUploading = false;
      this.fileUploaded.emit( fileObj );
    }
  }

  startUpload(): void {
    let access_token = Config.ACCESS_TOKEN;
    const event: UploadInput = {
      type: 'uploadAll',
      url: Config.API_ROOT_CORE + 'api/v1/files/upload',
      method: 'POST',
      data: { 
        project: Config.APP_NAME,
        public: this.public 
      },
      headers: { Authorization: `Bearer ${access_token}` }
    };
    this.uploadInput.emit(event);
  }

  // ---------------  END BROWSER BASED FILE UPLOAD --------------- //
  // ---------------  DEVICE PHOTO UPLOAD  -------------------- //

  getPhoto()
  {
    const options: CameraOptions = {
      quality: 100,
      encodingType: this.camera.EncodingType.JPEG,
      destinationType: this.camera.DestinationType.FILE_URI,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: false,
      targetWidth: 1200,
      targetHeight: 1200,
      correctOrientation: true,
      saveToPhotoAlbum: true
    }

    this.camera.getPicture(options).then((imageData) => {
      this.imageURI = imageData;
      this.uploadFile();
    }, (err) => {
      this.events.publish('app:log', {
        message: 'Failed to upload photo.',
        status: 'error',
        type: 'file upload error'
      });
    });
  }

  //
  // Upload the file populated in this.imageURI to API server.
  //
  uploadFile() {
    let loader = this.loadingCtrl.create({
      content: "Uploading Photo..."
    });
    loader.present();

    const fileTransfer: FileTransferObject = this.fileTransfer.create();
  
    let options: FileUploadOptions = {
      fileKey: 'file',
      fileName: 'mobile_upload.jpg',      
      params: {
        project: Config.APP_NAME,
        public: this.public
      }
      ,headers: {
        Authorization: "Bearer " + Config.ACCESS_TOKEN
      }
    }
    
    fileTransfer.upload(this.imageURI, Config.API_ROOT_CORE + 'api/v1/files/upload', options)
      .then((data) => {
        this.fileUploaded.emit( JSON.parse( data.response ) );
        loader.dismiss();
        this.events.publish('app:log', {
          message: 'Photo uploaded successfully.',
          status: 'success',
          type: 'file upload success'
        });
      }, (err) => {
        this.events.publish('app:log', {
          message: 'Failed to upload photo.',
          status: 'error',
          type: 'file upload error'
        });
      });
  }

  // -------------  END DEVICE PHOTO UPLOAD  ----------------- //

  clearFileDisplay() {
    this.fileDisplay.file.length = 0;
  }

  enableUpload() {
    this.enabled = true;
  }

  disableUpload() {
    this.enabled = false;
  }

  ngOnInit() {
    this.files = [];
  }
}
