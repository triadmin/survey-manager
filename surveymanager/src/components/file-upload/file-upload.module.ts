import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UploadComponent } from './upload.component';
import { NgxUploaderModule } from 'ngx-uploader';

// Ionic Native imports
import { Camera } from '@ionic-native/camera';
import { FileTransfer, FileTransferObject } from '@ionic-native/file-transfer';

@NgModule({
  declarations: [
    UploadComponent
  ],
  imports: [
    IonicPageModule.forChild(UploadComponent),
    NgxUploaderModule
  ],
  providers: [
    Camera,
    FileTransfer,
    FileTransferObject,
  ],
  exports: [
    UploadComponent
  ]
})
export class UploadComponentModule {}
