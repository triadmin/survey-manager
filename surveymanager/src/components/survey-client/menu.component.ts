import { Component } from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams,
  Events
} from 'ionic-angular';

// Config
import { Config } from '../../providers/config/config';

// Permissions Service
import { UserPermissionsService } from '../../providers/users/user-permissions-service';

@Component({
  template: `
      <button *ngIf="showNewSurveyButton && menuPurpose != 'activity-data-entry' && menuPurpose != 'activityView'" ion-button icon-start full color="pink" (click)="closePopover('new-activity')">
        <ion-icon name="add-circle"></ion-icon> New Activity
      </button>

      <div *ngIf="menuPurpose == 'dashboard'">
        <button *ngIf="showSelectSurveysButton" ion-button clear icon-start (click)="closePopover('select-survey')">
          <ion-icon name="clipboard"></ion-icon> Select Another Survey
        </button>
        <button ion-button clear icon-start (click)="closePopover('print')">
          <ion-icon name="print"></ion-icon> Print Report
        </button>
        <button ion-button clear icon-start (click)="closePopover('goals')">
          <ion-icon name="medal"></ion-icon> Goals
        </button>
      </div>

      <div *ngIf="menuPurpose == 'activity-data-entry'">
        <button ion-button clear icon-start (click)="closePopover('cancel')">
          <ion-icon name="close"></ion-icon> Cancel
        </button>
      </div>

      <div *ngIf="menuPurpose == 'activityView'">
        <button ion-button clear icon-start (click)="closePopover('edit-activity')">
          <ion-icon name="create"></ion-icon> Edit Activity
        </button>

        <button ion-button clear icon-start (click)="closePopover('send-message')">
          <ion-icon name="send"></ion-icon> Send a Message
        </button>

        <button ion-button clear icon-start (click)="closePopover('add-task')">
          <ion-icon name="checkmark-circle-outline"></ion-icon> Add a Task
        </button>

        <button ion-button clear icon-start (click)="closePopover('print')">
          <ion-icon name="print"></ion-icon> Print Activity
        </button>

        <button ion-button full color="super-light-gray" icon-start color="danger" (click)="closePopover('delete')">
          <ion-icon name="trash"></ion-icon> Delete Activity
        </button>
      </div>
      <hr />
      <button *ngIf="showCloseButton" ion-button clear color="muted" icon-start (click)="closeSurveyClient()">
        <ion-icon name="arrow-back"></ion-icon> Back to survey manager
      </button>
  `
})
export class SurveyClientMenuPopoverPage {
  menuPurpose: string = '';

  // Permissions 
  showCloseButton:boolean = false;
  showNewSurveyButton:boolean = true;
  showSelectSurveysButton:boolean = false;

  constructor(
    private viewCtrl: ViewController,
    private navCtrl: NavController,
    private navParams: NavParams,
    private userPermissionsService: UserPermissionsService,
    private events: Events
  )
  {
    if ( this.navParams.get('purpose') )
    {
      this.menuPurpose = this.navParams.get('purpose');
    }

    this.menuPermissions();

    if ( Config.USER_APP_RECORD.surveys.length > 1 )
    {
      this.showSelectSurveysButton = true;
    }
  }

  menuPermissions()
  {
    if ( this.userPermissionsService.userHasRole( ['survey-manager-admin','survey-manager-project-level-admin'] ) )
    {
      this.showCloseButton = true;
    }

    if ( this.userPermissionsService.userHasRole( ['survey-manager-read-only'] ) )
    {
      this.showNewSurveyButton = false;
    }
  }

  closeSurveyClient()
  {
    this.events.publish( 'survey:close-client' );
    this.events.publish( 'menu:open', {
      menuType: 'mainMenu'
    });
    this.closePopover();
  } 

  publishLogOutEvent()
  {
    this.events.publish( 'user:logout' );
    this.closePopover();
  }

  closePopover( selectedItem = null )
  {
    this.viewCtrl.dismiss( selectedItem );
  }
}
