import { Component } from '@angular/core';
import { 
  NavParams,
  Events,
  ViewController
} from 'ionic-angular';

@Component({
  template: `
    <p margin-left>Compare to another Region</p>
    <ion-list>
      <button ion-item (click)="closePopover(r.list_item.title)" *ngFor="let r of regions.children">{{ r.list_item.title }}</button>
    </ion-list>
  `
})
export class CompareRegionPopoverPage {
  regions = [];

  constructor(
    private navParams: NavParams,
    private events: Events,
    private viewCtrl: ViewController
  )
  {
    if ( this.navParams.get('regions') )
    {
      this.regions = this.navParams.get('regions');
      console.log(this.regions);
    }
  }

  closePopover( selectedItem = null )
  {
    this.viewCtrl.dismiss( selectedItem );
  }
}
