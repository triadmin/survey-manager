import { Component } from '@angular/core';
import { 
  NavParams,
  Events,
  ViewController
} from 'ionic-angular';

@Component({
  template: `
    <p margin-left>Compare to another Date Range</p>  
    <ion-list>
      <button ion-item (click)="closePopover('period')">Previous Period</button>
      <button ion-item (click)="closePopover('year')">Previous Year</button>
    </ion-list>
  `
})
export class CompareDateRangePopoverPage {

  constructor(
    private navParams: NavParams,
    private events: Events,
    private viewCtrl: ViewController
  )
  {
  }

  closePopover( selectedItem = null )
  {
    this.viewCtrl.dismiss( selectedItem );
  }
}
