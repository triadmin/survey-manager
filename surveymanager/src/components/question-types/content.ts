import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'content',
  template: `
    <ion-item text-wrap *ngIf="context == 'data-entry'" class="large">{{ question.question_text }}</ion-item>

    <p *ngIf="context == 'render'">{{ question.question_text }}</p>
  `
})

export class ContentComponent
{
  @Input() context = '';
  @Input() question: any;
  
  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor(
    private helperSurveyService: HelperSurveyService
  ) {}

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
