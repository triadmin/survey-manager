import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'list-with-headers-text',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-list-header class="large" no-lines>{{ question.question_text }}</ion-list-header>
      <div *ngFor="let answer of question.answers; let parentIndex = index">
        <ion-list-header class="large" no-lines *ngIf="answer.extra == 'header'">
          {{ answer.answer_text }}
        </ion-list-header>
        <ion-item *ngIf="answer.extra != 'header'">
          {{ answer.answer_text }}

          <input type="number" class="list-input" [(ngModel)]="responses[parentIndex].value" (blur)="saveResponse(parentIndex)" item-right />
        </ion-item>
      </div>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <h2><strong>{{ question.question_text }}</strong></h2>
      <div *ngIf="question.responses">
        <ion-item *ngFor="let r of question.responses">
          {{ r.text }}
          <ion-badge item-end color="pink">{{ r.value }}</ion-badge>
        </ion-item>
      </div>
    </ng-container>
  `
})

export class ListWithHeadersTextComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  responses: any = [];

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveResponse( responseIndex )
  {
    if ( this.responses[responseIndex].value > 0 )
    {
      // Cast value to string first.
      this.responses[responseIndex].value = String(this.responses[responseIndex].value);

      this.surveyQuestionResponseService.saveResponse( this.question, this.responses[responseIndex], 'list-with-headers-text' )
        .subscribe(data => {
          // Update response with responseId
          let d:any = data;
          this.responses[responseIndex].survey_response_id = d.survey_response_id;
        });
    }
  }

  renderResponse()
  {    
    let i = 0;
    for ( let a of this.question.answers )
    {
      if ( this.question.responses )
      {
        for ( let r of this.question.responses )
        {
          if ( r.survey_answer_id === a.survey_answer_id )
          {
            this.responses[i].value = r.value;
          }
        }
      }

      i++;
    }
  }

  initResponses()
  {
    let i = 0;
    for ( let a of this.question.answers )
    {
      this.responses[i] = {
        survey_attempt_id: this.surveyAttemptId,
        survey_question_id: this.question.survey_question_id,
        survey_answer_id: a.survey_answer_id,
        text: a.answer_text,
        value: ''
      };
      i++;
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text + ' ' + '[' + r.value + ']' );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      this.initResponses();
      this.renderResponse();
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
