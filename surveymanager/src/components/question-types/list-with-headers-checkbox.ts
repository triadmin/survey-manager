import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'list-with-headers-checkbox',
  template: `
    <ion-list-header class="large" color="label">{{ question.question_text }}</ion-list-header>
    <div *ngFor="let answer of question.answers; let i = index">
      <ion-list-header *ngIf="answer.extra == 'header'">
        {{ answer.answer_text }}
      </ion-list-header>

      <ion-item *ngIf="answer.extra != 'header'">
        <ion-label>{{ answer.answer_text }}</ion-label>
        <ion-checkbox green [(ngModel)]="answer.ischecked" (ionChange)="saveResponse(answer)"></ion-checkbox>
      </ion-item>
    </div>
  `
})

export class ListWithHeadersCheckboxComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  response: any;

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveResponse( answer )
  {
    answer.SurveyAnswersValue = '';
    this.surveyQuestionResponseService.saveResponse( this.question, answer, 'list-with-headers-checkbox' )
      .subscribe(data => {
        //this.question.Responses.push(data.json().data);
      });
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.Responses.length > 0 )
    {
      // Loop over answers.  Is an answer has a response, mark it 'checked'.
      var responseObj = this.question.Responses;
      var answersObj = this.question.answers;

      for ( var i = 0; i <= answersObj.length - 1; i++ )
      {
        for ( var j = 0; j <= responseObj.length - 1; j++ )
        {
          // Do we have a response for this answer?
          if ( responseObj[j].SurveyResponsesAnswersId === answersObj[i].SurveyAnswersId )
          {
              answersObj[i].ischecked = true;
          }
        }
      }
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    //this.renderResponse();
  }
}
