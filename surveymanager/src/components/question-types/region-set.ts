import { 
  Component, 
  Input, 
  NgZone,
  Output,
  EventEmitter
} from '@angular/core';

import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { SurveyAttemptsService } from '../../providers/surveys/survey-attempts-service';
import { SurveyData } from '../../providers/surveys/survey-client-data';

declare var google;

@Component({
  selector: 'region-set',
  template: `
    <ng-container *ngIf="context == 'data-entry' || context == 'render'">
      <ion-list *ngIf="childrenArray.length > 0">
        <ion-list-header class="large">{{ question.question_text }}</ion-list-header>
        <ion-item *ngFor="let children of childrenArray; let i = index;">
          <ion-select 
            class="full-width"
            multiple 
            [(ngModel)]="selectedChildrenArray[i]" 
            [compareWith]="regionSetCompareFn"
            (ionChange)="showChildSelect($event, i, true)" 
            placeholder="{{ children[0].list_item.descriptor }}"
          >
            <ion-option *ngFor="let child of children" [value]="child">
              {{ child.list_item.title }}
            </ion-option>
          </ion-select>
        </ion-item>

        <ion-item no-lines>
          <ion-label stacked color="label">Specific Location</ion-label>
          <ion-input type="text" [(ngModel)]="placeSearchText" (keyup)="doPlacesSearch($event)"></ion-input>
        </ion-item>
        <ion-item *ngIf="noPlacesError">
          <strong>No matching locations found!</strong>
        </ion-item>
        <button ion-item *ngFor="let place of autocompleteItems" (click)="getPlaceCoords(place)">
          {{ place.description }}
        </button>
      </ion-list>
    </ng-container>
  `
})

export class RegonSetComponent
{
  @Input() context = '';
  @Input() regionSet:any = {};

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttempt: any;

  response:any = {};
  childrenArray = [];
  selectedChildrenArray = [];
  locationObj = {
    region_set_nodes: [],
    place: '',
    coords: []
  };

  GoogleAutocomplete = new google.maps.places.AutocompleteService();
  autocomplete = { input: '' };
  autocompleteItems = [];
  placeSearchText = '';
  noPlacesError = false;

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private surveyAttemptsService: SurveyAttemptsService,
    private zone: NgZone
  ) {
  }

  saveResponse()
  { 
    this.surveyAttemptsService.saveSurveyAttempt( this.surveyAttempt )
      .subscribe( data => {

      });
  }

  showChildSelect( nodes, index, save = false )
  {   
    // Get rid of all elements in childrenArray > index.
    this.childrenArray.length = index + 1;

    // Get children of all selected nodes.
    let selectedNodeChildren = [];
    let coordsArray = [];
    for ( let n of nodes )
    {
      if ( n.children )
      {
        for ( let c of n.children )
        {
          selectedNodeChildren.push( c ); 
        }
      }
    }
    console.log('This is the show child select method');
    if ( selectedNodeChildren.length > 0 )
    {
      console.log('Showing child select with:');
      console.log(selectedNodeChildren);
      this.childrenArray.push( selectedNodeChildren );
    }


    // Go through all selected children and get coords.
    // We want 1 set of coords for each Region Set Item.
    for ( let sc of this.selectedChildrenArray )
    {
      for ( let c of sc )
      {
        if ( c.list_item.coords !== null && c.list_item.coords !== undefined && c.list_item.coords !== '' )
        {
          var coords = JSON.parse( c.list_item.coords );
          let placeCoords;

          // Check for center point coordinates first.
          if ( coords.point )
          {
            placeCoords = coords.point;
          }
          coordsArray.push( placeCoords );
        }
      }
    }

    // Now, get selectedChildrenArray into something we can save.
    let i = 0;
    let selectedChildrenArrayToSave = [];
    for ( let sca of this.selectedChildrenArray )
    {
      let selectedChildren = [];
      for ( let selectedChild of sca )
      {
        let selectedChildObj = {
          title: selectedChild.list_item.title,
          region_list_item_id: selectedChild.list_item.region_list_item_id
        };
        selectedChildren.push( selectedChildObj );
      }

      selectedChildrenArrayToSave.push( selectedChildren );
    }

    this.locationObj.region_set_nodes = selectedChildrenArrayToSave;
    this.locationObj.coords = coordsArray;
    this.surveyAttempt.location = JSON.stringify( this.locationObj );

    if ( save )
    {
      this.saveResponse();
    }
  }

  renderResponse()
  {
    let location = JSON.parse( this.surveyAttempt.location );

    // Recreate full selectedChildrenArray
    let i = 0;
    for ( let nodeArray of location.region_set_nodes )
    {
      this.selectedChildrenArray[i] = [];
      for ( let node of nodeArray )
      {
        this.selectedChildrenArray[i].push( findNode( node.region_list_item_id, this.regionSet ) );
      }
      i++;
    }

    // Recursive function to find a region_list_item in the region tree.
    function findNode( region_list_item_id, parentNode )
    {
      if ( parentNode.list_item && parentNode.list_item.region_list_item_id === region_list_item_id )
      {
        return parentNode;
      } else if ( parentNode.children ) {
        let foundChild = null;
        for ( let child of parentNode.children )
        {
          if ( child.list_item.region_list_item_id === region_list_item_id )
          {
            return child;
          } else {
            foundChild = findNode( region_list_item_id, child );
            if ( foundChild !== undefined )
            {
              return foundChild;
            }
          }
        }
      }
    }

    // Make sure all selected children levels are shown.
    i = 0;
    console.log(this.selectedChildrenArray);
    for ( let sca of this.selectedChildrenArray )
    {
      this.showChildSelect( sca, i );
      i++;
    }
  }

  regionSetCompareFn( r1, r2 )
  {
    return r1 && r2 ? r1.list_item.region_list_item_id === r2.list_item.region_list_item_id : r1 === r2;
  }

  doPlacesSearch( ev )
  {
    if ( ev.target.value.length < 4 ) {
      this.autocompleteItems = [];
      return;
    }

    this.GoogleAutocomplete.getPlacePredictions({ input: ev.target.value },
    ( predictions, status ) => {
      this.autocompleteItems = [];
      this.zone.run(() => {
        if ( predictions !== null )
        {
          this.noPlacesError = false;
          predictions.forEach((prediction) => {
            this.autocompleteItems.push(prediction);
          });
        } else {
          this.noPlacesError = true;
        }
      });
    });
  }

  getPlaceCoords( place )
  {
    var geocoder = new google.maps.Geocoder();
    var self = this;

    // Clear autocompleteItems.
    this.autocompleteItems = [];
    // Replace text in searchbox with selected place.
    this.placeSearchText = place.description;
    geocoder.geocode({ 'placeId': place.place_id }, function( results, status ) {
      if (status == google.maps.GeocoderStatus.OK) {
        self.locationObj.place = place.description;
        let placeCoordObj = {
          lat: results[0].geometry.location.lat(),
          lng: results[0].geometry.location.lng()
        }
        self.locationObj.coords.push( placeCoordObj );
        self.surveyAttempt.location = JSON.stringify( self.locationObj );

        // Save selection.
        self.saveResponse();
      } else {
        // Cannot find place.
      }
    });
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      console.log(this.regionSet);
      console.log(this.regionSet.children);
      this.childrenArray.push( this.regionSet.children );
      console.log(this.childrenArray);
    
      if ( this.surveyAttempt.location )
      {
        this.renderResponse();
      }
    }
  }
}
