import { 
  Component, 
  Input, 
  Output, 
  EventEmitter 
} from '@angular/core';

import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'textareafield',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <div margin-left>
        <ion-label class="large" color="label" stacked>{{ question.question_text }}</ion-label>
        <rich-text #richText [editorHeight]="'150px'" [formControlItem]="response_textarea" placeholder="Enter comments here..." (textComplete)="saveResponse()"></rich-text>
      </div>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <h2 margin-bottom><strong>{{ question.question_text }}</strong></h2>
      <div 
        class="normal-text"
        *ngIf="question.responses && question.responses.length > 0" 
        padding 
        [innerHtml]="question.responses[0].text"
      ></div>
    </ng-container>
  `
})

export class TextareaComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  response: any;

  @Output() editQuestionConfig = new EventEmitter<any>();

  // RichText Editor
  response_textarea: FormControl;

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService,
    public formBuilder: FormBuilder
  ) {}

  saveResponse( event )
  {
    this.question.responses = [];
    // Get rich-text field value
    this.response.text = this.response_textarea.value;
    this.surveyQuestionResponseService.saveResponse( this.question, this.response, 'textarea' )
      .subscribe(data => {
        this.question.responses.push( data );
        this.response = data;
      });
  }

  renderResponse()
  {
    if ( this.question.responses.length > 0 )
    {
      // Init the richText component.
      this.response_textarea = this.formBuilder.control(this.question.responses[0].text);   
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      this.response = {
        text: '',
        survey_question_id: this.question.survey_question_id,
        survey_attempt_id: this.surveyAttemptId,
        response_id: 0
      };
      this.response_textarea = this.formBuilder.control('');
      if ( this.question.responses !== undefined )
      {
        this.renderResponse();
      }
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
