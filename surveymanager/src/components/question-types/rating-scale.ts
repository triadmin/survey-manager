import { 
  Component, 
  forwardRef, 
  Input
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'rating-scale',
  template: `
    <button 
      ion-button 
      small 
      [outline]="isActiveButton(i)" 
      [color]="isActiveButton(i) ? 'primary' : 'warning'" 
      (click)="handleButtonClick(i)"
      *ngFor="let button of buttons; let i = index"
    >
      {{ button }}
    </button>
  `,
  providers: [{ 
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => RatingScaleComponent),
    multi: true
  }]
})

export class RatingScaleComponent implements ControlValueAccessor {
  @Input() buttons: any = [];
  @Input() question: any = {};

  selectedButtonIndex: number = 0;
  selectedButtonValue: any;

  // The internal data model (ngModel)
  private innerValue: any = '';

  // The value we save to the database.
  private saveValue: any = '';

  constructor() {
  }

  propagateChange = (_: any) => {};

  writeValue(value: any) {
    if (value !== undefined) {
      this.innerValue = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  // Get accessor
  get value(): any {
    return this.innerValue;
  };

  // Set accessor including call the onchange callback
  set value(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
    this.propagateChange(this.innerValue);
  }

  handleButtonClick( buttonIndex )
  {
    this.innerValue = this.buttons[buttonIndex];
    this.selectedButtonIndex = buttonIndex;
    this.propagateChange(this.innerValue);

    if ( this.question )
    {
      this.question.responses = [];
      let responseObj = {
        survey_answer_id: 0,
        text: String(this.innerValue)
      };
      this.question.responses.push( responseObj );
    }
  }

  isActiveButton( buttonIndex )
  {
    if ( this.buttons[buttonIndex] === this.innerValue )
    {
      // Turn button border off
      // for selected button
      return false;
    }

    return true;
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.question )
    {
      let extra = JSON.parse( this.question.extra );
      // Determine our buttons.
      for ( let a = 1; a <= extra.scale_size; a++ )
      {
        this.buttons.push(a);
      } 
    }
  }
}