import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'checkbox',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-list-header class="large" no-lines>{{ question.question_text }}</ion-list-header>
      <ion-item *ngFor="let answer of question.answers">
        <ion-label>{{ answer.answer_text }}</ion-label>
        <ion-checkbox [(ngModel)]="answer.checked" (click)="saveResponse(answer)"></ion-checkbox>
      </ion-item>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <h2><strong>{{ question.question_text }}</strong></h2>
      <div *ngIf="question.responses">
        <ion-item *ngFor="let r of question.responses">
          {{ r.text }}
        </ion-item>
      </div>
    </ng-container>
  `
})

export class CheckboxComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;  
  response: any;

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {}

  saveResponse( answer )
  {
    if ( answer.checked )
    {
      this.response.text = answer.answer_text;
      this.response.survey_answer_id = answer.survey_answer_id;

      // Save the response
      this.surveyQuestionResponseService.saveResponse( this.question, this.response, 'checkbox' )
        .subscribe( data => {
          this.question.responses.push( data );
        });
    } else {
      // Delete the response and remove it from the responses object.
      // We need to add the survey_response_id to this.response object.
      for ( let r of this.question.responses )
      {
        // If answer.survey_answer_id === r.survey_answer_id, we need to delete that response.
        if ( answer.survey_answer_id === r.survey_answer_id )
        {
          this.surveyQuestionResponseService.deleteResponse( this.question, r )
            .subscribe( data =>  {
              let j = this.question.responses.length;
              while ( j-- ) 
              {
                if ( this.question.responses[j].survey_response_id === r.survey_response_id )
                {
                  this.question.responses.splice( j, 1 );
                }
              }
            });
        }
      }
    }
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.responses.length > 0 )
    {
      // Loop over answers.  If an answer has a response, mark it 'checked'.
      for ( let a of this.question.answers )
      {
        for ( let r of this.question.responses )
        {
          if ( r.survey_answer_id === a.survey_answer_id )
          {
            a.checked = true
          }
        }
      }
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      // Entering data.
      this.response = {
        text: '',
        survey_question_id: this.question.survey_question_id,
        survey_attempt_id: this.surveyAttemptId,
        response_id: 0
      };

      if ( this.question.responses !== undefined )
      {
        this.renderResponse();
      } else {
        this.question.responses = [];
      }
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
