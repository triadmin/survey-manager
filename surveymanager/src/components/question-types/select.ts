import { 
  Component, 
  Input, 
  Output, 
  EventEmitter 
} from '@angular/core';

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'select-single',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-item>
        <ion-label class="large" color="label" stacked>{{ question.question_text }}</ion-label>
        <ion-select [(ngModel)]="response.text" (ionChange)="saveResponse()">
          <ion-option *ngFor="let answer of question.answers" value="{{ answer.answer_text }}">{{ answer.answer_text }}</ion-option>
        </ion-select>
      </ion-item>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <p>{{ question.question_text }} 
      <strong 
        *ngIf="question.responses && question.responses.length > 0"
      >
        {{ question.responses[0].text }}
      </strong></p>
    </ng-container>
  `
})

export class SelectComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  response: any;

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveResponse()
  {
    this.question.responses = [];

    // Need to find the answerId and add it to response object.
    for ( let a of this.question.answers )
    {
      if ( a.answer_text === this.response.text )
      {
        this.response.survey_answer_id = a.survey_answer_id;
      }
    }
    this.surveyQuestionResponseService.saveResponse( this.question, this.response, 'select' )
      .subscribe( data => {
        this.question.responses.push( data );
        this.response = data;
      });
  }

  renderResponse()
  {
    if ( this.question.responses.length > 0 )
    {
      this.response = this.question.responses[0];
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      this.response = {
        text: '',
        survey_question_id: this.question.survey_question_id,
        survey_attempt_id: this.surveyAttemptId,
        response_id: 0
      };

      if ( this.question.responses !== undefined )
      {
        this.renderResponse();
      }
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
