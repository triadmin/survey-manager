import { 
  Component, 
  Input, 
  Output, 
  EventEmitter 
} from '@angular/core';

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'select-multi',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-item>
        <ion-label class="large" stacked color="label">{{ question.question_text }}</ion-label>
        <ion-select multiple="true" [(ngModel)]="response" (ionChange)="saveResponse($event)">
          <ion-option *ngFor="let answer of question.answers" value="{{ answer.answer_text }}">{{ answer.answer_text }}</ion-option>
        </ion-select>
      </ion-item>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <p>{{ question.question_text }} This needs further editing.</p>
    </ng-container>
  `
})

export class SelectmultiComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  response: any = [];

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveResponse( answer )
  {
    // answer here is just an array of text that we selected.
    this.question.responses = [];
    let response = {};
    for ( let a of answer )
    {
      for ( let a2 of this.question.answers )
      {
        if ( a2.answer_text === a )
        {
          response = {
            text: a,
            survey_answer_id: a2.survey_answer_id,
            survey_question_id: this.question.survey_question_id,
            survey_attempt_id: this.surveyAttemptId
          }
          this.surveyQuestionResponseService.saveResponse( this.question, response, 'select-multi' )
            .subscribe(data => {
              this.question.responses.push( data );
            });
        }
      }
    }
  }

  renderResponse()
  {
    // Look for any reponses.
    if ( this.question.responses.length > 0 )
    {
      // Loop over answers.  If an answer has a response, mark it 'checked'.
      for ( let a of this.question.answers )
      {
        for ( let r of this.question.responses )
        {
          if ( r.survey_answer_id === a.survey_answer_id )
          {
            this.response.push( r.text );
          }
        }
      }
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      if ( this.question.responses !== undefined )
      {
        this.renderResponse();
      }
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
