import { 
  Component, 
  Input,
  Output,
  EventEmitter 
} from '@angular/core';
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { SurveyAttemptsService } from '../../providers/surveys/survey-attempts-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'survey-attempt',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-item>
        <ion-label class="large" color="label" stacked>Title for this entry</ion-label>
        <ion-input [(ngModel)]="surveyAttempt.title" type="text" (ionBlur)="saveResponse()" clearInput  autocorrect="on"></ion-input>
      </ion-item>

      <custom-date-picker 
        label="Entry Date"
        label_class="large"
        name="entry_date"
        [(ngModel)]="surveyAttempt.date"
      ></custom-date-picker>

      <ng-container *ngIf="this.question.extra.showFollowUp">
        <ion-item>
          <ion-label class="large" color="label">This entry requires follow-up</ion-label>
          <ion-toggle [(ngModel)]="followUpRequired" (ionChange)="saveResponse()"></ion-toggle>
        </ion-item>

        <ion-item>
          <ion-label class="large" color="label" stacked>This entry is a follow-up to a previous activity</ion-label>
          <ion-select [(ngModel)]="surveyAttempt.follow_up_to_survey_id" (ionChange)="saveResponse()" placeholder="Select Previous Activity">
            <ion-option *ngFor="let fua of followUpActivities" value="{{ fua.survey_attempt_id }}">
              <span *ngIf="fua.title == null">[No Title]</span>
              <span *ngIf="fua.title != null">{{ fua.title }}</span>
            </ion-option>
          </ion-select>
        </ion-item>
      </ng-container>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <h2><strong>{{ surveyAttempt.title }}</strong></h2>
      <p>Entered by {{ surveyAttempt.survey_user.first_name }} {{ surveyAttempt.survey_user.last_name }} on {{ surveyAttempt.date | date:'MMM d, y' }}</p>
      <ion-badge color="warning" *ngIf="surveyAttempt.follow_up_required == 'Yes'">Follow-up Required</ion-badge>
      <p *ngIf="locationString != ''"><strong>Location</strong> {{ locationString }}</p>
    </ng-container>
  `
})

export class SurveyAttemptComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttempt: any = {};
  @Input() surveyId: number;

  // Create our response object
  // for when there's no data
  response: any = {};
  followUpActivities: any;
  followUpRequired:boolean = false;
  locationString:any = {};

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor(
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private surveyAttemptsService: SurveyAttemptsService,
    private helperSurveyService: HelperSurveyService
  ) {
    //this.surveyAttempt.survey_user = {};
  }

  saveResponse()
  {
    if ( this.followUpRequired === true )
    {
      this.surveyAttempt.follow_up_required = 'Yes';
    } else {
      this.surveyAttempt.follow_up_required = 'No';
    }
    this.surveyAttempt.follow_up_to_survey_id = +this.surveyAttempt.follow_up_to_survey_id;
    this.surveyAttempt.survey_id = this.surveyId;

    this.surveyAttemptsService.saveSurveyAttempt( this.surveyAttempt )
      .subscribe(data => {
      });
  }

  renderResponse()
  {
    // Check to see if the Requires Follow Up toggle should be checked
    if ( this.surveyAttempt.follow_up_required == 'Yes' )
    {
      this.followUpRequired = true;
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      // Check any extra config options. 
      // Make sure we have an extra string AND it's not already 
      // parsed into an object first.
      if ( this.question.extra !== '' && typeof this.question.extra !== 'object' )
      {
        // Are we showing follow-up questions?
        this.question.extra = JSON.parse( this.question.extra );
      }

      if ( this.question.extra.showFollowUp )
      {
        // Get any prior survey attempts.
        // This is so we can display attempts to select for a follow-up.
        // Limit this list to only previous activities that have been marked for follow-up.
        this.surveyAttemptsService.getSurveyAttemptsForFollowUp( this.surveyId )
          .subscribe(data => {
            this.followUpActivities = data;
          });
      }
    
      this.renderResponse();
    } else if ( this.context === 'render' ) {
      // Try to parse location string.
      if ( this.surveyAttempt.location !== '' )
      {
        let locationObj = JSON.parse( this.surveyAttempt.location );
        if ( locationObj.region_set_nodes )
        {
          let locationNodeTitleArray = [];
          for ( let node of locationObj.region_set_nodes )
          {
            for ( let item of node )
            {
              locationNodeTitleArray.push( item.title );
            }            
          }

          this.locationString = locationNodeTitleArray.join( ', ' );
        }
      }
    }
  }
}
