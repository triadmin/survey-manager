import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { 
  ModalController, 
  Events,
  ActionSheetController 
} from 'ionic-angular';

// Modals
import { PhotoViewModalPage } from '../../components/modals/photo-viewer.modal';

// Services
import { HelperService } from '../../providers/helpers/helper-service';
import { FilesService } from '../../providers/files/files-service';
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';
;
@Component({
  selector: 'upload',
  template: `
    <ng-container *ngIf="context == 'data-entry' || context == 'render'">
      <div margin-left>
        <ion-label class="large" color="label" stacked margin-bottom>{{ question.question_text }}</ion-label>
        <app-upload
          (fileUploaded)="fileUploaded($event)"        
          uploadLabel="Drag and drop files here"
          public="yes"
        ></app-upload>

        <div padding id="photo-container">
          <file-list
            [files]="files"
            (fileToView)="viewPhoto($event)"
            (fileToDelete)="presentDeleteFileActionSheet($event)"
          ></file-list>
        </div>
      </div>
    </ng-container>
  `
})

export class UploadComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  response: any = {};
  files = [];

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperService: HelperService,
    private filesService: FilesService,
    private modalCtrl: ModalController,
    private events: Events,
    private actionSheetCtrl: ActionSheetController,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  fileUploaded( fileObj )
  {
    let newFileObj:any = this.helperService.makeFileObject( fileObj );
    newFileObj.file_purpose = 'upload_question_type_file';
    newFileObj.survey_id = this.question.survey_id;
    newFileObj.survey_question_id = this.question.survey_question_id;
    newFileObj.survey_attempt_id = this.surveyAttemptId;

    this.filesService.saveFileEntry( newFileObj )
      .subscribe( data => {
        let d:any = data;
        this.response = {
          text: String(d.file_primary_id),
          survey_question_id: this.question.survey_question_id,
          survey_attempt_id: this.surveyAttemptId
        };
        this.files.push( d );
        this.saveResponse();
      },
      error => {
        console.log(error);
      });
  }

  saveResponse()
  {
    this.surveyQuestionResponseService.saveResponse( this.question, this.response, 'upload' )
      .subscribe( data => {
        // Not sure we need this for uploade qType.
        this.question.responses.push( data );
        this.response = data;
      });
  }

  renderResponse()
  {
    if ( this.question.responses.length > 0 )
    {
      for ( let r of this.question.responses )
      {
        let fileId = +r.text;
        this.filesService.getFile( fileId )
          .subscribe( data => {
            this.files.push( data );
          });
      }
    }
  }

  viewPhoto( photo )
  {
    let photoViewerModal = this.modalCtrl.create( PhotoViewModalPage, {
      photo: photo
    });
    photoViewerModal.present();
  }

  deleteFile( file )
  {
    this.filesService.deleteFileEntry( file )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'File has been deleted.',
          status: 'warning',
          type: 'delete photo'
        });

        // Remove file from list.
        let j = this.files.length;
        while ( j-- ) 
        {
          if ( this.files[j].file_id === file.file_id )
          {
            this.files.splice( j, 1 );
          }
        }

        // Remove from question responses collection.
        let i = this.question.responses.length;
        while ( i-- ) 
        {
          if ( +this.question.responses[i].text === file.file_primary_id )
          {
            // Delete response from database.
            this.surveyQuestionResponseService.deleteResponse( this.question, this.question.responses[i] )
              .subscribe( data => {

              });
            this.question.responses.splice( i, 1 );
          }
        }
      });
  }

  presentDeleteFileActionSheet( file )
  {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Are you sure you want to delete this file?',
      buttons: [
        {
          text: 'Delete file',
          role: 'destructive',
          handler: () => {
            this.deleteFile( file );
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            
          }
        }
      ]
    });

    actionSheet.present();
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  ngOnInit()
  {
    //console.log(this.question);
    if ( this.context === 'data-entry' )
    {
      this.response = {
        text: '',
        survey_question_id: this.question.survey_question_id,
        survey_attempt_id: this.surveyAttemptId,
        response_id: 0
      };

      if ( this.question.responses !== undefined )
      {
        this.renderResponse();
      }
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );

      if ( this.question.responses !== undefined )
      {
        this.renderResponse();
      }
    }
  }
}
