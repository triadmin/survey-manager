import { 
  Component, 
  Input, 
  Output, 
  EventEmitter 
} from '@angular/core';

// Providers
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'radio',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-list no-lines [(ngModel)]="response.text" radio-group>
        <ion-list-header class="large">{{ question.question_text }}</ion-list-header>
        <ion-item *ngFor="let answer of question.answers">
          <ion-label>{{ answer.answer_text }}</ion-label>
          <ion-radio 
            type="radio" 
            (click)="saveResponse(answer)" 
            value="{{ answer.answer_text }}"
            id="radio-{{ answer.survey_answer_id }}"
          ></ion-radio>
        </ion-item>
      </ion-list>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <p>{{ question.question_text }} 
        <strong *ngIf="question.responses && question.responses.length > 0">{{ question.responses[0].text }}</strong>
      </p>
    </ng-container>
  `
})

export class RadioComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;
  response: any;

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveResponse( answer )
  {
    this.question.responses = [];

    this.response.survey_answer_id = answer.survey_answer_id

    this.surveyQuestionResponseService.saveResponse( this.question, this.response, 'radio' )
      .subscribe(data => {
        this.question.responses.push( data );
        this.response = data;
      });
  }

  renderResponse()
  {
    // Look for any reponses (this is a radio button, we're only going to have 1 response.)
    if ( this.question.responses.length > 0 )
    {
      this.response = this.question.responses[0];
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      this.response = {
        text: '',
        survey_question_id: this.question.survey_question_id,
        survey_attempt_id: this.surveyAttemptId,
        response_id: 0
      };
      console.log(this.question.responses);
      if ( this.question.responses !== undefined )
      {
        console.log('rendering radio response');
        this.renderResponse();
      }
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      // Rendering survey results.
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
