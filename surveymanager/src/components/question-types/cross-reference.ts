import { 
  Component, 
  Input, 
  Output, 
  EventEmitter 
} from '@angular/core';

// Providers
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';
import { SurveysService } from '../../providers/surveys/surveys-service';
import { SurveyAttemptsService } from '../../providers/surveys/survey-attempts-service';

@Component({
  selector: 'cross-reference',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-item>
        <ion-label class="large" color="label" stacked>{{ question.question_text }}</ion-label>
        <ion-select 
          [(ngModel)]="selectedSurveyAttempt" 
          (ionChange)="getResponsesToCrossReferenceQuestion($event)"
          [compareWith]="surveyAttemptCompareFn"
        >
          <ion-option *ngFor="let sa of surveyAttempts" [value]="sa">{{ sa.title }}</ion-option>
        </ion-select>
      </ion-item>

      <ion-list-header class="large" no-lines *ngIf="crossReferenceResponses.length > 0">Select items that apply.</ion-list-header>
      <ion-item *ngFor="let r of crossReferenceResponses">
        <ion-label>{{ r.text }}</ion-label>
        <ion-checkbox [(ngModel)]="r.checked" (ionChange)="saveResponse(r, $event)"></ion-checkbox>
      </ion-item>
    </ng-container>

    <ng-container *ngIf="context == 'render'">
      <p>{{ question.question_text }} 
        <strong *ngIf="question.responses && question.responses.length > 0">{{ question.responses[0].text }}</strong>
      </p>
    </ng-container>
  `
})

export class CrossReferenceComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() surveyAttemptId: number;

  response:any = {};
  survey:any = {};
  surveyAttempts = [];
  crossReferenceQuestion:any = {};
  crossReferenceResponses = [];
  selectedSurveyAttempt = {};

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService,
    private surveysService: SurveysService,
    private surveyAttemptsService: SurveyAttemptsService
  ) {
  }

  getResponsesToCrossReferenceQuestion( ev )
  {
    this.crossReferenceResponses = [];
    for ( let r of ev.responses )
    {
      if ( r.survey_question_id === this.crossReferenceQuestion.survey_question_id )
      {
        this.crossReferenceResponses.push( r );
      }
    }
  }

  saveResponse( answer, ev )
  {
    // answer in this context is actually a response from another survey.
    if ( ev.checked )
    {
      this.response.text = answer.text;
      this.response.answer_id = 0;
      this.response.value = JSON.stringify( { survey_attempt_id: answer.survey_attempt_id } );

      // Save the response
      this.surveyQuestionResponseService.saveResponse( this.question, this.response, 'cross-reference' )
        .subscribe( data => {
          this.question.responses.push( data );
        });
    } else {
      // Delete the response and remove it from the responses object.
      // We need to add the survey_response_id to this.response object.
      for ( let r of this.question.responses )
      {
        // If answer.text === r.text, we need to delete that response.
        if ( answer.text === r.text )
        {
          this.surveyQuestionResponseService.deleteResponse( this.question, r )
            .subscribe( data =>  {
              let j = this.question.responses.length;
              while ( j-- ) 
              {
                if ( this.question.responses[j].survey_response_id === r.survey_response_id )
                {
                  this.question.responses.splice( j, 1 );
                }
              }
            });
        }
      }
    }
  }

  renderResponse()
  {
    // Look for any reponses.
    console.log(this.question.responses);
    if ( this.question.responses.length > 0 )
    {
      // First let's see what survey attempt we're working with.
      let selectedSurveyAttemptId = JSON.parse( this.question.responses[0].value ).survey_attempt_id;

      for ( let sa of this.surveyAttempts )
      {
        if ( sa.survey_attempt_id === selectedSurveyAttemptId )
        {
          console.log('selected SA');
          console.log(sa);
          this.selectedSurveyAttempt = sa;
          this.getResponsesToCrossReferenceQuestion( sa );
        }
      }

      // Loop over crossReferenceResponses.  
      // If an answer has a response, mark it 'checked'.
      for ( let crr of this.crossReferenceResponses )
      {
        for ( let r of this.question.responses )
        {
          if ( r.text === crr.text )
          {
            crr.checked = true
          }
        }
      }
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  surveyAttemptCompareFn( sa1, sa2 )
  {
    return sa1 && sa2 ? sa1.survey_attempt_id === sa2.survey_attempt_id : sa1 === sa2;
  }

  // Returns strings for inclusion into a PDF Document Definition object.
  getDataForPdf()
  {
    // Format responses.
    let responseArray = [];
    for ( let r of this.question.responses )
    {
      responseArray.push( r.text );
    }

    let pdfObj = {
      label: this.question.question_text,
      responses: responseArray
    };

    return pdfObj;
  }

  ngOnInit()
  {
    // Entering data.
    this.response = {
      text: '',
      survey_question_id: this.question.survey_question_id,
      survey_attempt_id: this.surveyAttemptId,
      response_id: 0
    };

    // Set up our question type configuration.
    let questionConfig = JSON.parse( this.question.extra );
    if ( questionConfig.surveyId !== undefined )
    {
      this.surveysService.getSurvey( questionConfig.surveyId )
        .subscribe( data => {
          this.survey = data;

          // Find question we're cross-referencing.
          for ( let q of this.survey.questions )
          {
            if ( q.survey_question_id === questionConfig.questionId )
            {
              this.crossReferenceQuestion = q;
            }
          }

          // Get survey attempts.
          this.surveyAttemptsService.getSurveyAttempts( questionConfig.surveyId )
            .subscribe( surveyAttempts => {
              this.surveyAttempts = surveyAttempts;

              if ( this.question.responses !== undefined )
              {
                this.renderResponse();
              }
            });
        });
    }
    
    if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      // Rendering survey results.
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
