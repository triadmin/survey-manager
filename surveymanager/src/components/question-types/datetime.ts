import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';

// Services
import { SurveyQuestionResponseService } from '../../providers/surveys/survey-question-response-service';
import { HelperSurveyService } from '../../providers/helpers/helper-survey-service';

@Component({
  selector: 'datetime',
  template: `
    <ng-container *ngIf="context == 'data-entry'">
      <ion-list-header>{{ question.SurveyQuestionsText }}</ion-list-header>
      <ion-item>
        <ion-label>Date</ion-label>
        <ion-datetime displayFormat="MMM DD, YYYY" (ionChange)="saveResponse($event, 'date')" [(ngModel)]="date"></ion-datetime>
      </ion-item>
      <ion-item *ngIf="include_time != false">
        <ion-label>Time</ion-label>
        <ion-datetime disabled (ionChange)="saveResponse($event, 'time')" displayFormat="h:mm A" pickerFormat="h mm A" [(ngModel)]="time"></ion-datetime>
      </ion-item>
    </ng-container>

    <ng-container *ngIf="context == 'render'">Date time data goes here</ng-container>

    <ng-container *ngIf="context == 'survey-create'">
      <ion-row>
        <ion-col col-1>
          <ion-icon class="icon-big" name="calendar" color="light-gray"></ion-icon>
        </ion-col>
        <ion-col col-10>
          <h2 [innerHtml]="question.question_text"></h2>
          <p><b>Date and Time</b></p>
        </ion-col>
        <ion-col col-1>
          <button icon-only small (click)="editQuestion()">
            <ion-icon color="primary" name="create"></ion-icon>
          </button>
        </ion-col>
      </ion-row>
    </ng-container>
  `
})

export class DatetimeComponent
{
  @Input() context = '';

  // For rendering.
  @Input() questions = [];
  @Input() questionIdentifier = '';

  // For data entry.
  @Input() question: any;
  @Input() include_time: boolean;
  @Input() surveyAttemptId: number;
  response: any;
  date: any = new Date().toISOString();
  time: any = new Date().toISOString();

  @Output() editQuestionConfig = new EventEmitter<any>();

  constructor( 
    private surveyQuestionResponseService: SurveyQuestionResponseService,
    private helperSurveyService: HelperSurveyService
  ) {
  }

  saveResponse( event, type )
  {
    var answer;
    if ( type == 'date' )
    {
      // Let's convert to timestamp.  That way it can be stored in a
      // MySQL text field and allows for easier conversion in JS.
      var month = event.month.value;
      var day = event.day.value;
      var year = event.year.value;
      var date = month + '/' + day + '/' + year;
      var timestamp = new Date( date ).getTime();
      answer = {
        'SurveyAnswersValue': 'date',
        'SurveyAnswersText': timestamp,
        'SurveyAnswersSurveyQuestionsId': this.question.SurveyQuestionsId,
        'SurveyAnswersId': 0
      };
      this.surveyQuestionResponseService.saveResponse( this.question, answer, 'text' )
      .subscribe(data => {
        //this.question.Responses.push(data.json().data);
      });
    } else if ( type == 'time' ) {
      /*

      We've disabeld time for the demo because it's overwritting the date.

      var hour = event.hour.value;
      var minute = event.minute.value;
      var ampm = event.ampm.text; // gives PM instead of pm. Not sure that's at all important.
      var time = hour + ':' + minute + ' ' + ampm;
      answer = {
        'SurveyAnswersValue': 'time',
        'SurveyAnswersText': time,
        'SurveyAnswersSurveyQuestionsId': this.question.SurveyQuestionsId,
        'SurveyAnswersId': 0
      };
      */
    }

    //this.surveyService.saveResponse( answer, 'text' ).subscribe(data => {});
  }

  renderResponse()
  {
    if ( this.question.Responses.length > 0 )
    {
      var responseObj = this.question.Responses;
      this.response = responseObj[0].SurveyResponsesText;

      // Convert timestamp to date
      this.date = new Date( parseInt( this.response, 10 ) ).toISOString();

      // Convert timestamp to time
    }
  }

  editQuestion()
  {
    this.editQuestionConfig.emit();
  }

  ngOnInit()
  {
    if ( this.context === 'data-entry' )
    {
      this.renderResponse();
    } else if ( this.context === 'render' && this.questionIdentifier !== '' ) {
      this.question = this.helperSurveyService.getQuestionFromQuestionIdentifier( this.questions, this.questionIdentifier );
    }
  }
}
