import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { UserDetailsComponent } from './user-details.component';

@NgModule({
  declarations: [
    UserDetailsComponent
  ],
  imports: [
    IonicPageModule.forChild(UserDetailsComponent)
  ],
  providers: [
  ],
  exports: [
    UserDetailsComponent
  ]
})
export class UserDetailsComponentModule {}
