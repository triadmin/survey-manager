import { Component, Input } from '@angular/core';
import { 
  Events,
  PopoverController
} from 'ionic-angular';

// Popover pages.
import { UserAccountPopoverPage } from '../popovers/user-account.popover';
import { MessagesPopoverPage } from '../popovers/messages.popover';
import { TasksPopoverPage } from '../popovers/tasks.popover';

// Services
import { Config } from '../../providers/config/config';

@Component({
  selector: 'user-details',
  template: `
    <ion-item no-padding class="user-info-container">
      <ion-avatar 
        item-start 
        *ngIf="config.USER.avatar_file.file_thumb_url.length > 0"
        (click)="presentUserPopover($event)"
      >
        <img src="{{ config.USER.avatar_file.file_thumb_url }}" />
      </ion-avatar>

      <span class="avatar" *ngIf="config.USER.avatar_file.file_thumb_url.length == 0">{{ config.USER.userInitials }}</span>
      <p>Hello, {{ config.USER.first_name }}!</p>

      <button class="badge" icon-only color="dark-blue" (click)="presentMessagesPopover($event);$event.stopPropagation();"
        *ngIf="showNotifications" 
        item-end 
      >
        <ion-badge *ngIf="messages.length > 0" color="danger">{{ messages.length }}</ion-badge>
        <ion-icon name="chatboxes" color="white"></ion-icon>
      </button>
      <button class="badge" icon-only (click)="presentTasksPopover($event);$event.stopPropagation();"
        *ngIf="showNotifications" 
        item-end
      >
        <ion-badge *ngIf="tasks.length > 0" color="danger">{{ tasks.length }}</ion-badge>
        <ion-icon name="checkmark-circle-outline" color="white"></ion-icon>
      </button>
    </ion-item>
  `
})
export class UserDetailsComponent {
  @Input() showNotifications = false;
  config = Config; 
  messages = [];
  tasks = [];

  constructor(
    private popoverCtrl: PopoverController,
    private events: Events
  ) {
    this.events.unsubscribe('messages:unread-loaded');
    this.events.unsubscribe('tasks:open-loaded');

    this.events.subscribe('messages:unread-loaded', ( data ) => {
      this.messages = data.messages;
    });

    this.events.subscribe('tasks:open-loaded', ( data ) => {
      this.tasks = data.tasks;
    });
  }
  
  presentUserPopover( ev )
  {
    let popover = this.popoverCtrl.create( UserAccountPopoverPage );
    popover.present({
      ev: ev
    });

    popover.onDidDismiss(data => {
      switch( data )
      {
        case 'log-out':
          this.events.publish('user:logout');
          break;
        case 'my-account':
          //this.menuGoTo('userAccountPage', this.userAccountPage);
          break;
      }
    });
  }

  presentMessagesPopover( ev )
  {
    let popover = this.popoverCtrl.create( MessagesPopoverPage, {
      messages: this.messages
    }, {
      cssClass: 'popover-2x'
    });
    popover.present({
      ev: ev
    });
  }

  presentTasksPopover(ev )
  {
    let popover = this.popoverCtrl.create( TasksPopoverPage, {
      tasks: this.tasks
    }, {
      cssClass: 'popover-2x'
    });
    popover.present({
      ev: ev
    });
  }
}