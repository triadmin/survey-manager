import { 
  Component, 
  forwardRef, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

@Component({
  selector: 'color-picker',
  template: `
    <ion-item>
      <p *ngIf="label.length > 0">{{ label }}</p>
      <button 
        ion-button
        class="btn-color" 
        *ngFor="let color of colors; index as i" 
        [style.opacity]="isSelectedColor(color)" 
        [color]="color" 
        (click)="handleButtonClick(color)"
      ></button>
    </ion-item>
  `,
  providers: [{ 
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ColorPickerComponent),
    multi: true
  }]
})

export class ColorPickerComponent implements ControlValueAccessor {
  @Input() label: string = '';

  colors:any = [
    'cerise-pink',
    'deep-chestnut',
    'crayon-orange',
    'turbo',
    'cerulean',
    'deep-sky-blue',
    'french-blue',
    'saphire',
    'atlantis',
    'asparagus',
    'bright-gray',
    'lily',
    'bright-turquoise'
  ];

  selectedButtonIndex: number = 0;
  selectedColor: any;

  // The internal data model (ngModel)
  private innerValue: any = '';

  // The value we save to the database.
  private saveValue: any = '';

  constructor() {
  }

  propagateChange = (_: any) => {};

  writeValue(value: any) {
    if (value !== undefined) {
      this.innerValue = value;
    }
  }

  registerOnChange(fn) {
    this.propagateChange = fn;
  }

  registerOnTouched() {}

  // Get accessor
  get value(): any {
    return this.innerValue;
  };

  // Set accessor including call the onchange callback
  set value(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
    this.propagateChange(this.innerValue);
  }

  isSelectedColor( color )
  {
    if ( color === this.innerValue )
    {
      return '1';
    }
  }

  handleButtonClick( color )
  {
    this.innerValue = color;
    this.selectedColor = color;
    this.propagateChange(this.innerValue);
  }
}
