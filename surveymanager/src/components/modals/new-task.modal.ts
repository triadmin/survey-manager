import { Component } from '@angular/core';
import { 
  ViewController, 
  NavParams,
  Events
} from 'ionic-angular';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

// Providers
import { SurveyData } from '../../providers/surveys/survey-client-data';
import { Config } from '../../providers/config/config';
import { TasksService } from '../../providers/tasks/tasks-service';

@Component({
  templateUrl: 'new-task.modal.html'
})

export class NewTaskModalPage
{
  surveyData: any = SurveyData;
  config: any = Config;
  survey_attempt_id = 0;

  // RichText Editor
  new_task_text: FormControl;
  task:any = {};

  constructor(
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private tasksService: TasksService,
    private events: Events
  ) {
  }

  saveTask()
  {
    // Get rich-text field value
    this.task.task_text = this.new_task_text.value;
    this.task.survey_id = this.surveyData.survey.survey_id;
    this.task.survey_attempt_id = this.survey_attempt_id;
    this.task.survey_user_id = this.config.USER.user_app_record.survey_user_id;

    this.tasksService.saveTask( this.task )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Task saved.',
          status: 'success',
          type: 'save task'
        }); 

        this.events.publish('task:add', {
          task: data
        });
        this.dismissModal();
      });
  }

  dismissModal() {
    this.task = {};
    this.viewCtrl.dismiss();
  }

  ngOnInit()
  {
    this.new_task_text = this.formBuilder.control('');
    this.survey_attempt_id = this.navParams.get('surveyAttemptId');
  }
}
