import { Component } from '@angular/core';
import { 
  ViewController, 
  NavParams,
  ActionSheetController,
  Events
} from 'ionic-angular';

// Providers
import { FilesService } from '../../providers/files/files-service';

@Component({
  template: `
    <ion-header>
      <ion-toolbar color="header">
        <ion-title>Photo Viewer</ion-title>
        <ion-buttons start>
          <button ion-button (click)="dismissModal()">Close</button>
        </ion-buttons>
      </ion-toolbar>  
    </ion-header>
      
    <ion-content>
      <img zoom-pan class="img-photo-viewer" [src]="photo.file_url" />
    </ion-content>
  `
})

export class PhotoViewModalPage
{
  photo: any = {};

  constructor(
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private actionSheetCtrl: ActionSheetController,
    private filesService: FilesService,
    private events: Events
  ) {
    this.photo = this.navParams.get('photo');
  }

  dismissModal() {
    this.viewCtrl.dismiss();
  }
}
