import { Component } from '@angular/core';
import { 
  ViewController, 
  NavParams,
  Events
} from 'ionic-angular';
import { 
  FormControl, 
  FormBuilder
} from "@angular/forms";

// Providers
import { SurveyData } from '../../providers/surveys/survey-client-data';
import { Config } from '../../providers/config/config';
import { MessagesService } from '../../providers/messages/messages-service';

@Component({
  templateUrl: 'new-message.modal.html'
})

export class NewMessageModalPage
{
  photo: any = {};
  surveyData: any = SurveyData;
  config: any = Config;
  survey_attempt_id = 0;

  // RichText Editor
  new_message_text: FormControl;
  message:any = {};

  constructor(
    private viewCtrl: ViewController,
    private navParams: NavParams,
    private formBuilder: FormBuilder,
    private messagesService: MessagesService,
    private events: Events
  ) {
    if ( this.navParams.get('surveyAttemptId') )
    {
      this.survey_attempt_id = this.navParams.get('surveyAttemptId');
    }
  }

  sendMessage()
  {
    // Get rich-text field value
    this.message.message_text = this.new_message_text.value;
    this.message.survey_id = this.surveyData.survey.survey_id;
    this.message.survey_attempt_id = this.survey_attempt_id;
    this.message.survey_user_id = this.config.USER.user_app_record.survey_user_id;

    // Transform recipients
    for ( let r of this.message.recipients )
    {
      r.user_id_to = r.survey_user_id;
      r.user_id_from = this.config.USER.user_app_record.survey_user_id;
    }

    this.messagesService.saveMessage( this.message )
      .subscribe( data => {
        this.events.publish('app:log', {
          message: 'Message delivered.',
          status: 'success',
          type: 'send message'
        }); 
        this.events.publish( 'messages:create', {
          message: this.message
        });
        this.dismissModal();
      });
  }

  dismissModal() {
    this.message = {};
    this.viewCtrl.dismiss();
  }

  ngOnInit()
  {
    this.new_message_text = this.formBuilder.control('');
  }
}
