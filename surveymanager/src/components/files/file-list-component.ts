import { 
  Component,  
  Input,
  Output,
  EventEmitter
} from '@angular/core';

// Providers
import { ImageService } from '../../providers/images/image-service';

@Component({
  selector: 'file-list',
  template: `
    <ion-row *ngIf="files && files.length > 0">
      <ion-col col-3 *ngFor="let file of files">
        <div *ngIf="file.file_type == 'image'">
          <img *ngIf="file.file_thumb_url.length > 0" src="{{ file.file_thumb_url }}" (click)="viewFile(file)" />
          <img *ngIf="file.file_thumb_url.length == 0" src="{{ file.file_url }}" (click)="viewFile(file)" />
        </div>
        <div style="text-align: center;" margin-bottom *ngIf="file.file_type != 'image'">
          <a href="{{ file.file_url }}" target="_blank"><img [src]="getFileIcon(file.file_type) | safe_html" style="max-width: 5rem; margin: 0 auto;" /></a>
          <a href="{{ file.file_url }}" target="_blank">{{ file.file_name }}</a>
        </div>

        <button class="button-file-delete" clear ion-button icon-only (click)="deleteFile(file)">
          <ion-icon name="close-circle" color="danger"></ion-icon>
        </button>
      </ion-col>
    </ion-row>
  `
})

//
export class FileListComponent {
  @Input() files: any;

  // Output for image files that are displayed in 
  // a modal window.
  @Output() fileToView = new EventEmitter<any>();
  @Output() fileToDelete = new EventEmitter<any>();

  constructor(
    public imageService: ImageService
  ) {}

  viewFile( file ) 
  {
    this.fileToView.emit( file );
  }

  deleteFile( file )
  {
    this.fileToDelete.emit( file );
  }

  getFileIcon( fileType ) 
  {
    let base64 = '';
    switch ( fileType )
    {
      case 'word':
        base64 = this.imageService.iconWord();
        break;
      case 'pdf':
        base64 = this.imageService.iconPdf();
        break;
      case 'excel':
        base64 = this.imageService.iconExcel();
        break;
    }
    return base64;
  }

}