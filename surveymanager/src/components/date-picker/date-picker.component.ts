import { 
  Component, 
  forwardRef, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { DatePipe } from '@angular/common';
import { Platform } from 'ionic-angular';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

const noop = () => {};

export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CustomDatePickerComponent),
  multi: true
};

import moment from 'moment';

@Component({
  selector: 'custom-date-picker',
  template: `
    <ion-item>
      <ion-label class="label label-md label-md-label {{ label_class }}" *ngIf="label.length > 0" color="label" stacked>{{ label }}</ion-label>
      <ion-input 
        *ngIf="!platform.is('cordova')" 
        type="text" 
        name="{{ name }}"
        pattern="\d*" 
        placeholder="MM/DD/YYYY" 
        mask="**/**/****" 
        [(ngModel)]="value" 
        (ionBlur)="onBlur()" 
        (input)="onChange($event)"
      ></ion-input>
      
      <ion-datetime 
        *ngIf="platform.is('cordova')" 
        displayFormat="MM/DD/YYYY" 
        name="{{ name }}"
        min="{{dateRange.min}}"
        max="{{dateRange.max}}"
        [(ngModel)]="value" 
        (ionBlur)="onBlur()"
        (input)="onChange($event)"
      ></ion-datetime>
    </ion-item>  
  `,
  providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR, DatePipe]
})

export class CustomDatePickerComponent implements ControlValueAccessor {
  @Input() label: string;
  @Input() label_class: string = '';
  @Input() name: string;
  @Output() dirtyData = new EventEmitter<any>();

  // The internal data model (ngModel)
  private innerValue: any = '';

  // The value we save to the database.
  private saveValue: any = '';

  // Placeholders for the callbacks which are later provided
  // by the Control Value Accessor
  private propagateChange = (_: any) => {};

  dateRange: any = {};

  constructor(
    public platform: Platform,
    public datePipe: DatePipe
  ) {
    this.getYearRange();
  }

  // From ControlValueAccessor interface
  // Sets our initial form value from the model.
  writeValue(value: any) {
    value = this.checkEmptyDate( value );
    if (value !== this.innerValue) {
      if ( this.platform.is('cordova') )
      {
        this.innerValue = value;
      } else {
        this.innerValue = this.convertDateFromMySql(value);
      }
    }
  }

  // Get accessor
  get value(): any {
    return this.innerValue;
  };

  // Set accessor including call the onchange callback
  set value(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
    this.propagateChange(this.innerValue);
  }

  // Set touched on blur
  onBlur() {
    let dateIsGood = true;
    if ( !this.platform.is('cordova') )
    {
      dateIsGood = this.sanityCheck( this.innerValue );
    }
    if ( dateIsGood === true )
    {
      this.saveValue = this.convertDateToMySql(this.innerValue);
      this.propagateChange(this.saveValue);
      this.dirtyData.emit();
    } else {
      // TODO: Highlight error in date field.
      console.log('Problem with date');
    }
    console.log(dateIsGood);
  }

  onChange(event) {
    this.innerValue = event.target.value;
  }

  // From ControlValueAccessor interface
  // Must implement this.
  registerOnChange(fn: any) {
    this.propagateChange = fn;
  }

  // From ControlValueAccessor interface
  // Must implement this.
  registerOnTouched() {}

  /********* Helper methods /*********/
  sanityCheck( date: any )
  {
    // Quick sanity check from our mask directive
    // because sometimes on edit, we'll get a date like:
    // 03/122/018.
    // TODO: Maybe find a new date mask directive.
    // This only happens with the date entry text field.
    let dateIsGood = true;
    if ( date !== null )
    {
      let strDate = date.split('/').join('');
      if ( strDate.length !== 8 )
      {
        dateIsGood = false;
      } else {
        let tmpDate = strDate.slice(0, 2) + '/' + strDate.slice(2, 4) + '/' + strDate.slice(4, 8);
        this.innerValue = tmpDate;
        if ( strDate.slice(0, 2) > 12 || strDate.slice(2, 4) > 31 )
        {
          dateIsGood = false;
        } else {
          dateIsGood = true;
        }
      }

      return dateIsGood;
    } else {
      return false;
    }
  }

  convertDateFromMySql( date: any )
  {
    return moment( date ).format( 'MM/DD/YYYY' );
  }

  convertDateToMySql( date: any )
  {
    return moment( date ).format( 'YYYY-MM-DD' );
  }

  checkEmptyDate( date ) 
  {
    if ( 
      date === '0001-01-01' || 
      date === '0001-01-01 00:00:00' ||
      date === '1901-01-01' ||
      date === '1901-01-01 00:00:00' ||
      date === '0000-00-00' || 
      date === '0000-00-00 00:00:00' || 
      date === null 
      ) {
      return date = '';
    }
    return date;
  }

  getYearRange()
  {
    var today = new Date();
    var yyyy = today.getFullYear();
    this.dateRange = {
      min: yyyy - 10,
      max: yyyy + 10
    };
  }
}