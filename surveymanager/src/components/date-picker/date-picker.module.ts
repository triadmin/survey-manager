import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CustomDatePickerComponent } from './date-picker.component';

// Directives
import { InputMask } from '../../directives/input-mask.directive'; 

@NgModule({
  declarations: [
    CustomDatePickerComponent,
    InputMask
  ],
  imports: [
    IonicPageModule.forChild(CustomDatePickerComponent)
  ],
  providers: [
  ],
  exports: [
    CustomDatePickerComponent
  ]
})
export class CustomDatePickerComponentModule {}
