import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RichTextComponent } from './rich-text';

// Components
import { LinkPopoverPage } from './link.popover';
import { ImagePopoverPage } from './image.popover';

// Modules
import { UploadComponentModule } from '../file-upload/file-upload.module';

@NgModule({
  declarations: [
    RichTextComponent,
    LinkPopoverPage,
    ImagePopoverPage
  ],
  imports: [
    IonicPageModule.forChild(RichTextComponent),
    UploadComponentModule
  ],
  entryComponents: [
    LinkPopoverPage,
    ImagePopoverPage
  ],  
  exports: [
    RichTextComponent
  ]
})
export class RichTextComponentModule {}
