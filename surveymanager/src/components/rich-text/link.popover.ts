import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

@Component({
  template: `
    <ion-list style="width: 450px;">
      <ion-item>
        <ion-label color="label" stacked>Enter Link URL</ion-label>
        <ion-input #linkUrl type="text" [(ngModel)]="linkURL" name="linkURL"></ion-input>
      </ion-item>
      <ion-item>
        <button ion-button small (click)="closePopover()">Add Link</button>
      </ion-item>
    </ion-list>
  `
})
export class LinkPopoverPage {
  linkURL:string = '';

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  )
  {}

  closePopover()
  {
    this.viewCtrl.dismiss( this.linkURL );
  }
}
