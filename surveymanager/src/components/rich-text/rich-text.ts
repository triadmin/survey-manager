import { 
  Component, 
  ViewChild, 
  ElementRef, 
  Input, 
  Output,
  EventEmitter 
} from '@angular/core';
import { FormControl } from "@angular/forms";
import { PopoverController } from 'ionic-angular';

// Popovers
import { LinkPopoverPage } from './link.popover';
import { ImagePopoverPage } from './image.popover';

@Component({
  selector: 'rich-text',
  templateUrl: 'rich-text.html'
})
export class RichTextComponent {
  @ViewChild('editor') editor: ElementRef;
  @ViewChild('decorate') decorate: ElementRef;
  @ViewChild('styler') styler: ElementRef;

  @Input() formControlItem: FormControl;
  @Input() placeholderText: string;
  @Input() editorHeight: string = '40vh';

  @Output() textChange = new EventEmitter<string>();
  @Output() textComplete = new EventEmitter<string>();

  element:any;
  uniqueId = `editor${Math.floor(Math.random() * 1000000)}`;

  constructor(
    private popoverCtrl: PopoverController
  ) {
  }

  getEditorHeight()
  {
    return this.editorHeight;
  }

  getPlaceholderText() {
    if (this.placeholderText !== undefined) {
      return this.placeholderText
    }
    return '';
  }

  updateEditorValue()
  {
    this.element.innerHTML = this.formControlItem.value;
  }

  private stringTools = {
    isNullOrWhiteSpace: (value: string) => {
      if (value == null || value == undefined) {
        return true;
      }
      value = value.replace(/[\n\r]/g, '');
      value = value.split(' ').join('');

      return value.length === 0;
    },
    stripHtml: (html: string ) => {
      var doc = new DOMParser().parseFromString(html, 'text/html');
      return doc.body.textContent || "";
    }
  };

  private updateItem() 
  {
    this.element = this.editor.nativeElement as HTMLDivElement;
    let element = this.element;
    element.innerHTML = this.formControlItem.value;

    // if (element.innerHTML === null || element.innerHTML === '') {
    //   element.innerHTML = '<div></div>';
    // }

    const reactToChangeEvent = ( action = null, ev ) => { 
      if ( action === 'paste' )
      {
        let clipboardData = ev.clipboardData;
        let pastedData = clipboardData.getData('Text');        
        element.innerHTML = this.stringTools.stripHtml( pastedData );
        this.formControlItem.setValue(element.innerHTML);
      } else {
        if (this.stringTools.isNullOrWhiteSpace(element.innerText)) {
          element.innerHTML = '<div></div>';
          this.formControlItem.setValue(null);
        } else {
          this.formControlItem.setValue(element.innerHTML);
        }
      }

      this.textChange.emit();
    };

    const onBlur = () => {
      console.log('bluring');
      this.textComplete.emit();
    }

    element.onchange = ( ev ) => reactToChangeEvent( 'onchange', ev );
    element.onkeyup = ( ev ) => reactToChangeEvent( 'onkeyup', ev );
    element.onpaste = ( ev ) => reactToChangeEvent( 'paste', ev );
    element.oninput = ( ev ) => reactToChangeEvent( 'oninput', ev );
    element.onblur = ( ev ) => onBlur();
  }

  private wireupButtons() {
    let buttons = (this.decorate.nativeElement as HTMLDivElement).getElementsByTagName('button');
    for (let i = 0; i < buttons.length; i++) {
      let button = buttons[i];

      let command = button.getAttribute('data-command');

      if (command.includes('|')) {
        let parameter = command.split('|')[1];
        command = command.split('|')[0];

        button.addEventListener('click', () => {
          document.execCommand(command, false, parameter);
        });
      } else {
        button.addEventListener('click', ( ev ) => {
          if ( command === 'createLink' )
          {
            let sel = this.saveSelection();
            let linkPopover = this.popoverCtrl.create( LinkPopoverPage );
            linkPopover.present({
              ev: ev
            });

            linkPopover.onDidDismiss( data => {
              this.restoreSelection( sel );
              if ( data !== null && data.length > 0 )
              {
                document.execCommand(command, false, data);
              }
            });
          } else if ( command === 'insertImage' ) {
            let sel = this.saveSelection();
            let imagePopover = this.popoverCtrl.create( ImagePopoverPage );
            imagePopover.present({
              ev: ev
            });

            imagePopover.onDidDismiss( data => {
              this.restoreSelection( sel );
              if ( data !== null && data.url.length > 0 )
              {
                document.execCommand(command, false, data.url);
              }
            });
          } else {
            document.execCommand(command);
          }
        });
      }
    }
  }

  private saveSelection() {
    let sel = window.getSelection();
    if ( sel.getRangeAt && sel.rangeCount ) {
      return sel.getRangeAt(0);
    }
    return null;
  }

  private restoreSelection(range) {
    if (range) {      
      let sel = window.getSelection();
      sel.removeAllRanges();
      sel.addRange(range);
    }
  }

  ngAfterContentInit() {
    this.updateItem();
    this.wireupButtons();
  }
}
