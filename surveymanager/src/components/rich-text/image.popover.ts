import { Component } from '@angular/core';
import { NavController, ViewController, NavParams } from 'ionic-angular';

// Components


@Component({
  template: `
    <ion-list style="width: 450px;">
      <ion-item>
      <app-upload
        (fileUploaded)="closePopover($event)"
        uploadLabel="Drag and drop image"
        public="yes"
      ></app-upload>
      </ion-item>
    </ion-list>
  `
})
export class ImagePopoverPage {
  imageURL:string = '';

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController,
    public navParams: NavParams
  )
  {}

  closePopover( fileObj )
  {
    this.viewCtrl.dismiss( fileObj );
  }
}
