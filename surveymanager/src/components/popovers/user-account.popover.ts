import { Component } from '@angular/core';
import { NavController, ViewController } from 'ionic-angular';

// Config
import { Config } from '../../providers/config/config';

@Component({
  template: `
    <ion-list>
      <ion-item>    
        <button ion-button clear icon-start (click)="closePopover('my-account')">My Account</button>
      </ion-item>
      <ion-item>
        <button ion-button clear icon-start (click)="closePopover('log-out')">Log Out</button>
      </ion-item>
      <span *ngIf="userApps.length > 1">
      <ion-item><ion-icon name="apps"></ion-icon> My Apps</ion-item>
      <ion-item *ngFor="let app of userApps">
        <a href="{{ app.url }}" target="_blank" ion-button clear>{{ app.name }}</a>
      </ion-item>
      </span>
    </ion-list>
  `
})
export class UserAccountPopoverPage {
  userApps:any = [];

  constructor(
    public viewCtrl: ViewController,
    public navCtrl: NavController
  )
  {
    this.userApps = Config.USER_APPS;
  }

  closePopover( selectedItem )
  {
    this.viewCtrl.dismiss( selectedItem );
  }

}
