import { Component } from '@angular/core';
import { 
  ViewController,
  ModalController,
  NavParams,
  Events
} from 'ionic-angular';

// Modals
import { NewMessageModalPage } from '../modals/new-message.modal';

// Providers
import { MessagesService } from '../../providers/messages/messages-service';

// Pages
import { SurveyViewPage } from '../../pages/survey-client/survey-attempt-view/survey-view';

@Component({
  template: `
    <div style="border-bottom: 2px solid #ccc;">
      <ion-row>
        <ion-col col-4 padding><strong>Unread Messages</strong></ion-col>
        <ion-col col-8>
          <div float-right>
            <!--
            <button ion-button small clear icon-start (click)="showAllMessagesPage()">
              <ion-icon name="chatbubbles"></ion-icon> All Messages
            </button>
            -->
            <button ion-button small clear icon-start (click)="presentNewMessageModal()">
              <ion-icon name="add-circle"></ion-icon> New Message
            </button>
          </div>
        </ion-col>
      </ion-row>
    </div>

    <ion-list style="height: 20rem; overflow: scroll;">
      <ion-item *ngFor="let m of messages">
        <button icon-only item-start (click)="markAsRead(m)">
          <ion-icon name="checkmark-circle-outline"></ion-icon>
        </button>
        <small class="muted">From <b>{{ m.first_name }} {{ m.last_name }}</b> {{ m.created_at | moment_from_now }}</small>
        <button no-margin ion-button clear *ngIf="m.survey_attempt_id > 0" (click)="showSurveyAttemptPage(m.survey_attempt_id)">{{ m.title }}</button>
        <p [innerHTML]="m.message_text" margin-top></p>
      </ion-item>
    </ion-list>
  `
})
export class MessagesPopoverPage {
  messages = [];

  constructor(
    private viewCtrl: ViewController,
    private events: Events,
    private messagesService: MessagesService,
    private modalCtrl: ModalController,
    private navParams: NavParams
  )
  {
  }

  showAllMessagesPage()
  {

  }

  presentNewMessageModal()
  {
    let newMessageModal = this.modalCtrl.create( NewMessageModalPage, {});
    newMessageModal.present();
  }

  showSurveyAttemptPage( surveyAttemptId )
  {
    let surveyAttempt = {
      survey_attempt_id: surveyAttemptId,
      survey_user: {},
      location: ''
    }
    this.events.publish('survey-attempt:view', {
      surveyAttempt: surveyAttempt
    });
    this.closePopover();
  }

  closePopover()
  {
    this.viewCtrl.dismiss( );
  }

  ngOnInit()
  {
    this.messages = this.navParams.get('messages');
    console.log(this.messages);
  }
}
