import { Component } from '@angular/core';
import { 
  NavController, 
  ViewController,
  NavParams,
  Events
} from 'ionic-angular';

@Component({
  template: `
    <ion-list>
      <button ion-item *ngFor="let a of activities" (click)="showSurveyAttemptPage(a)">
        {{ a.title }}
      </button>
    </ion-list>
  `
})
export class MapActivityPopoverPage {
  activities = [];

  constructor(
    private viewCtrl: ViewController,
    private navCtrl: NavController,
    private navParams: NavParams,
    private events: Events
  )
  {}

  showSurveyAttemptPage( activity )
  {
    this.navCtrl.pop();
    this.events.publish('survey-attempt:view', {
      surveyAttempt: activity
    });
  }

  closePopover()
  {
    this.viewCtrl.dismiss();
  }

  ngOnInit()
  {
    this.activities = this.navParams.get('activities');
  }
}
