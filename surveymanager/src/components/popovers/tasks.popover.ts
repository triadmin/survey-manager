import { Component } from '@angular/core';
import { 
  ViewController,
  NavParams,
  Events
} from 'ionic-angular';

// Providers
import { TasksService } from '../../providers/tasks/tasks-service';

@Component({
  template: `
    <div style="border-bottom: 2px solid #ccc;">
      <ion-row>
        <ion-col col-4 padding><strong>Open Tasks</strong></ion-col>
        <ion-col col-8></ion-col>
      </ion-row>
    </div>

    <ion-list style="height: 20rem; overflow: scroll;">
      <button ion-item *ngFor="let t of tasks" (click)="showSurveyAttemptPage(t.survey_attempt)">
        <small class="muted">Created {{ t.created_at | moment_from_now }}</small>
        <p><b>{{ t.survey_attempt.title }}</b></p>
        <p [innerHTML]="t.task_text" margin-top></p>
      </button>
    </ion-list>
  `
})
export class TasksPopoverPage {
  tasks = [];

  constructor(
    private  viewCtrl: ViewController,
    private tasksService: TasksService,
    private navParams: NavParams,
    private events: Events
  )
  {
  }

  showSurveyAttemptPage( surveyAttempt )
  {
    this.events.publish('survey-attempt:view', {
      surveyAttempt: surveyAttempt
    });
    this.closePopover();
  }

  closePopover()
  {
    this.viewCtrl.dismiss( );
  }

  ngOnInit()
  {
    this.tasks = this.navParams.get('tasks');
  }
}
