import { 
  Component, 
  Input,
  Output,
  EventEmitter
} from '@angular/core';
import { NavController, Events, ModalController } from 'ionic-angular';

@Component({
  selector: 'custom-header',
  template: `
    <ion-navbar color="header">
      <ion-title>{{ header.title }}</ion-title>

      <ion-buttons start>
        <button ion-button (click)="toggleMenu()" hideWhen="landscape">
          <ion-icon name="menu"></ion-icon>
        </button>
      </ion-buttons>

      <ion-buttons end *ngIf="header.button">
        <button ion-button icon-start (click)="buttonClick()">
          <ion-icon *ngIf="header.button.icon" name="{{ header.button.icon }}"></ion-icon> {{ header.button.label }}
        </button>
      </ion-buttons>
    </ion-navbar>
  `
})
export class CustomHeaderComponent {
  @Input() header;
  @Output() buttonClicked = new EventEmitter<any>();

  constructor(
    public navCtrl: NavController,
    public events: Events,
    public modalCtrl: ModalController
  ) {
  }

  toggleMenu()
  {
    this.events.publish('menu:toggle', {
      toggle: ''
    });
  }

  buttonClick()
  {
    console.log('button clicked');
    this.buttonClicked.emit();
  }
}