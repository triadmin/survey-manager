import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

// Ionic Native
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
//import { Network } from '@ionic-native/network';
import { Keyboard } from '@ionic-native/keyboard';
import { File } from '@ionic-native/file';
import { FileOpener } from '@ionic-native/file-opener';

// Pages
import { DashboardPage } from '../pages/dashboard-system/dashboard';
import { SurveysPage } from '../pages/survey-create/surveys';
import { SurveyViewPage } from '../pages/survey-client/survey-attempt-view/survey-view';
import { SurveyAddEditPage } from '../pages/survey-create/survey-add-edit';
import { RegionSetsPage } from '../pages/region-sets/region-sets';
import { RegionMapPage } from '../pages/survey-client/region-map/region-map';
import { RegionSetsAddEditPage } from '../pages/region-sets/region-sets-add-edit';
import { UsersPage } from '../pages/users/users';
import { UserAccountPage } from '../pages/user-account/user-account';
import { LoginPage } from '../pages/login/login';
import { GoalsPage } from '../pages/survey-client/goals/goals-list';
import { GoalAddEditPage } from '../pages/survey-client/goals/goal-add-edit';

// Survey Client
import { DashboardSurveyClientPage } from '../pages/survey-client/dashboard/dashboard-survey';
import { SurveyDataEntryPage } from '../pages/survey-client/survey-data-entry/survey-data-entry';
import { SelectSurveyPage } from '../pages/survey-client/select-survey/select-survey';

// Popovers
import { UserAccountPopoverPage } from '../components/popovers/user-account.popover';
import { AnswerSettingsPopoverPage } from '../pages/survey-create/components/answer-settings.popover';
import { SurveyClientMenuPopoverPage } from '../components/survey-client/menu.component';
import { RegionSetSubNodePopoverPage } from '../pages/region-sets/components/region-set-sub-node.popover';
import { RegionSetLocationPopoverPage } from '../pages/region-sets/components/region-set-location.popover';
import { SurveyMenuPopoverPage } from '../pages/survey-create/components/survey-menu.popover';
import { DeleteSurveyPagePopoverPage } from '../pages/survey-create/components/delete-page.popover';
import { MessagesPopoverPage } from '../components/popovers/messages.popover';
import { TasksPopoverPage } from '../components/popovers/tasks.popover';
import { MapActivityPopoverPage } from '../components/popovers/map-activity.popover';

// Services
import { TokenInterceptor } from '../providers/http/token.interceptor';
import { UserPermissionsService } from '../providers/users/user-permissions-service';
import { SurveysService } from '../providers/surveys/surveys-service';
import { SurveyQuestionsService } from '../providers/surveys/survey-questions-service';
import { SurveyAttemptsService } from '../providers/surveys/survey-attempts-service';
import { SurveyQuestionResponseService } from '../providers/surveys/survey-question-response-service';
import { UsersService } from '../providers/users/users-service';
import { LoginService } from '../providers/login/login-service';
import { FilesService } from '../providers/files/files-service';
import { ProjectsService } from '../providers/projects/projects-service';
import { RegionsService } from '../providers/surveys/regions-service';
import { SurveyReportElementService } from '../providers/surveys/survey-report-element-service';
import { PdfService } from '../providers/pdf/pdf-service';
import { ImageService } from '../providers/images/image-service';
import { MessagesService } from '../providers/messages/messages-service';
import { TasksService } from '../providers/tasks/tasks-service';
import { GoalsService } from '../providers/goals/goals-service';

// Helper Services
import { HelperService } from '../providers/helpers/helper-service';
import { HelperCsvService } from '../providers/helpers/helper-csv-service';
import { HelperSurveyService } from '../providers/helpers/helper-survey-service';
import { HelperPDFService } from '../providers/helpers/helper-pdf-service';
import { HelperMapService} from '../providers/helpers/helper-map-service';
import { HelpersChartService } from '../providers/helpers/helper-chart-service';

// Pipes
import { NewlinePipe } from '../pipes/common-pipes';
import { ReplacePipe } from '../pipes/common-pipes';
import { FileextensionPipe } from '../pipes/common-pipes';
import { MomentFromNowPipe } from '../pipes/common-pipes';
import { MomentConvertMySqlPipe } from '../pipes/common-pipes';
import { SafeHtmlPipe } from '../pipes/common-pipes'; 

// Modules
import { FormsModule } from '@angular/forms';
import { ChartsModule } from 'ng2-charts-x';
import { LinkyModule } from 'angular-linky';
import { RichTextComponentModule } from '../components/rich-text/rich-text.module';
import { UploadComponentModule } from '../components/file-upload/file-upload.module';
import { CustomDatePickerComponentModule } from '../components/date-picker/date-picker.module';
import { NgxGaugeModule } from 'ngx-gauge';
import { UserDetailsComponentModule } from '../components/user-details/user-details.module';
import { CountUpModule } from 'countup.js-angular2';

// Modals
import { PhotoViewModalPage } from '../components/modals/photo-viewer.modal';
import { NewMessageModalPage } from '../components/modals/new-message.modal';
import { NewTaskModalPage } from '../components/modals/new-task.modal';

// Survey client left column
import { SurveyClientLeftColumnModule } from '../pages/survey-client/modules/survey-client-left-column/survey-client-left-column.module';

// Directives
import { ZoomPanDirective } from '../directives/zoom-pan.directive';

// Components
import { CustomHeaderComponent } from '../components/header-custom.component';
import { ColorPickerComponent } from '../components/color-picker/color-picker.component';
import { MenuComponent } from '../pages/survey-create/components/menu.component';
import { SurveyInformationComponent } from '../pages/survey-create/components/survey-info.component';
import { SurveyQuestionComponent } from '../pages/survey-create/components/survey-questions.component';
import { SurveyReportConfigComponent } from '../pages/survey-create/components/survey-report-config.component';
import { SurveySettingsComponent } from '../pages/survey-create/components/survey-settings.component';
import { SurveyExportComponent } from '../pages/survey-create/components/survey-export.component';
import { RegionSetItemsComponent } from '../pages/region-sets/components/region-set-items.component';
import { FileListComponent } from '../components/files/file-list-component';
import { TasksComponent } from '../pages/survey-client/survey-attempt-view/components/tasks.component';
import { MessagesComponent } from '../pages/survey-client/survey-attempt-view/components/messages.component';

// Question Types
import { CheckboxComponent } from '../components/question-types/checkbox';
import { RadioComponent } from '../components/question-types/radio';
import { TextComponent } from '../components/question-types/text';
import { TextareaComponent } from '../components/question-types/textarea';
import { SelectComponent } from '../components/question-types/select';
import { SelectmultiComponent } from '../components/question-types/selectmulti';
import { SurveyAttemptComponent } from '../components/question-types/survey-attempt';
import { UploadComponent } from '../components/question-types/upload';
import { RegonSetComponent } from '../components/question-types/region-set';
import { ListWithHeadersTextComponent } from '../components/question-types/list-with-headers-text';
import { ListWithHeadersCheckboxComponent } from '../components/question-types/list-with-headers-checkbox';
import { HeadingComponent } from '../components/question-types/heading';
import { DatetimeComponent } from '../components/question-types/datetime';
import { RatingScaleComponent } from '../components/question-types/rating-scale';
import { ContentComponent } from '../components/question-types/content';
import { CrossReferenceComponent } from '../components/question-types/cross-reference';
import { SliderComponent } from '../components/question-types/slider';
import { ToggleComponent } from '../components/question-types/toggle';

// Survey View Components (Standard and Custom)
import { SurveyViewDefaultComponent } from '../pages/survey-client/survey-attempt-view/templates/survey-view.default.template';
import { SurveyViewTnfComponent } from '../pages/survey-client/survey-attempt-view/templates/custom/survey-view.tnf.template';

// Question Type Components for Creating Survey
import { TextboxSingleFormComponent } from '../pages/survey-create/components/question-types/textbox-single-form.component';
import { TextboxMultiFormComponent } from '../pages/survey-create/components/question-types/textbox-multi-form.component';
import { MultiAnswerFormComponent } from '../pages/survey-create/components/question-types/multi-answer-form.component';
import { HeadingFormComponent } from '../pages/survey-create/components/question-types/heading-form.component';
import { ContentFormComponent } from '../pages/survey-create/components/question-types/content-form.component';
import { RegionFormComponent } from '../pages/survey-create/components/question-types/region-form.component';
import { UploadFormComponent } from '../pages/survey-create/components/question-types/upload-form.component';
import { RatingScaleFormComponent } from '../pages/survey-create/components/question-types/rating-scale-form.component';
import { SessionListFormComponent } from '../pages/survey-create/components/question-types/session-list.component';
import { ListWithHeadersFormComponent } from '../pages/survey-create/components/question-types/list-with-headers-form.component';
import { CrossReferenceFormComponent } from '../pages/survey-create/components/question-types/cross-reference-form.component';
import { SurveyAttemptFormComponent } from '../pages/survey-create/components/question-types/survey-attempt-form.component';
import { ToggleFormComponent } from '../pages/survey-create/components/question-types/toggle-form.component';
import { SliderFormComponent } from '../pages/survey-create/components/question-types/slider-form.component';

// Chart Comonents
import { BarSimpleComponent } from '../pages/survey-client/reports/report-elements/bar-simple.component';
import { BarHorizontalComponent } from '../pages/survey-client/reports/report-elements/bar-horizontal.component';
import { DonutComponent } from '../pages/survey-client/reports/report-elements/donut.component';
import { PieComponent } from '../pages/survey-client/reports/report-elements/pie.component';
import { TableComponent } from '../pages/survey-client/reports/report-elements/table.component';
import { GaugeComponent } from '../pages/survey-client/reports/report-elements/gauge.component';
import { TrendComponent } from '../pages/survey-client/reports/report-elements/trend.component';
import { GoalReportComponent } from '../pages/survey-client/reports/report-elements/goal-report.component';

@NgModule({
  declarations: [
    MyApp,
    DashboardPage,
    SurveysPage,
    SurveyAddEditPage,
    SurveyViewPage,
    RegionSetsPage,
    RegionSetsAddEditPage,
    RegionMapPage,
    UsersPage,
    UserAccountPage,
    LoginPage,

    DashboardSurveyClientPage,
    SurveyDataEntryPage,
    SelectSurveyPage,

    GoalsPage,
    GoalAddEditPage,

    UserAccountPopoverPage,
    AnswerSettingsPopoverPage,
    SurveyClientMenuPopoverPage,
    RegionSetSubNodePopoverPage,
    RegionSetLocationPopoverPage,
    SurveyMenuPopoverPage,
    MessagesPopoverPage,
    TasksPopoverPage,
    MapActivityPopoverPage,
    DeleteSurveyPagePopoverPage,

    CustomHeaderComponent,
    ColorPickerComponent,
    MenuComponent,
    SurveyInformationComponent,
    SurveyQuestionComponent,
    SurveyReportConfigComponent,
    SurveySettingsComponent,
    SurveyExportComponent,
    RegionSetItemsComponent,
    FileListComponent,
    TasksComponent,
    MessagesComponent,

    TextboxSingleFormComponent,
    TextboxMultiFormComponent,
    MultiAnswerFormComponent,
    HeadingFormComponent,
    ContentFormComponent,
    RegionFormComponent,
    UploadFormComponent,
    RatingScaleFormComponent,
    SessionListFormComponent,
    ListWithHeadersFormComponent,
    CrossReferenceFormComponent,
    SurveyAttemptFormComponent,
    ToggleFormComponent,
    SliderFormComponent,

    CheckboxComponent,
    DatetimeComponent,
    HeadingComponent,
    ListWithHeadersCheckboxComponent,
    ListWithHeadersTextComponent,
    RadioComponent,
    SelectComponent,
    SelectmultiComponent,
    TextComponent,
    TextareaComponent,
    SurveyAttemptComponent,
    RegonSetComponent,
    UploadComponent,
    RatingScaleComponent,
    ContentComponent,
    CrossReferenceComponent,
    SliderComponent,
    ToggleComponent,
    
    SurveyViewDefaultComponent,
    SurveyViewTnfComponent,

    BarSimpleComponent,
    DonutComponent,
    PieComponent,
    TableComponent,
    BarHorizontalComponent,
    GaugeComponent,
    TrendComponent,
    GoalReportComponent,

    NewlinePipe,
    ReplacePipe,
    FileextensionPipe,
    MomentFromNowPipe,
    MomentConvertMySqlPipe,
    SafeHtmlPipe,

    ZoomPanDirective,

    PhotoViewModalPage,
    NewMessageModalPage,
    NewTaskModalPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    ChartsModule,
    FormsModule,
    LinkyModule,
    RichTextComponentModule,
    UploadComponentModule,
    CustomDatePickerComponentModule,
    NgxGaugeModule,
    SurveyClientLeftColumnModule,
    UserDetailsComponentModule,
    CountUpModule,

    IonicModule.forRoot(MyApp, {
      backButtonText: '',
      backButtonIcon: 'arrow-back',
      platforms: {
        ios: {
          scrollAssist: false,
          autoFocusAssist: false,
          statusbarPadding: true
        }
      }
    }),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    DashboardPage,
    SurveysPage,
    SurveyViewPage,
    SurveyAddEditPage,
    RegionSetsPage,
    RegionSetsAddEditPage,
    RegionMapPage,
    UsersPage,
    UserAccountPage,
    LoginPage,

    DashboardSurveyClientPage,
    SurveyDataEntryPage,
    SelectSurveyPage,

    GoalsPage,
    GoalAddEditPage,

    UserAccountPopoverPage,
    AnswerSettingsPopoverPage,
    SurveyClientMenuPopoverPage,
    RegionSetSubNodePopoverPage,
    RegionSetLocationPopoverPage,
    SurveyMenuPopoverPage,
    MessagesPopoverPage,
    TasksPopoverPage,
    MapActivityPopoverPage,
    DeleteSurveyPagePopoverPage,

    PhotoViewModalPage,
    NewMessageModalPage,
    NewTaskModalPage
  ],
  providers: [
    UserPermissionsService,
    SurveysService,
    SurveyQuestionsService,
    SurveyAttemptsService,
    SurveyQuestionResponseService,
    FilesService,
    UsersService,
    HelperService,
    HelperCsvService,
    HelperSurveyService,
    HelperMapService,
    HelperPDFService,
    HelpersChartService,
    LoginService,
    ProjectsService,
    RegionsService,
    ImageService,
    SurveyReportElementService,
    PdfService,
    MessagesService,
    TasksService,
    GoalsService,

    StatusBar,
    SplashScreen,
    File,
    FileOpener,
    Keyboard,
    //Network,
    
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    { provide: ErrorHandler, useClass: IonicErrorHandler }
  ]
})
export class AppModule {}
