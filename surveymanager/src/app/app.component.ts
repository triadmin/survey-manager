import { Component, ViewChild } from '@angular/core';
// Ionic Angular
import { 
  AlertController,
  Events, 
  MenuController, 
  ModalController, 
  Nav, 
  Platform,
  PopoverController,
  ToastController
} from 'ionic-angular';

// Pages
import { SurveysPage } from '../pages/survey-create/surveys';
import { RegionSetsPage } from '../pages/region-sets/region-sets';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard-system/dashboard';
import { UserAccountPage } from '../pages/user-account/user-account';
import { DashboardSurveyClientPage } from '../pages/survey-client/dashboard/dashboard-survey';
import { UsersPage } from '../pages/users/users';
import { SelectSurveyPage } from '../pages/survey-client/select-survey/select-survey';

// Providers
import { HelperService } from '../providers/helpers/helper-service';
import { ProjectsService } from '../providers/projects/projects-service';
import { UserPermissionsService } from '../providers/users/user-permissions-service';
import { UsersService } from '../providers/users/users-service';

// Config 
import { Config } from '../providers/config/config';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  dashboardPage: any;
  surveysPage: any;
  regionSetsPage: any;
  userAccountPage: any;
  userManagerPage: any;
  
  userName: string = 'User';
  userInitials: string = 'U';
  activeMenuItem: string = 'dashboardPage';
  showMenu: boolean = false;
  activeMenuType: string = 'mainMenu';
  navClass: string = '';
  surveyListInitData: any = {};
  config: any = Config;

  constructor(
    public platform: Platform, 
    public alertCtrl: AlertController,
    public events: Events,
    public menuCtrl: MenuController,
    public toastCtrl: ToastController,
    public popoverCtrl: PopoverController,
    public helperService: HelperService,
    public projectsService: ProjectsService,
    public userPermissionsService: UserPermissionsService,
    public usersService: UsersService
  ) {
    this.initializeApp();
    this.listenToEvents();

    this.dashboardPage = DashboardPage;
    this.surveysPage = SurveysPage;
    this.regionSetsPage = RegionSetsPage;
    this.userAccountPage = UserAccountPage;
    this.userManagerPage = UsersPage;
  }

  initializeApp()
  {
    this.platform.ready().then(() => {
      if ( this.platform.is('cordova') ) {}
      this.getTriProjectList();
    });
  }

  getTriProjectList()
  {
    // Get list of TRI projects.
    this.projectsService.getTriProjects().subscribe( data => {
      Config.TRI_PROJECTS = data;
      this.getAppRecord();
    });
  }

  getAppRecord()
  {
    this.projectsService.getApp().subscribe( data => {
      Config.APP_RECORD = data;
      this.events.publish( 'app:init-complete' );
    });
  }

  determineUserRootPage()
  {
    if ( this.userPermissionsService.userHasRole( ['survey-manager-admin','survey-manager-project-level-admin'] ) )
    {
      // If user is an admin, send them to the dashboard screen.
      this.nav.setRoot( SurveysPage );
    } else {
      // Else just send user to survey client.
      // Not So Fast: We need to get surveys this user is assigned to.
      // If number of assigned surveys is > 1, then send them to the survey select page.
      if ( Config.USER_APP_RECORD.surveys.length > 1 )
      {
        this.nav.setRoot( SelectSurveyPage );
      } else {
        // If number of assigned surveys == 1, then send them to the dashboard.
        this.nav.setRoot( DashboardSurveyClientPage, {
          survey: Config.USER_APP_RECORD.surveys[0]
        });
      }
      this.events.publish( 'menu:close', {
        menuType: 'surveyList'
      });
    }
    this.showMenu = true;
  }

  // 
  // Menu control methods
  //
  menuGoTo( menuItem, page )
  {
    this.nav.setRoot( page );
    this.activeMenuItem = menuItem;
  }

  openMenu() {
    this.showMenu = true;
    this.navClass = '';
  }
 
  closeMenu() {
    this.showMenu = false;
    this.navClass = 'menu-none';
  }
 
  toggleMenu() {
    this.menuCtrl.toggle();
  }

  // End menu control methods.

  listenToEvents()
  {
    // Log events
    this.events.subscribe('app:log', ( logData ) => {
      this.presentToast( logData );
    });
  
    // Is init method done?
    this.events.subscribe('app:init-complete', ( logData ) => {
      // Check user access.
      if ( this.userPermissionsService.userHasToken() )
      {
        // Set user information in Config.
        this.usersService.setUserInfo();     
        // Determine where user should go.
        this.determineUserRootPage();   
      } else {
        // Send user to login screen.
        this.rootPage = LoginPage;
      }
    });

    // Is user logging in?
    this.events.subscribe('user:login', ( logData ) => {
      this.determineUserRootPage();
    });

    // Is user logging out?
    this.events.subscribe('user:logout', ( logData ) => {
      this.logOut();
    });

    // Close survey client?
    this.events.subscribe('survey:close-client', ( logData ) => {
      this.nav.setRoot( SurveysPage );
    });

    // Swap out left-side menu.
    this.events.subscribe('menu:swap', ( data ) => {
      let menuType = data.menuType;
      if ( menuType === 'surveyList' )
      {
        this.navClass = 'menu-big';
      } else {
        this.navClass = '';
      }
      this.activeMenuType = menuType;
    });

    this.events.subscribe('menu:close', ( data ) => {
      this.closeMenu();
    });

    this.events.subscribe('menu:open', ( data ) => {
      this.openMenu();
    });

    // Initialize survey activity list for survey apps.
    this.events.subscribe('surveyList:init', ( data ) => {
      this.surveyListInitData = data;
    });
  }

  presentOfflineAlert()
  {
    let alert = this.alertCtrl.create({
      title: 'No internet connection',
      subTitle: 'Please connect to the internet to use this app.',
      buttons: ['Dismiss']
    });
    alert.present();
  }

  presentToast( logData, duration = 2000, closeButtonText = 'Dismiss' )
  {
    let toast = this.toastCtrl.create({
      message: logData.message,
      duration: duration,
      position: 'bottom',
      cssClass: 'toast-' + logData.status
    });

    toast.onDidDismiss(() => {
    });

    toast.present();
  }

  logOut()
  {
    // Destroy local storage keys
    localStorage.removeItem('access_token');
    localStorage.removeItem('user');

    // Go to login page.
    this.nav.setRoot(LoginPage);

    // Make sure our menu if off.
    this.events.publish( 'menu:swap', {
      menuType: 'mainMenu'
    });
    this.showMenu = false;
  }
}

